<meta name="csrf-token" content="{{ csrf_token() }}">

<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
<v-app id="app">

    <company-edit :company="{{json_encode($company)}}"></company-edit>
{{--    <entities :company="{{json_encode($company)}}"></entities>--}}
</v-app>


@include('scripts')