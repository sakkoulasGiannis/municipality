@extends('layouts.app')

@section('content')
    <history :appointments="{{json_encode($appointments)}}" :auth="{{json_encode($auth)}}"></history>
@endsection

@section('script')
    <script src="{{ asset('js/manifest.js')}}"></script>
    <script src="{{ asset('js/vendor.js')}}"></script>
    <script src="{{ asset('js/app.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.el.min.js')}}"></script>
@endsection
