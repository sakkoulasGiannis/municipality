<meta title="Calendar">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
<v-app id="app">
    rooname {{$roomname}}

    <meetup :roomname="{{$roomname}}"></meetup>

</v-app>

<script src='https://meet.jit.si/external_api.js'></script>
@include('scripts')
