@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row text-center p-5">
        <div class="col-md-12  text-center">
            <div class="mt-5 mb-5">
                <a type="button"   data-toggle="modal" data-target="#runModal">
                    <img class="img-fluid" src="{{url('/images/main.png') }}" alt="">
                 </a>

{{--                <a href="/auth" class="    btn-lg">--}}
{{--                    <img class="img-fluid" src="{{url('/images/main.png') }}" alt="">--}}
{{--                </a>--}}
                <img class="img-fluid pt-5" src="{{url('/images/malevizi-text1200x90.png')}}">
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal  fade" id="runModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="text-center">
            <h3>{{env('MUNICIPALITY_NAME', 'ΕΙΣΟΔΟΣ ΥΠΗΡΕΣΙΑΣ')}}</h3>
            <p>Εφαρμογή e -Rantevou</p>
       </div>
             <div class="text-justify">
                <p>
                {{env('POPUP_WELCOME_MESSAGE', '...')}}
                </p>
            </div>
          
          <div class="accept">
              <form action="/auth" method="get">
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                    <label class="form-check-label" for="exampleCheck1">Έχω διαβάσει την <a href="https://malevizi.gov.gr/gdpr/" target="_blank">Πολιτική Προστασίας Προσωπικών Δεδομένων</a> του Δήμου Μαλεβιζίου και τους <a href="https://malevizi.gov.gr/oroi-chrisis/" target="_blank">Όρους Χρήσης</a> .</label>
                  </div>
                  <div class="text-center">
                      <button type="submit" class="btn btn-success btn-lg" style="color: #fff;">Είσοδος στην υπηρεσία</button> <br/>
                      <br/>
                  <a  href="/images/odigies-e-rantevou.pdf" target="_blank" type="submit" class="btn btn-info mt-10" style="color: #fff;">Οδηγίες</a>
                  </div>
                </form>
          </div>
          
          
      </div>
      
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row" style="background-color: #e1e557; padding: 5px;">
        <div class="col-md-12">
            <div class="text-center">
                Η Είσοδος στην Υπηρεσία πραγματοποιείται με προσωπικούς κωδικούς TaxisNet.
            </div>
         
        </div>
    </div>
</div>
<style>
    .panel {
        border: solid 1px #eee;
        box-shadow: 0 0 4px #bdbdbd;
    }
</style>
@endsection