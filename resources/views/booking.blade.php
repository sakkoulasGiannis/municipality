@extends('layouts.app')

@section('content')
    <bookingform :auth="{{json_encode($auth)}}"></bookingform>
   
   
   
   @if(env('FOOTERCONTENT', true))
   
    <div class="container">
        <div class="row apptitle blue-backgroun" >
            <div class="col-md-12 text-center">
                <h3>Η ηλεκτρονική εφαρμογή</h3>
                <h1>«Δημότης Μαλεβιζίου»</h1>
                <h3>λειτουργεί σε 24ωρη βάση</h3>
            </div>

        </div>
        <div class="row light-blue-background">
            <div class="col-md-3 offset-md-3">
                <a href="https://play.google.com/store/apps/details?id=gr.noveltech.cityzen.malevizi">
                    <img class="img-fluid" src="https://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dgr.noveltech.cityzen.malevizi&size=250x250&format=png&margin=5&color=000000&bgcolor=ffffff" alt=""><br/>
                    <img src="https://play.google.com/intl/en_us/badges/static/images/badges/el_badge_web_generic.png" class="img-fluid appimage google" alt="">
                </a>
            </div>

            <div class="col-md-3">
                <a href="https://apps.apple.com/app/id1503025503">
                    <img class="img-fluid" src="https://api.qrserver.com/v1/create-qr-code/?data=https%3A%2F%2Fapps.apple.com%2Fapp%2Fid1503025503&size=250x250&format=png&margin=5&color=000000&bgcolor=ffffff" alt=""><br/>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/500px-Download_on_the_App_Store_Badge.svg.png" class="img-fluid appimage apple" alt="">
                </a>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script src="{{ asset('js/manifest.js')}}"></script>
    <script src="{{ asset('js/vendor.js')}}"></script>
    <script src="{{ asset('js/app.js')}}?id={{time()}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.el.min.js')}}"></script>
@endsection
