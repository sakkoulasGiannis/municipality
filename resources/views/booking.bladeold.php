@extends('layouts.app')

@section('content')
    <bookingform></bookingform>
@endsection

@section('script')
    @include('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.el.min.js')}}"></script>
@endsection
