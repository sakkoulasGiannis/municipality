<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Entity;
class UserController extends Controller
{
    public function userIsOnline(){
        return Auth::user();
    }

    public function confirmEmail(){
        return view('/confirmemail');
    }

    public function updateUser(Request $request, User $user){
        if($request->has('entities') && count($request->get('entities')) > 0 ){
            $entites = [];
            $companies = [];
            foreach ($request->get('entities') as $e){


                $entity  = Entity::find($e);
                $company = $entity->company->id;

                if(!in_array($company, $companies)){
                    $companies[] = $company;
                }

                $entites[] = $e;

            }

            $user->entities()->sync($request->get('entities'));
            if(count($companies)>0 ){
                $user->companies()->sync($companies);
            }

        }
    }

    public function entities(User $user){
        return $user->entities;
    }
    public function entitiesId(User $user){
        return $user->entities()->pluck('id');
    }

    public function companies(User $user){
        return $user->companies;
    }

    public function companiesId(User $user){
        return $user->companies()->pluck('id');
    }

    public function history(){
        $auth = [];

        if(\Session::has('auth')){
            $auth = \Session::get('auth');
            $appointments = Appointment::with('entities')->where('email', trim($auth['email']))->get();
            return view('history', compact('auth', 'appointments'));
        }

//        return config('global.settings');

    }

    public function logout(){

        \Session::forget('auth');
        return redirect('/');

    }
}
