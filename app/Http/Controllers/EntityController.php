<?php

namespace App\Http\Controllers;

use App\Company;
use App\Entity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntityController extends Controller {

	public function index() {
		if (Auth::user()->hasRole('administrator')) {
			$entities = Entity::get();
		} else {
			$company = Auth::user()->companies->pluck('id');
			$entities = Entity::whereIn('company_id', $company)->get();
		}

		return view('entity', compact('entities'));
	}

	public function update(Entity $entity, Request $request) {
		$entity->update($request->get('entity'));
		$entity->services()->sync($request->get('services'));
		$timetable = $request->get('timetable');
		$timetableValues = [];
		$entity->timetable()->delete();
		foreach ($timetable as $t) {

			foreach ($t['hours'] as $ht) {

				if ($ht['open_at'] != null) {
					$values = [
						'day' => $t['day_number'],
						'open_at' => $ht['open_at'],
						'close_to' => $ht['close_to'],
						'slots' => $ht['slots'],
					];
					$timetableValues[] = $values;
					$entity->timetable()->create($values);
				}
			}
		}

		return $entity;
	}
	public function delete(Entity $entity) {
		$entity->delete();
		return 'success';
	}

	public function services(Entity $entity) {
		return $entity->services->pluck('id');
	}

	public function all_services(Entity $entity) {
		return $entity->services;
	}

	public function create(Request $request) {
		if ($request->has('company_id')) {
			$company = Company::find($request->get('company_id'));
		} else {
			$company = Auth::user()->company;
		}
		$company = $company->entities()->create($request->get('entity'));
		$company->update(['icalKey' => implode('', str_split(substr(strtolower(md5(microtime() . rand(1000, 9999))), 0, 30), 6))]);
		return $company;
	}

	public function json_format_for_company(Request $request) {
		$auth = Auth::user();
		if ($request->has('company_id') && $request->get('company_id') != null) {

			return Entity::with('company', 'timetable')->where('company_id', $request->get('company_id'))
				->whereIn('id', $auth->entities()->pluck('id'))
				->orderBy('title')
				->get();

		} elseif ($auth->hasRole('manager')) {

			return Entity::with('company', 'timetable')->whereIn('id', $auth->entities()->pluck('id'))
				->orderBy('title')
				->get();
//            return Entity::where('manager_id', $auth->id)->get();
		} elseif ($auth->hasRole('administrator')) {

			return Entity::with('company', 'timetable')->get();
		} else {
			$auth = Auth::user();
			return Entity::with('company', 'timetable')->where('company_id', $auth->company->id)->orderBy('title')->get();
		}

	}

	public function json_format() {

		$auth = Auth::user();
		if (!$auth) {
			return null;
		}
		if ($auth->hasRole('manager')) {

			return Entity::whereIn('company_id', $auth->companies()->pluck('id'))
				->whereIn('id', $auth->entities()->pluck('id'))
				->orderBy('title')
				->get();
		} else {
//            return Entity::where('company_id', $auth->company->id)->get();
		}
	}

	public function expertise_json() {
		return Entity::select('expertise')
			->groupBy('expertise')->get();
	}

	public function entities_json() {
		return Entity::select('id', 'title', 'expertise')->get();
	}

	public function expertise_json_by_company(Request $request) {
		if ($request->has('service_id') && $request->get('service_id')) {
			return Entity::with('timetable', 'services')->whereHas('services', function ($q) use ($request) {$q->where('id', $request->get('service_id'));})->get();
		} else {
			return Entity::with('timetable')->select('id', 'title', 'expertise', 'duration')->where('company_id', $request->get('company_id'))->get();
		}

//        return Entity::find('1')->timetable->select('id', 'title', 'expertise', 'duration')->get();
	}

	public function timetable(Entity $entity) {
		$timetables = $entity->timetable->groupBy('day');
		$days = [];
		$daysOfWeek = ['Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευ', 'Σαββάτο', 'Κυριακή'];

		foreach ($timetables as $key => $day) {
			$hours = [];
			foreach ($day as $item) {
				$hours[] = [
					'slots' => $item->slots,
					'open_at' => date('H:i', strtotime($item->open_at)),
					'close_to' => date('H:i', strtotime($item->close_to)),
				];
			}
			$days[] = [
				"day_number" => $key,
				"day" => $daysOfWeek[$key - 1],
				"hours" => $hours,
			];
		}

		if (count($days) > 0) {

//            foreach ($days as $td) {
			//                foreach ($daysOfWeek as $key => $d) {
			//                    if ($td['day'] == $d) {
			//                        continue 2;
			//                    } else {
			////                        $days[] = [
			////                            "day_number" => $key + 1,
			////                            "day" => $d,
			////                            "hours" => [
			////                               [ 'slots' => 1,
			////                                   'open_at' => '00:00',
			////                                   'close_to' => '00:00'
			////                               ]
			////                            ]
			////                        ];
			//                    }
			//                }
			//
			//            }
		} else {
			foreach ($daysOfWeek as $key => $d) {

				$days[] = [
					"day_number" => $key + 1,
					"day" => $d,
					"hours" => [
						['slots' => 1,
							'open_at' => '00:00',
							'close_to' => '00:00'],
					],
				];

			}
		}

		return $days;

		return $days;
	}
}
