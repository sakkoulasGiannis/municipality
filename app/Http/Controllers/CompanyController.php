<?php

namespace App\Http\Controllers;

use App\Company;
use App\Entity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CompanyController extends Controller
{


    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */

    public function create(Request $request)
    {

        $auth = Auth::user();
        $company = Company::create([
            'name' => Str::slug($request->get('name'), '-'),
            'title' => $request->get('title'),
        ]);
        $auth->company_id = $company->id;
        $auth->save();
        $auth->assignRole('administrator');

        return redirect('/calendar');
    }

    public function managers()
    {
        return view('managers');
    }

    public function updateManager(Request $request, User $user)
    {
        return $user;
        return $request->all();

    }

    public function update( $company, Request $request)
    {

        $company = Company::find($company);
       return $company->update($request->all());


    }



    public function companies()
    {
        return view('companies');
    }

    public function company(Company $company)
    {

        return view('company', compact('company'));
    }



    public function companies_entities_json(Request $request)
    {
        return Entity::where('company_id', $request->get('company_id'))->get();
    }

    public function company_json()
    {
        return Company::with('entities')->get();

    }

    public function company_json_tree()
    {
        return Company::with('entities')->get();
    }

    public function get_companies_entities_services(){
//        $companies = Company::with('entities.services')->get();
        $companies = Company::all();

        $data = [];
        foreach($companies as $company){
            $services = [];
            $timetable = [];
            foreach($company->entities()->with('timetable')->get() as $e){
                  $service = $e->services()->orderBy('title')->get(['title', 'id', 'documents', 'description', 'external_link']);
                  foreach($service as $s){
                    $services[$s->id] = $s;
                }

                $timetable[$e->id] = $e->timetable;
            }
            $return = array();
            array_walk_recursive($services, function($a) use (&$return) { $return[] = $a; });

            $columns = array_column($return, 'title');
            array_multisort($columns, SORT_ASC, $return);


            $data[] = [
                'company' => $company->title,
                'company_id' => $company->id,
                'timetable' => $timetable,
                'services' =>$return

            ];
        }
        return $data;
    }
    public function getManagers()
    {
        if (Auth::user()->hasRole('administrator')) {
            return User::with('entities', 'company')->get();
        }
        if (Auth::user()->hasRole('manager') && Auth::user()->hasPermissionTo('ViewEntites')) {
            return User::with('entities', 'company')->get();
        }
        if(Auth::user()->company){
            $company = Auth::user()->company;
            return User::where('company_id', $company->id)->get();
        }

    }

    public function destroy($id){

        $company = Company::find($id);

        return $company->delete();
    }

}
