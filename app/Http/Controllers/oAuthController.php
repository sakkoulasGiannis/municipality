<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class oAuthController extends Controller
{


    public function init()
    {


        if(env('GSISAUTH', true)){

        


        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
//            'clientId' => 'TP3UQR46439',   demo // The client ID assigned to you by the provider
            'clientId' => 'L8AWEWZ6439',    // The client ID assigned to you by the provider
//            'clientSecret' => 'MalTest@2021',  demo  // The client password assigned to you by the provider
            'clientSecret' => 'KM15A%05DAf',    // The client password assigned to you by the provider
            'redirectUri' => 'https://malevizi.booktool.gr/auth',
            'urlAuthorize' => 'https://www1.gsis.gr/oauth2server/oauth/authorize',
            'urlAccessToken' => 'https://www1.gsis.gr/oauth2server/oauth/token',
            'urlResourceOwnerDetails' => 'https://www1.gsis.gr/oauth2server/userinfo?format=xml'
        ]);

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $authorizationUrl = $provider->getAuthorizationUrl();

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

// Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
//die('sd');
            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }

            exit('Invalid state');

        } else {


            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            $token = $accessToken->getToken();

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www1.gsis.gr/oauth2server/userinfo?format=xml',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer " . $token,
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            //  print_r($response);
//return           $response = json_decode($response, true);


            $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            $array['userinfo']['@attributes'];
            $data = $array['userinfo']['@attributes'];


            $auth = '{
            "attributes": {
            "uid": ["' . $data['userid'] . '"],
            "preferredLanguage": "el"},
            "username": "' . $data['userid'] . '",
            "authenticator": "TaxisNet",
            "given_name": "' . $data['firstname'] . '",
            "family_name": "' . $data['lastname'] . '",
            "father_name": "' . $data['fathername'] . '",
            "mother_name": "' . $data['mothername'] . '",
            "birth_year": "' . $data['birthyear'] . '",
            "birthday": "",
            "id_card_number": "",
            "email": "",
            "phone_number": "",
            "vat": "' . $data['taxid'] . '"
            }';



            $auth = json_decode($auth, true);
            $auth = \Session::put('auth', $auth);
//            return \Session::get('auth');

            return redirect()->route('booking');


        }
    }else{
        return redirect()->route('login');
    }

    }


    public function auth_check(Request $request)
    {

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => '_358c589a83428e66cd518f6fa94ebf84d8048698e0',    // The client ID assigned to you by the provider
            'clientSecret' => '_88822fb09bcb865c144396310ddc32d85b3a6c7c03',   // The client password assigned to you by the provider
            'urlAuthorize' => 'https://ids.rethymno.gr/sso/module.php/oauth2/authorize.php',
            'urlAccessToken' => 'https://ids.rethymno.gr/sso/module.php/oauth2/access_token.php',
            'urlResourceOwnerDetails' => 'https://rethymno.booktool.gr',
            'redirectUri' => 'https://rethymno.booktool.gr/auth_check',
        ]);


        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            // $options = ['scope' => 'openid'];
            $options = ['scope' => request()->scope ?? 'basic'];
            $authorizationUrl = $provider->getAuthorizationUrl($options);

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }

            exit('Invalid state');
        } else {

            try {

                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);
                $token = $accessToken->getToken();

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://ids.rethymno.gr/sso/module.php/oauth2/userinfo.php',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer " . $token,
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                $response = json_decode($response, true);
                $auth = \Session::put('auth', $response);
                \Session::get('auth');
                return redirect()->route('booking')->with('auth', $response);
                dd($response);
                // We have an access token, which we may use in authenticated
                // requests against the service provider's API.
                // echo 'Access Token: ' . $accessToken->getToken() . "<br>";
                // echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
                // echo 'Expired in: ' . $accessToken->getExpires() . "<br>";
                // echo 'Already expired? ' . ($accessToken->hasExpired() ? 'expired' : 'not expired') . "<br>";

                // Using the access token, we may look up details about the
                // resource owner.
//                $resourceOwner = $provider->getResourceOwner($accessToken);

                //$url = "https://rethymno.booktool.gr://token#access_token={$accessToken->getToken()}&expires_in={$accessToken->getExpires()}";
                // return redirect()->away($url);


                return response()->json([
                    'userinfo' => $resourceOwner->toArray(),
                    'token' => $accessToken->getToken()
                ]);

                // The provider provides a way to get an authenticated API request for
                // the service, using the access token; it returns an object conforming
                // to Psr\Http\Message\RequestInterface.
                // $request = $provider->getAuthenticatedRequest(
                //     'GET',
                //     $auth2_config['userinfo_url'],
                //     $accessToken
                // );

            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                // Failed to get the access token or user details.
                exit($e->getMessage());
            }
        }
    }

    public function userInfo()
    {

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => '_358c589a83428e66cd518f6fa94ebf84d8048698e0',    // The client ID assigned to you by the provider
            'clientSecret' => '_88822fb09bcb865c144396310ddc32d85b3a6c7c03',   // The client password assigned to you by the provider
            'urlAuthorize' => 'https://ids.rethymno.gr/sso/module.php/oauth2/authorize.php',
            'urlAccessToken' => 'https://ids.rethymno.gr/sso/module.php/oauth2/access_token.php',
            'urlResourceOwnerDetails' => 'Rethimno',
            'redirectUri' => 'https://rethymno.booktool.gr/auth_check',
        ]);


        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            // $options = ['scope' => 'openid'];
            $options = ['scope' => request()->scope ?? 'basic'];
            $authorizationUrl = $provider->getAuthorizationUrl($options);

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }

            exit('Invalid state');
        } else {

            try {

                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                // We have an access token, which we may use in authenticated
                // requests against the service provider's API.
                // echo 'Access Token: ' . $accessToken->getToken() . "<br>";
                // echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
                // echo 'Expired in: ' . $accessToken->getExpires() . "<br>";
                // echo 'Already expired? ' . ($accessToken->hasExpired() ? 'expired' : 'not expired') . "<br>";

                // Using the access token, we may look up details about the
                // resource owner.
                $resourceOwner = $provider->getResourceOwner($accessToken);

                $url = "{$auth2_config['app_url_scheme']}://token#access_token={$accessToken->getToken()}&expires_in={$accessToken->getExpires()}";
                return redirect()->away($url);


                // The provider provides a way to get an authenticated API request for
                // the service, using the access token; it returns an object conforming
                // to Psr\Http\Message\RequestInterface.
                // $request = $provider->getAuthenticatedRequest(
                //     'GET',
                //     $auth2_config['userinfo_url'],
                //     $accessToken
                // );

            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                // Failed to get the access token or user details.
                exit($e->getMessage());
            }
        }
    }


}
