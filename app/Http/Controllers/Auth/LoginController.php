<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
//    protected function redirectTo()
//    {
//        return '/company';
//    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
//    protected $redirectTo = '/calendar';

    protected function authenticated(Request $request, $user)
    {

        
       
        if ( $user->hasRole(['manager', 'med_manager', 'administrator']) ) {// do your margic here
          
            return redirect()->route('calendar');
        }else{

            
            $auth = '{
                "attributes": {
                "uid": ["' . $user->id . '"],
                "preferredLanguage": "el"},
                "username": "' . $user->id . '",
                "authenticator": "auth",
                "given_name": "' . $user->name . '",
                "family_name": "' . $user->lastname . '",
                "father_name": "' . $user->fathers_name . '",
                "mother_name": "' . $user->mothers_name  . '",
                "birth_year": "",
                "birthday": "",
                "id_card_number": "",
                "email": "'.$user->email.'",
                "phone_number": "",
                "vat": "' . $user->vat . '"
                }';
    
 
    
                $auth = json_decode($auth, true);
                $auth = \Session::put('auth', $auth);
            return redirect()->route('booking');
        }

        // return redirect('/calendar');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
