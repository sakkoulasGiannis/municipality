<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Company;
use App\Entity;
use App\Http\Resources\AppointmentBResource;
use App\Http\Resources\AppointmentResource;
use App\Mail\DeleteAppointmentMail;
use App\Mail\NewAppointmentMail;
use App\Mail\UpdateAppointmentMail;
use App\User;
use Carbon\Carbon;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller {

	public function ical($key, Request $request) {
		if (!isset($key)) {
			return "nothing found";
		}
		$entity = Entity::where('icalKey', $key)->first();
		$calendarName = $entity->title;
		$vCalendar = new Calendar('www.booktool.gr');
		$vCalendar->setName($calendarName);
		$vCalendar->setTimezone('Europe/Athens');

		$appointments = Appointment::where('entity_id', $entity->id)->whereDate('created_at', '>=', \Carbon\Carbon::today()->subWeek())->get();
		foreach ($appointments as $a) {
			$vEvent = new Event($a->id);
			$start_at = Carbon::parse($a->start_date . ' ' . $a->start_time);
			$end_at = Carbon::parse($a->end_date . ' ' . $a->end_time);
			$vEvent
				->setDtStart($start_at)
				->setDtEnd($end_at)
				->setNoTime(false)
				->setSummary($a->title);

			$vCalendar->addComponent($vEvent);
		}

		header('Content-Type: text/calendar; charset=utf-8');
		header('Content-Disposition: attachment; filename="cal.ics"');
		return $vCalendar->render();
	}

	public function calendar() {
		$auth = Auth::user();

		if (!$auth) {
			return redirect('/login');
		} elseif (!$auth->company && $auth->hasRole('administrator')) {
			return redirect('/companies');
		} elseif ($auth && $auth->company) {
			$company = $auth->company;
			if ($company) {
				return redirect('show/' . $company->name);
			}
			return view('calendar');

		} elseif ($auth->hasRole('manager')) {
			return view('calendar');
		} elseif (!$auth->company) {
			return 'Please Contact with the Administrator';
		} elseif ($auth) {
			return view('calendar');
		}

	}

	public function spesificCalendar(Company $company) {

		$auth = Auth::user();
		if (!$auth) {
			return redirect('/login');
		} elseif ($auth) {
			return view('spesific-calendar', compact('company'));
		}
	}

	public function appointments_json(Request $request) {

		$auth = Auth::user();
		if (!$auth) {
			return redirect('/login');
		}
		if ($request->has('company_id') && $request->get('company_id') != null) {
			$company = [$request->get('company_id')];
		} elseif ($auth->hasRole('manager')) {
			$company = $auth->companies()->pluck('id');
		} else {
			$company = $auth->companies()->pluck('id');
		}

		if ($request->has('entity') && $request->entity != null) {

			return AppointmentResource::collection(Appointment::whereIn('company_id', $company)->where('entity_id', $request->entity)->whereBetween('start_date', [$request->start['date'], $request->end['date']])->get());
		}

		return AppointmentBResource::collection(Appointment::with('entities')
				->whereIn('company_id', $company)
				->whereIn('entity_id', $auth->entities()->pluck('id'))
				->whereBetween('start_date', [$request->start['date'], $request->end['date']])
				->get());
	}

	public function AppointmentAudits(Appointment $appointment) {
		return $appointment->audits()->with('user')->orderBy('created_at', 'desc')->get();
	}

	public function get(Appointment $appointment) {

		return Appointment::find($appointment->id);
		//@todo check if user can see endities
		if (!Auth::user()) {
			return null;
		}
		return $appointment;
	}

	public function create(Request $request) {

		$validator = Validator::make($request->all(), [
			'title' => ['required', 'string', 'max:255'],
			'duration' => ['required'],
			'start_date' => ['required'],
			'start_time' => ['required'],
			'end_date' => ['required'],
			'end_time' => ['required'],
		]);

		if ($validator->passes()) {

//            $company = Auth::user()->company;
			$entity = Entity::find($request->get('entity_id'));
			$company = $entity->company;
			$appointment = array_merge($request->all(), ['company_id' => $company->id]);

			$appointment = Appointment::create($appointment);

			$this->sendMail($appointment, $appointment->entities);

			return $appointment;
		}

		return response()->json(['error' => $validator->errors()->all()]);

	}

	public function update(Appointment $appointment, Request $request) {

		$appointment->update($request->all());
		return $appointment;
	}

	public function updateWithEmail(Appointment $appointment, Request $request) {

		$appointment->update($request->all());
		$this->sendUpdateMail($appointment, $appointment->entities);
		return $appointment;
	}

	/*
		     * save appointment from booking form
	*/
	public function saveAppointment(Request $request) {
		Appointment::create($request->all());
		return redirect('/booking/success');
	}

	public function successAppointment() {
		return view('success');
	}

	public function pendings() {
		if (Auth::user()->hasRole('administrator')) {

			return Appointment::where('approved', 0)
				->orderBy('start_date', 'asc')
				->get();
		} else {
			$company = Auth::user()->companies->pluck('id');
			return Appointment::whereId('company_id', $company)
				->orderBy('start_date', 'asc')
				->where('approved', 0)
				->get();
		}

	}

	public function booking() {

	 
		if (\Session::has('auth')) {
			$auth = \Session::get('auth');
// $auth = json_decode(json_encode($auth), true);

//  \Session::get('auth');
		} else {
			return 'please login';
 			// $auth = '{
            // "attributes": {
            // "uid": [
            // "119679877_taxisnet"
            // ],
            // "preferredLanguage": "el"
            // },
            // "username": "119679877_taxisnet",
            // "authenticator": "TaxisNet",
            // "given_name": "ΙΩΑΝΝΗΣ",
            // "family_name": "ΣΑΚΚΟΥΛΑΣ",
            // "father_name": "ΓΕΩΡΓΙΟΣ",
            // "mother_name": "ΕΥΡΙΔΙΚΗ",
            // "birth_year": "1981",
            // "birthday": "19811006",
            // "id_card_number": "χ357512",
            // "email": "sakkoulas@gmail.com",
            // "phone_number": "6936780044",
            // "vat": "119679877"
            // }';
			// $auth = json_decode($auth, true);
			// \Session::put('auth', $auth);

//            return json_encode(['status'=>false]);
		}
//return        $auth = $request->get();

 
		return view('booking', compact('auth'));
	}

	public function bookAppointment(Request $request) {

		$time = Carbon::parse($request->get('start_time'));
		$endTime = $time->addMinutes(30);
		$extraData = [

			'end_date' => $request->get('start_date'),
			'end_time' => $endTime->format('H:i'),
			'approved' => 1,
		];

		$appointment = array_merge($request->all(), $extraData);

//        $company = Company::where('id', $request->get('company_id'))->first();

		$appointment = Appointment::create($appointment);

		$this->sendMail($appointment, $appointment->entities);

		return $appointment;
	}

	public function joinMeetup(Request $request) {
		$roomname = $request->r;
		return view('meetup', compact('roomname'));
	}

	public function getAppointmentsForDay(Request $request) {

		$entity = Entity::find($request->get('entity_id'));
		$closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
			->where('start_date', $request->get('start_date'))
			->when($request->has('appointment_id') && $request->get('appointment_id') != '', function ($query) use ($request) {
				$query->where('id', '!=', $request->get('appointment_id'));
			})
			->get();

		$start = strtotime('06:00');
		$end = strtotime('23:05');

		$time = [];
		$duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;

		for ($i = $start; $i < $end; $i = $i + 5 * 60) {
			$stime = date('H:i', $i);
			$plusDurationMinutes = strtotime("+ {$duration} minutes", $i);
			$plusDurationMinutes;
			$a = $closeTimes
				->where('startTimeTimestamp', '<=', $plusDurationMinutes)
				->where('endTimeTimestamp', '>', $i)
				->first();
			if (!$a) {
				$time[] = '' . $stime;
			}
		}

		return $time;
	}

	
	
	public function getAppointmentsForBooking(Request $request) {

//        return $request->all();
		$entity = Entity::find($request->get('entity_id'));
		$timetable = $entity->timetable()->where('day', $request->get('dayNumber'))->first(['open_at', 'close_to', 'slots']);

		$closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
			->where('start_date', $request->get('start_date'))
			->when($request->has('appointment_id') && $request->get('appointment_id') != '', function ($query) use ($request) {
				$query->where('id', '!=', $request->get('appointment_id'));
			})
			->get();

	 
	 	$start = strtotime($timetable->open_at);
 		$end = strtotime($timetable->close_to ." + 15 minutes");
 		$firstRun = true;
		$time = [];
		  $duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;
  		for ($i = $start; $i < $end; $i = $i + (($duration )* 60)    ) {
			 
			 
		
			 
			$stime = date('H:i', $i);
			$send = date('H:i', $end);
 //			if($entity->gap_duration != null &&  $entity->gap_duration != ''){
//				$duration = $duration  ;
//			}
			
			
			$plusDurationMinutes = strtotime("+ " . $duration     . " minutes", $i);
			// $plusDurationMinutes;
			
			$foundCloseTimes = $closeTimes
				->where('startTimeTimestamp', '<=', $plusDurationMinutes)
				->where('endTimeTimestamp', '>', $i)
				->all();

			if ($this->isOpen($timetable, $i, $plusDurationMinutes, $foundCloseTimes)) {
				if (!$foundCloseTimes) {
					$time[] = '' . $stime;
				} else {

//                    return $foundCloseTimes;
					if ($timetable->slots > count($foundCloseTimes)) {
						$time[] = '' . $stime;
					} else {
						$time[] = 'x' . $stime;
					}
				}
			}
			
//						$i += $entity->gap_duration * 60;
	 		if($firstRun && $entity->gap_duration !='' && $entity->gap_duration != null) {
				 $i = $i + ($entity->gap_duration * 60);
			 }
			 $firstRun =   ($firstRun)?false:false;
		}

		return $time;
	}
	
	public function getAppointmentsForBookingOLD(Request $request) {

//        return $request->all();
		$entity = Entity::find($request->get('entity_id'));
		$timetable = $entity->timetable()->where('day', $request->get('dayNumber'))->first(['open_at', 'close_to', 'slots']);

		$closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
			->where('start_date', $request->get('start_date'))
			->when($request->has('appointment_id') && $request->get('appointment_id') != '', function ($query) use ($request) {
				$query->where('id', '!=', $request->get('appointment_id'));
			})
			->get();

	 
	 
		$start = strtotime('06:00');
		$end = strtotime('23:05');

		$time = [];
		  $duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;

		for ($i = $start; $i < $end; $i = $i + ($duration * 60) ) {
			
			$stime = date('H:i', $i);
			$send = date('H:i', $end);
			$plusDurationMinutes = strtotime("+ " . $duration . " minutes", $i);
			// $plusDurationMinutes;
			
			$foundCloseTimes = $closeTimes
				->where('startTimeTimestamp', '<=', $plusDurationMinutes)
				->where('endTimeTimestamp', '>', $i)
				->all();

			if ($this->isOpen($timetable, $i, $plusDurationMinutes, $foundCloseTimes)) {
				if (!$foundCloseTimes) {
					$time[] = '' . $stime;
				} else {

//                    return $foundCloseTimes;
					if ($timetable->slots > count($foundCloseTimes)) {
						$time[] = '' . $stime;
					} else {
						$time[] = 'x' . $stime;
					}
				}
			}
		}

		return $time;
	}

	public function isOpen($timetable, $start, $end, $foundCloseTimes) {
//        foreach ($timetable as $t) {

		if (strtotime($timetable->open_at) <= $start && strtotime($timetable->close_to) >= $end) {
			return true;
		}
//        }
		return false;
	}

	function calculateTime(Request $request) {
		$entity = Entity::find($request->get('entity_id'));
		$closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
			->where('start_date', $request->get('start_date'))
			->get();

		$start = strtotime('06:00');
		$end = strtotime('23:05');

		$time = [];
		$duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;

		for ($i = $start; $i < $end; $i = $i + 5 * 60) {
			$stime = date('H:i', $i);
			$plusDurationMinutes = strtotime("+ {$duration} minutes", $i);
			$a = $closeTimes
				->where('startTimeTimestamp', '<', $plusDurationMinutes)
				->where('endTimeTimestamp', '>', $i)
				->first();
			if (!$a) {
				$time[] = '' . $stime;
			}
		}

		return $time;

	}

	/*
		     * delete appointment from cliends on front end
	*/
	public function deleteAppointmentFromUser(Request $request) {
		$id = $request->get('appointment');
		if ($id != null && \Session::has('auth')) {
			$user = \Session::get('auth');
			$appointment = Appointment::find($id);
			if ($appointment->vat == trim($user['vat'])) {
				$this->delete($appointment);
				return ['status' => 'success'];
			}

			return ['status' => 'Δέν έχετε πρόσβαση για να διαγράψετε το Ραντεβού, Συνδεθείτε πάλι και ξανά προσπαθήστε , εάν το πρόβλημα εξακολουθεί να υπάρχει ακόμα παρακαλώ επικοινωνήστε με το αντίστοιχο τμήμα.'];
		}
	}

	public function delete(Appointment $appointment, Request $request) {

		$appointment->admin_comment = $request->get('admin_comment');
		$appointment->save();

		$appointment->delete();
		$this->sendDeleteMail($appointment, $appointment->entities);
		return true;
	}

	/*
		     * delete email
	*/
	protected function sendDeleteMail($appointment, $entity) {
		if ($appointment->email != null) {
			try {
				Mail::to($appointment->email)->send(new DeleteAppointmentMail($appointment, $entity, 'user'));
			} catch (\Exception $e) {
				// Get error here
			}

		}

		if (count($entity->users) > 0) {
			foreach ($entity->users as $user) {
				try {
					Mail::to($user->email)->send(new DeleteAppointmentMail($appointment, $entity, 'manager'));
				} catch (\Exception $e) {
					// Get error here
				}

			}
		}
		try {
			// Mail::to('sakkoulas@gmail.com')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
			// Mail::to('mitron@terranet.gr')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
		} catch (\Exception $e) {
			// Get error here
		}
	}

	protected function sendUpdateMail($appointment, $entity) {
		if ($appointment->email != null) {
			try {
				Mail::to($appointment->email)->send(new UpdateAppointmentMail($appointment, $entity, 'user'));
			} catch (\Exception $e) {
				// Get error here
			}

		}

		if (count($entity->users) > 0) {
			foreach ($entity->users as $user) {
				try {
					Mail::to($user->email)->send(new UpdateAppointmentMail($appointment, $entity, 'manager'));
				} catch (\Exception $e) {
					// Get error here
				}

			}
		}
		try {
			// Mail::to('sakkoulas@gmail.com')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
			// Mail::to('mitron@terranet.gr')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
		} catch (\Exception $e) {
			// Get error here
		}
	}

	protected function sendMail($appointment, $entity) {
		if ($appointment->email != null) {
			try {
				Mail::to($appointment->email)->send(new NewAppointmentMail($appointment, $entity, 'user'));
			} catch (\Exception $e) {
				// Get error here
			}

		}

		if (count($entity->users) > 0) {
			foreach ($entity->users as $user) {
				try {
					Mail::to($user->email)->send(new NewAppointmentMail($appointment, $entity, 'manager'));
				} catch (\Exception $e) {
					// Get error here
				}

			}
		}
		try {
			// Mail::to('sakkoulas@gmail.com')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
			// Mail::to('mitron@terranet.gr')->send(new NewAppointmentMail($appointment, $entity, 'manager'));
		} catch (\Exception $e) {
			// Get error here
		}
	}

}
