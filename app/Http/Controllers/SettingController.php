<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index(){
        $settings = Setting::first();
        return view('settings', compact('settings'));
    }
    public function update( Request $request){
        $setting = Setting::first();
        $setting->update($request->input());
        $setting->update;
    }

}
