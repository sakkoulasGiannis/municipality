<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Entity;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewAppointmentMail;
class DevController extends Controller
{
    public function testMail (){

        $appointment = Appointment::first();
        $entity = Entity::first();
        return new NewAppointmentMail($appointment, $entity, 'user');

        Mail::to('sakkoulas@mailtrap.com')->send(new NewAppointmentMail($appointment));

    }
}
