<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Resources\MediaCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
 class ServiceController extends Controller
{

    public function index(){
        $services = Service::all();
        return view('services.index');
    }

    public function edit(Service $service){
        return view('services.edit', compact('service'));
    }

    public function serviceMedia(Service $service){
        return $service->getMedia();
    }

    public function entities(Service $service){
        return $service->entities;
    }


    public function get_services(){
        return Service::orderBy('title')->get()->groupBy('title');
    }

    public function get_all_services(){
        return Service::orderBy('title')->get();
    }

    public function search(Request $request){
        $items = Service::where('title', 'like', $request->get('s'))
        ->orWhere('description', 'like', $request->get('s'));
        return $items;
    }

    public function new(){
        return view('services.new');
    }
    public function create(Request $request){
      return   $service = Service::create($request->input());
         return redirect('services/'.$service->id);
        return $service;
    }

    public function update(Service $service, Request $request){
        return $service->update($request->except(['id']));
    }

     public function destroy($id){
        $service = Service::find($id);
         $service->delete($id);
         return true;
     }

    public function upload(Service $service, Request $request){

        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf,doc, docx, xls, xlsx',

        ]);

        if($request->file()) {
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');


           $service->addMedia($request->file('file'))->usingName($request->get('name'))->toMediaCollection();

//            $fileUpload->name = time().'_'.$request->file->getClientOriginalName();
//            $fileUpload->path = '/storage/' . $file_path;
//            $fileUpload->save();

            return response()->json(['success'=>'File uploaded successfully.']);
        }
    }

    public function getMedia(Service $service){
//        return $service->getMedia();
        return new MediaCollection($service->getMedia());
        return $service->getMedia();
    }

    public function deleteMedia($id){

        return Media::find($id)->delete();
    }
}
