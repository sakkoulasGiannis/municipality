<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->title,
            'user_name' => $this->given_name ." ".$this->family_name,
            'comment' => $this->comment,
            'duration' => $this->duration,
            'start' => $this->start_date . " " . $this->start_time,
            'end' => $this->end_date . " " . $this->end_time,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
//            'end'=> ($this->end_date != null && $this->end_time != null)?$this->end_date . " ".$this->end_time: $this->start_date. " ". Carbon::parse($this->start_time)->addMinutes($this->duration)->format('H:i:s'),
            'color' => ($this->approved) ? $this->color : 'black',
        ];
    }
}
