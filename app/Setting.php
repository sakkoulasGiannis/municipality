<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'title',
        'sub_title',
        'homeDescription',
        'callMethod',
        'live',
        'clientId',
        'clientSecret',
        'urlAuthorize',
        'urlAccessToken',
        'urlResourceOwnerDetails',
        'redirectUri',
        'devClientId',
        'devClientSecret',
        'devUrlAuthorize',
        'devUrlAccessToken',
        'devUrlResourceOwnerDetails',
        'devRedirectUri',
        'phone',
        'phone_1',
        'email',
        'email_1',
        'address',
        'post_code',
        'city',
        'region',
        'general_data',
        'smtpHost',
        'smtpPort',
        'smtpEncryption',
        'smtpUsername',
        'smtpPassword',
        'send_from',
        'notify_entity_users'
    ];


}
