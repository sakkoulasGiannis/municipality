<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Appointment extends Model implements AuditableContract {
	use SoftDeletes;
	use Auditable;

	protected $fillable = ['entity_id', 'email', 'phone', 'user_id', 'company_id', 'title', 'comment', 'start_date', 'end_date', 'start_time', 'end_time', 'duration', 'color', 'approved', 'given_name',
		'family_name',
		'admin_comment',
		'father_name',
		'mother_name',
		'birthday',
		'id_card_number',
		'phone_number',
		'chat_url',
		'vat'];

	protected $appends = ['startTimeTimestamp', 'endTimeTimestamp'];

	public function getStartTimeTimestampAttribute() {
		return strtotime($this->start_time);
	}
	public function getEndTimeTimestampAttribute() {
		return strtotime($this->end_time);
	}

	public function getStartTimeAttribute($value) {
		return substr($value, 0, 5);
	}
	public function user() {
		return $this->belongsTo(User::class, 'id', 'user_id');
	}

	public function entities() {
		return $this->hasOne(Entity::class, 'id', 'entity_id');
	}
	public function entity() {
		return $this->belongsTo(Entity::class, 'id', 'entity_id');
	}

}
