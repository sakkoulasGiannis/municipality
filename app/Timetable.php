<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{

    protected $fillable = ['slots', 'day', 'open_at', 'close_to'];

    public function entity(){
        return $this->hasOne(Entity::class);
    }
}
