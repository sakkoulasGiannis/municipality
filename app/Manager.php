<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $fillable = ['name', 'email', 'password', 'company_id', 'password'];

    public function company(){
        return $this->belongsTo(Company::class, 'id', 'company_id');
    }
    public function appointments(){
        return $this->hasMany(appointments::class, 'manager_id', 'id');
    }
}
