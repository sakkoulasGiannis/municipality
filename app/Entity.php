<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model {
	protected $fillable = ['company_id', 'title', 'description', 'expertise', 'color', 'duration', 'gap_duration', 'enabled', 'icalKey', 'address'];

	public function services() {
		return $this->belongsToMany(Service::class);
	}

	public function users() {
		return $this->belongsToMany(User::class, 'entity_users');
	}

	public function company() {
		return $this->belongsTo(Company::class, 'company_id', 'id');
	}

	public function timetable() {
		return $this->hasMany(Timetable::class, 'entity_id', 'id');
	}

	public function close() {
		return $this->hasMany(Close::class, 'entity_id', 'id');
	}
}
