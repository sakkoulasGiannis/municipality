<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Entity;
class IcalKey extends Mailable
{
    use Queueable, SerializesModels;

    public $entity;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.icalKey')->with([
            'icalKey' => $this->entity->icalKey
        ]);;
    }
}
