<?php

namespace App\Mail;

use App\Appointment;
use App\Entity;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpdateAppointmentMail extends Mailable {
	use Queueable, SerializesModels;

	public $appointment;
	public $entity;
	public $role;
	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(Appointment $appointment, Entity $entity, $role) {
		$this->appointment = $appointment;
		$this->entity = $entity;
		$this->role = $role;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
//        return $this->from(env('EMAIL_SENT_FROM'),env('EMAIL_FROM_TEXT', false))->view('email.newAppointment')->subject(env('EMAIL_SUBJECT_NEW_APPOINTMENT', 'ΝΕΟ ΡΑΝΤΕΒΟΥ'));
		return $this->from(env('EMAIL_SENT_FROM'), env('EMAIL_FROM_TEXT', false) . ' ' . $this->entity->title)->view('email.UpdateAppointment')->subject(env('EMAIL_SUBJECT_UPDATE_APPOINTMENT', 'ΕΝΗΜΕΡΩΣΗ ΡΑΝΤΕΒΟΥ'));

	}
}
