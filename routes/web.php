<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

//Route::get('/auth', function () {
//    return Socialite::driver('gsis')->redirect();
//});

Route::get('/mail', 'DevController@testMail');

//Route::get('login', )
Route::get('/permission/{permissionName}', 'PermissionController@check');
//Route::get('/auth', 'oAuthController@userInfo');
Route::get('/auth', 'oAuthController@init');
Route::get('/authorize', 'oAuthController@userInfo');
//Route::get('/authorize', 'oAuthController@check_authorise');
Route::get('/auth_check', 'oAuthController@auth_check');
Route::get('/access_token', 'oAuthController@access_token');
Route::get('/ical/send/{entity}', 'IcalController@sendMail');
Route::get('/ical/{key}', 'AppointmentController@ical');
Route::get('/confirmemail', 'UserController@confirmEmail');
Route::get('/userisonline', 'UserController@userIsOnline');

Route::get('/booking', 'AppointmentController@booking')->name('booking');
Route::post('/booking/add', 'AppointmentController@saveAppointment');
Route::get('/booking/success', 'AppointmentController@successAppointment');
Route::get('/booking/history', 'UserController@history');
Route::post('/booking/history/deleteAppointment', 'AppointmentController@deleteAppointmentFromUser');
Route::get('/booking/logout', 'UserController@logout');

Route::get('/show/{company}', 'AppointmentController@spesificCalendar')->name('spesificCalendar');
Route::get('/company', 'CompanyController@company');
Route::post('/company', 'CompanyController@create')->name('createCompany');
Route::get('/company/managers', 'CompanyController@managers');
Route::get('/company/users', 'CompanyController@getManagers');
Route::get('/company/users/{user}/entities', 'UserController@entities');
Route::get('/company/users/{user}/entitiesId', 'UserController@entitiesId');
Route::get('/company/users/{user}/companiesId', 'UserController@companiesId');
Route::get('/company/users/{user}/companies', 'UserController@companies');
Route::post('/company/{id}/delete', 'CompanyController@destroy')->name('destroyCompany');
Route::post('/company/users/{user}', 'UserController@updateUser');
Route::put('/company/{company}', 'CompanyController@update');
Route::get('/company/{company}', 'CompanyController@company');

Route::post('/company/managers/create', 'ManagerController@create');
//Route::post('/company/{company}', 'CompanyController@get_compan');
Route::get('/companies/', 'CompanyController@companies');
Route::get('/companies_json', 'CompanyController@company_json');
Route::get('/companies_json_tree', 'CompanyController@company_json_tree');
Route::get('/get_companies_entities_services', 'CompanyController@get_companies_entities_services');
Route::post('/companies_entities_json', 'CompanyController@companies_entities_json');

Route::get('/entity', 'EntityController@index');
Route::post('/entity/create', 'EntityController@create');
Route::post('/entity/{entity}/update', 'EntityController@update');
Route::post('/entity/{entity}/delete', 'EntityController@delete');
Route::get('/entity/{entity}/services', 'EntityController@services');
Route::get('/entity/{entity}/all_services', 'EntityController@all_services');
Route::post('/entity/{company}', 'EntityController@update');
Route::get('/calendar', 'AppointmentController@calendar')->name('calendar');
Route::get('/calendar/pendings', 'AppointmentController@pendings');
Route::post('/calendar/appointments_json', 'AppointmentController@appointments_json');
Route::post('/calendar/{appointment}/audits', 'AppointmentController@AppointmentAudits');
Route::get('/calendar/get/{appointment}', 'AppointmentController@get');
Route::get('/calendar/{appointment}', 'AppointmentController@get');

Route::post('/appointment/', 'AppointmentController@create');
Route::post('/appointment/{appointment}/delete', 'AppointmentController@delete');
Route::post('/appointment/{appointment}', 'AppointmentController@update');
Route::post('/appointmentWithEmail/{appointment}', 'AppointmentController@updateWithEmail');

Route::get('/entities/timetable/{entity}', 'EntityController@timetable');
Route::get('/entities/json_format', 'EntityController@json_format');
Route::post('/entities/json_format', 'EntityController@json_format_for_company'); // use this to filter company entities
Route::get('/expertise_json', 'EntityController@expertise_json');
Route::post('/expertise_json_by_company', 'EntityController@expertise_json_by_company');
Route::get('/entities_json/{expertise}', 'EntityController@entities_json_by_expertise');

Route::post('/bookAppointment', 'AppointmentController@bookAppointment');
Route::get('meetup', 'AppointmentController@joinMeetup');
Route::post('/get-appointments-for-day', 'AppointmentController@getAppointmentsForDay');
Route::post('/get-appointments-for-booking', 'AppointmentController@getAppointmentsForBooking');
Route::get('/calculatetime', 'AppointmentController@calculateTime');

Route::post('/searchwhitelist', 'WhitelistController@searchEmail');
Route::post('/searchUser', 'WhitelistController@searchUser');

Route::get('/get_service_entities/{service}', 'ServiceController@entities');
Route::get('/get_service_media/{service}', 'ServiceController@serviceMedia');

Route::get('/get_services', 'ServiceController@get_services');
Route::get('/get_all_services', 'ServiceController@get_all_services');
Route::get('/services', 'ServiceController@index');
Route::post('/services', 'ServiceController@create');
Route::get('/services/new', 'ServiceController@new');
Route::get('/services/{service}', 'ServiceController@edit');
Route::put('/services/{service}', 'ServiceController@update');
Route::post('/upload/{service}', 'ServiceController@upload')->name('upload');
Route::get('/get_media/{service}', 'ServiceController@getMedia')->name('upload');
Route::delete('/service/{id}', 'ServiceController@destroy');
Route::delete('/media/{id}/delete', 'ServiceController@deleteMedia');
Route::get('/settings', 'SettingController@index');
Route::put('/settings', 'SettingController@update');

Route::get('/vue', function () {
	return view('index');
});
Route::group(['middleware' => ['auth.user']], function () {

});
Route::group(['middleware' => ['auth.admin']], function () {
	// login protected routes.
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
