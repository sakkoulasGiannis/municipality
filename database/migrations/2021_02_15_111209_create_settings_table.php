<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('logo')->nullable();
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->text('homeDescription')->nullable();
            $table->smallInteger('callMethod')->default(0);  // 0 ΓΓΠΣ , 1 ΡΕΘΥΜΟ ΤΥΠΕ , ec

            $table->boolean('live')->default(0);
            $table->string('clientId')->nullable();
            $table->text('clientSecret')->nullable();
            $table->text('urlAuthorize')->nullable();
            $table->text('urlAccessToken')->nullable();
            $table->text('urlResourceOwnerDetails')->nullable();
            $table->text('redirectUri')->nullable();

            $table->string('devClientId')->nullable();
            $table->text('devClientSecret')->nullable();
            $table->text('devUrlAuthorize')->nullable();
            $table->text('devUrlAccessToken')->nullable();
            $table->text('devUrlResourceOwnerDetails')->nullable();
            $table->text('devRedirectUri')->nullable();


            $table->string('phone')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('email')->nullable();
            $table->string('email_1')->nullable();
            $table->string('address')->nullable();
            $table->string('post_code')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();


            $table->string('smtpHost')->nullable();
            $table->string('smtpPort')->nullable();
            $table->string('smtpEncryption')->nullable();
            $table->string('smtpUsername')->nullable();
            $table->string('smtpPassword')->nullable();

            $table->string('send_from')->nullable();
            $table->boolean('notify_entity_users')->default(1);



            $table->json('general_data')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
