<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlotsInTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timetables', function (Blueprint $table) {
            $table->integer('slots')->unsigned()->default(1)->after('day');
        });

        Schema::table('appointments', function (Blueprint $table) {
            $table->string('chat_url')->nullable()->after('end_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timetables', function (Blueprint $table) {
            $table->dropColumn('slots');
        });
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropColumn('chat_url');
        });
    }
}
