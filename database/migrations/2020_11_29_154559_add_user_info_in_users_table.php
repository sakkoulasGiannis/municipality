<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserInfoInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropColumn('amka');
            $table->string('given_name')->nullable()->after('id');
            $table->string('family_name')->nullable()->after('given_name');
            $table->string('father_name')->nullable()->after('family_name');
            $table->string('mother_name')->nullable()->after('father_name');
            $table->string('birthday')->nullable()->after('mother_name');
            $table->string('id_card_number')->nullable()->after('birthday');
            $table->string('phone_number')->nullable()->after('id_card_number');
            $table->string('vat')->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->string('amka')->nullable()->after('id');

            $table->dropColumn('given_name');
            $table->dropColumn('family_name');
            $table->dropColumn('father_name');
            $table->dropColumn('mother_name');
            $table->dropColumn('birthday');
            $table->dropColumn('id_card_number');
            $table->dropColumn('phone_number');
            $table->dropColumn('vat');
        });
    }
}
