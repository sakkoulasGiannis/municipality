<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('lastname')->nullable()->after('name');
            $table->string('fathers_name')->nullable()->after('lastname');
            $table->string('mothers_name')->nullable()->after('fathers_name');
            $table->string('vat')->nullable()->after('mothers_name');
            $table->string('phone')->nullable()->after('mothers_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('lastname');
            $table->dropColumn('fathers_name');
            $table->dropColumn('mothers_name');
            $table->dropColumn('vat');
            $table->dropColumn('phone');
        });
    }
}
