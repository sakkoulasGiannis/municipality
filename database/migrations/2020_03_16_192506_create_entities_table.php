<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //@todo assign entity to multiple users
        Schema::create('entities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->required(); // belongs to many
            $table->integer('manager_id')->nullable(); // belongs to many
            $table->string('title')->required();
            $table->string('expertise')->nullable();
            $table->string('description')->nullable();
            $table->string('color')->default('blue');
            $table->boolean('enabled')->required()->default(1);
            $table->string('duration')->nullable(); // default minutes of an appointment , can be changed from managers on an appointment
            $table->string('gap_duration')->nullable(); // duration between two appointments
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
}
