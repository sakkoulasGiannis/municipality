<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Setting;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'title' => null,
            'sub_title' => null,
            'homeDescription' => null,
            'callMethod' => 0,
            'live' => 0,
            'clientId' => null,
            'clientSecret' => null,
            'urlAuthorize' => null,
            'urlAccessToken' => null,
            'urlResourceOwnerDetails' => null,
            'redirectUri' => null,

            'devClientId' => null,
            'devClientSecret' => null,
            'devUrlAuthorize' => null,
            'devUrlAccessToken' => null,
            'devUrlResourceOwnerDetails' => null,
            'devRedirectUri' => null,

            'phone' => null,
            'phone_1' => null,
            'email' => null,
            'email_1' => null,
            'address' => null,
            'post_code' => null,
            'city' => null,
            'region' => null,
            'general_data' => null,


        ]);
    }
}
