(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/auth.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/auth.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    source: String
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/bookingform.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-bootstrap-datetimepicker */ "./node_modules/vue-bootstrap-datetimepicker/dist/vue-bootstrap-datetimepicker.js");
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker/dist/locale */ "./node_modules/vuejs-datepicker/dist/locale/index.js");
/* harmony import */ var vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css */ "./node_modules/pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css");
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue2_timepicker_src__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue2-timepicker/src */ "./node_modules/vue2-timepicker/src/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Import date picker css



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['auth'],
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__["default"],
    datePicker: vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0___default.a,
    VueTimepicker: vue2_timepicker_src__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      user: null,
      e6: 1,
      en: vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__["en"],
      el: vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__["el"],
      stepFirstColum: 'col-md-4',
      setTypeOfAppointment: 1,
      createdAppointment: {},
      openDate: new Date(),
      chat_url: '',
      expertise: null,
      loadingAppointment: false,
      searchQuery: '',
      selectedDate: null,
      selectedDateOfAppointment: null,
      student_name: '',
      selectTime: null,
      start_date: new Date().toISOString().substr(0, 10),
      start_time: '12:30',
      title: null,
      comment: '',
      color: '#1976d2',
      company_id: null,
      entity_id: null,
      approved: false,
      email: '',
      phone: '',
      calendarActiveDays: [],
      highlighted: [],
      services: [],
      selectedService: null,
      selectedServiceMedia: [],
      emailfError: null,
      showLoginForm: false,
      stepTwo: false,
      step: 1,
      selectTimes: [],
      disabledDates: null,
      showCalendar: false,
      expertises: [],
      entities: [],
      entity: null,
      companies: [],
      date: new Date(),
      entityDays: [],
      userCompanies: null,
      options: {
        format: 'YYYY-MM-DD',
        useCurrent: false
      }
    };
  },
  methods: {
    getUser: function getUser() {
      axios.get('/userisonline').then(function (response) {
        this.user = response.data;

        if (this.user != '') {
          this.email = this.user.email;
          this.signIn();
        }
      }.bind(this))["catch"](function (error) {
        console.log(error);
      });
    },
    searchService: function searchService() {
      console.log(this.searchQuery);
    },
    scrollTo: function scrollTo(id) {
      var element = document.getElementById(id);
      element.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "start"
      });
    },
    signIn: function signIn() {
      var self = this;
      var whitelisted = false;
      this.stepTwo = false;
      this.emailfError = null; // no user found
    },
    getServices: function getServices() {
      this.showLoginForm = false;
      axios.get('/get_services').then(function (response) {
        this.services = response.data;
      }.bind(this))["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    get_service_media: function get_service_media() {
      axios.get('/get_service_media/' + this.selectedService.id).then(function (response) {
        this.selectedServiceMedia = response.data;
      }.bind(this))["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    get_service_entities: function get_service_entities() {
      axios.get('/get_service_entities/' + this.selectedService.id).then(function (response) {
        this.entities = response.data;
      }.bind(this))["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    findUser: function findUser() {
      this.showLoginForm = false;
      axios.post('/searchUser', {
        email: this.email
      }).then(function (response) {
        if (response.data.status && response.data.login == 'yes') {
          window.location.href = "/login";
        } else if (!response.data.status && response.data.login == 'no') {
          window.location.href = "/register";
        } else {
          this.stepTwo = true;
        }
      }.bind(this))["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    printDialog: function printDialog() {
      window.print();
    },
    dateIsSelected: function dateIsSelected(date) {
      this.selectedDateOfAppointment = moment(date).format('DD-MM-YYYY');
      var ndate = moment(date); // var searchDate = moment(date).format('YYYY-MM-DD');

      var dayNumber = ndate.day();
      var closeHours = []; // getAppointmentsForThisDay

      var self = this;
      axios.post('/get-appointments-for-booking', {
        'start_date': ndate.format('YYYY-MM-DD'),
        'entity_id': self.entity.id,
        'dayNumber': dayNumber
      }).then(function (response) {
        self.selectTimes = response.data;
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {
        return;
        var self = this;

        for (var e = 0; e < self.entities.length; e++) {
          if (this.entity_id == self.entity.id) {
            for (var a = 0; a < this.companies.length; a++) {
              console.log('this.companies[a]');
              console.log(this.companies[a]);

              if (this.companies[a].company_id == this.company_id) {
                if (this.companies[a].timetable[this.entity_id].length > 0) {
                  for (var d = 0; d < this.companies[a].timetable[this.entity_id].length; d++) {
                    if (this.companies[a].timetable[this.entity_id][d].day == dayNumber) {
                      var open_at = this.companies[a].timetable[this.entity_id][d].open_at;
                      var close_to = this.companies[a].timetable[this.entity_id][d].close_to;
                      var nndate = ndate.format('YYYY-MM-DD');
                      var startDateTime = moment(nndate + ' ' + open_at);
                      var endDateTime = moment(nndate + ' ' + close_to);
                      console.log(open_at);
                      console.log(close_to);
                      self.selectTimes = [];

                      if (!closeHours.includes(moment(startDateTime).format("HH:mm"))) {
                        self.selectTimes.push(moment(startDateTime).format("HH:mm"));
                      }

                      var count = 1;

                      while (startDateTime < endDateTime && count < 50) {
                        //@todo add 10 to douration
                        startDateTime = startDateTime.add(15, 'minutes');

                        if (closeHours.length > 0 && closeHours.includes(moment(startDateTime).format("HH:mm"))) {} else {
                          self.selectTimes.push(moment(startDateTime).format("HH:mm"));
                        }

                        count++;
                      }
                    }
                  }
                } else {
                  //@hide calendar;
                  this.showCalendar = false;
                }
              }
            }
          }
        }
      }.bind(this));
    },
    // get open days from entity timetable
    setTimeTable: function setTimeTable() {
      this.entityDays = [];
      console.log('this.entity.id');
      console.log(this.entity.id);

      for (var e = 0; e < this.entities.length; e++) {
        if (this.entity.id == this.entities[e].id) {
          for (var a = 0; a < this.companies.length; a++) {
            if (this.companies[a].company_id == this.company_id) {
              if (this.companies[a].timetable[this.entity.id] != undefined && this.companies[a].timetable[this.entity.id].length > 0) {
                console.log(this.companies[a].timetable[this.entity.id]);

                for (var d = 0; d < this.companies[a].timetable[this.entity.id].length; d++) {
                  this.entityDays.push(this.companies[a].timetable[this.entity.id][d].day);
                }
              } else {
                console.log(this.companies); //@hide calendar;

                this.showCalendar = false;
              }
            }
          }
        }
      }

      this.selectTimes = [];
      this.showCalendar = true;
      this.disabledDate();
    },
    setTime: function setTime(startTime, endTime) {// let date = "2017-03-13";
      // let timeAndDate = moment(date + ' ' + startTime)
      // while (i < 10) {
      //     text += "The number is " + i;
      //     i++;
      // }
      //
      // timeAndDate.add(30, 'minutes').format('HH:mm');;
    },
    activeDays: function activeDays(date) {},
    highlightedDates: function highlightedDates() {
      var start = moment(),
          // Sept. 1st
      end = moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'),
          days = this.entityDays; // selected day

      var result = [];

      for (var d = 0; d < days.length; d++) {
        var current = start.clone();

        while (current.day(7 + days[d]).isBefore(end)) {
          result.push(current.clone());
        }
      }

      var activedays = result.map(function (m) {
        return m.format('YYYY-MM-DD');
      });
      this.hhighlighted = activedays;
      this.hhighlighted.push({
        includeDisabled: true
      });
      return {
        customPredictor: function (date) {
          // return checkDate();
          var cardate = new Date(); //close past dates

          if (date.getTime() < cardate.getTime()) {
            return true;
          }

          if (!this.hhighlighted.includes(moment(date).format('YYYY-MM-DD'))) {
            console.log();
            return true;
          } // if (date.getDate() == 30 ) {
          //     return true
          // }

        }.bind(this)
      };
    },
    disabledDate: function disabledDate() {
      var start = moment(),
          // Sept. 1st
      end = moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'),
          days = this.entityDays; // selected day

      var result = [];

      for (var d = 0; d < days.length; d++) {
        var current = start.clone();
        console.log('days[d]');
        console.log(days[d]);

        while (current.day(7 + days[d]).isBefore(end)) {
          result.push(current.clone());
        }
      }

      var activedays = result.map(function (m) {
        return m.format('YYYY-MM-DD');
      });
      this.calendarActiveDays = activedays;
      return {
        customPredictor: function (date) {
          // return checkDate();
          var cardate = new Date(); //close past dates

          if (date.getTime() < cardate.getTime()) {
            return true;
          }

          if (!this.calendarActiveDays.includes(moment(date).format('YYYY-MM-DD'))) {
            console.log();
            return true;
          } // if (date.getDate() == 30 ) {
          //     return true
          // }

        }.bind(this)
      };
    },
    getEntityTimeTable: function getEntityTimeTable() {
      var self = this;
      axios.post('/expertise_json_by_company', {
        'company_id': self.company_id,
        'service_id': self.selectedService.id
      }).then(function (response) {
        console.log(response.data);
        self.entities = response.data;
        self.selectedDate = null;
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    getCompanies: function getCompanies() {
      var self = this;
      axios.get('/get_companies_entities_services').then(function (response) {
        console.log('companies');
        console.log(response.data);
        self.companies = response.data;
        console.log('companyWithServices');
        console.log(response.data);
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    postAppointment: function postAppointment() {
      this.loadingAppointment = true;

      if (this.setTypeOfAppointment == 2) {
        this.chat_url = 'https://meet.jit.si/' + Math.random().toString(5).substr(2, 35);
      }

      var self = this;
      axios.post('/bookAppointment', {
        given_name: self.auth.given_name,
        family_name: self.auth.family_name,
        father_name: self.auth.father_name,
        mother_name: self.auth.mother_name,
        vat: self.auth.vat,
        phone_number: self.auth.phone_number,
        id_card_number: self.auth.id_card_number,
        birthday: self.auth.birthday,
        start_date: moment(self.selectedDate).format('YYYY-MM-DD'),
        start_time: self.selectTime,
        title: self.title,
        comment: self.comment,
        color: self.color,
        email: self.email,
        company_id: self.company_id,
        entity_id: self.entity_id,
        approved: self.approved,
        phone: self.phone,
        chat_url: this.chat_url
      }).then(function (response) {
        console.log(response.data);
        this.createdAppointment = response.data;
        this.loadingAppointment = false;
      }.bind(this))["catch"](function (error) {
        console.log(error);
      }).then(function () {
        self.step = 5; // window.location = '/booking/success';
      });
    },
    //delete this
    getEntities: function getEntities() {
      console.log('create entity');
      var self = this;
      axios.get('/entities_json').then(function (response) {
        console.log(response.data);
        self.entities = response.data;
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    },
    getExpertises: function getExpertises() {
      console.log('create entity');
      var self = this;
      axios.get('/expertise_json_by_company').then(function (response) {
        console.log(response.data);
        self.expertises = response.data;
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {});
    }
  },
  computed: {
    resultCompanies: function resultCompanies() {
      if (this.searchQuery != '') {
        console.log('start search');
        var companies = this.companies; // Object.keys(this.companies).map((key) => [Number(key), this.companies[key]]);

        var newCompanies = [];

        for (var a = 0; a < this.companies.length; a++) {
          var insert = false;
          var services = [];

          for (var b = 0; b < this.companies[a].services.length; b++) {
            if (this.companies[a].services[b].title.toLowerCase().includes(this.searchQuery.toLowerCase())) {
              var insert = true;
              services.push(this.companies[a].services[b]);
              console.log(this.companies[a].services[b]);
            }
          }

          if (insert) {
            newCompanies.push({
              'company': this.companies[a].company,
              'company_id': this.companies[a].company_id,
              'services': services,
              'timetable': this.companies[a].timetable
            });
          }

          console.log(newCompanies);
          console.log(this.companies);
        }

        return newCompanies;
      } else {
        return this.companies;
      }
    },
    entityList: function entityList() {
      var _this = this;

      if (this.expertise == null || this.expertise == '') {
        return this.entities;
      }

      var items = this.entities;
      return this.entities.filter(function (item) {
        return item.expertise == _this.expertise;
      });
    }
  },
  created: function created() {
    this.getCompanies();
  },
  mounted: function mounted() {
    // var element = document.getElementById('start');
    // element.scrollIntoView({behavior: "smooth", block: "start", inline: "start"});
    // this.getEntities();
    // this.getExpertises();
    this.disabledDates = this.disabledDate();
    this.highlighted = this.highlightedDates();
    this.getServices();
    this.getUser();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/calendar.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var vue2_timepicker_src__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-timepicker/src */ "./node_modules/vue2-timepicker/src/index.js");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Vue.directive('can', function (el, binding) {
//   console.log('can');
//   console.log(el);
//   console.log(Laravel.permissions);
//   console.log(Laravel.permissions.indexOf(binding)  !== 1);
//   return 0;
//   return Laravel.permissions.indexOf(binding)  !== 1;
// })

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    navigation: _navigation__WEBPACK_IMPORTED_MODULE_0__["default"],
    VueTimepicker: vue2_timepicker_src__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: ['company'],
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    return {
      overlayLoading: false,
      startRemoveProcess: false,
      company_id: null,
      viewSingleDayDate: null,
      disablebuttonID: null,
      colormask: '#XXXXXXXX',
      pendings: [],
      EntityAvailiableTimeByDay: [],
      user: {},
      firstTime: '07:00',
      showPendings: false,
      menu2: false,
      date: new Date().toISOString().substr(0, 10),
      items: [{
        href: '/calendar',
        icon: 'mdi-chart-bubble',
        text: 'Διαχείρηση'
      }, {
        href: '#',
        icon: 'mdi-bicycle-basket',
        text: 'Ραντεβού'
      }, // { href:'#', icon: 'mdi-account', text: 'Επισκέπτες' },
      // { href:'#', icon: 'mdi-wallet-giftcard', text: 'Στατιστικά' },
      // { href:'#', icon: 'mdi-shape', text: 'Σημεία Ραντεβού' },
      {
        href: '#',
        divider: true
      }, // { href:'#', icon: 'lightbulb_outline', text: 'Προφίλ' },
      {
        href: '/company/managers',
        icon: 'mdi-account',
        text: 'Διαχειριστές'
      }, {
        href: '#',
        divider: true
      }, {
        href: '/company',
        icon: 'settings',
        text: 'Εταιρία'
      }, {
        href: '#',
        icon: 'chat_bubble',
        text: 'Διαγραμμένα Ραντέβου'
      }],
      //menu
      drawer: null,
      editAppointment: false,
      mask: '##:##',
      entityDialog: false,
      notifications: false,
      sound: true,
      widgets: false,
      menu: false,
      menu_end: false,
      menuend_date: false,
      on: false,
      showAudits: false,
      appointmentAudits: [],
      appointment: {
        'email': '',
        'phone': '',
        'start_date': '',
        'start_time': null,
        'title': 'test titles',
        'comment': 'lorem ipsum',
        'color': '#1976d2',
        'duration': 30,
        'user_id': 1,
        'entity_id': 1,
        'approved': false
      },
      //update appointment
      current_appointment: [],
      rightPanel: false,
      picker: null,
      flat: true,
      sidepanel: true,
      //calendar variables
      typeToLabel: {
        month: 'ΜΗΝΑΣ',
        week: 'ΕΒΔΟΜΑΔΑ',
        day: 'ΗΜΕΡΑ'
      },
      focus: '',
      month: "",
      today: null,
      start: null,
      end: null,
      type: 'month',
      types: ['month', 'week', 'day', '4day'],
      mode: 'stack',
      modes: ['stack', 'column'],
      weekday: [0, 1, 2, 3, 4, 5, 6],
      weekdays: [{
        text: 'Mon - Sun',
        value: [1, 2, 3, 4, 5, 6, 0]
      }, {
        text: 'Mon - Fri',
        value: [1, 2, 3, 4, 5]
      }],
      value: '',
      entity: {
        'title': 'test',
        'description': 'test',
        'color': 'red',
        'duration': '60'
      },
      selectedEvent: {},
      selectedElement: null,
      selectedOpen: false,
      events: [],
      userCompanies: [],
      selectedCompanie: null,
      entities: [],
      services: [],
      selectedEntity: {
        title: '',
        color: 'blue',
        id: null
      },
      colors: ['blue', 'indigo', 'deep-purple', 'cyan', 'green', 'orange', 'grey darken-1'],
      names: ['Meeting', 'Holiday', 'PTO', 'Travel', 'Event', 'Birthday', 'Conference', 'Party']
    };
  },
  methods: {
    getAvailableTimesForEntityByDay: function getAvailableTimesForEntityByDay() {
      console.log('getAvailableTimesForEntityByDay');
      console.log(this.appointment);
      var closeHours = [];
      var self = this;
      axios.post('/get-appointments-for-day', {
        'start_date': this.appointment.start_date,
        'entity_id': this.appointment.entity_id,
        'appointment_id': this.appointment.id,
        duration: this.appointment.duration
      }).then(function (response) {
        self.EntityAvailiableTimeByDay = response.data; // self.appointment.start_time = response.data['start_time'][0];

        var time = moment(self.appointment.start_time, 'HH:mm');
        time.add(self.appointment.duration, 'minutes');
        self.getServicesByEntity();
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {}.bind(this));
    },
    filterEntities: function filterEntities() {
      // selectedCompanie
      console.log('selected cp[ane');
      console.log(this.selectedCompanie);
      this.getEntities(this.selectedCompanie);
    },
    getUserCompanies: function getUserCompanies() {
      var self = this;
      axios.get('/company/users/' + this.user.id + '/companies').then(function (response) {
        // handle success
        self.userCompanies = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    getUser: function getUser() {
      axios.get('/userisonline').then(function (response) {
        this.user = response.data;
        this.getUserCompanies();
      }.bind(this))["catch"](function (error) {
        console.log(error);
      });
    },
    setEntity: function setEntity() {
      var entity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      if (entity == null) {
        this.selectedEntity = {
          title: '',
          color: 'blue',
          id: null
        };
      } else {
        this.appointment.entity_id = this.entity.id;
        this.selectedEntity = entity;
      }

      this.rightPanel = false;
      this.getEvents(this.start, this.end, entity);
    },
    openPendings: function openPendings() {
      this.editAppointment = true;
      this.showPendings = true;
    },
    getPendings: function getPendings(value) {
      this.pendings = value;
    },
    setToday: function setToday() {
      this.focus = this.today;
    },
    myDate: function myDate(day) {
      this.type = 'day';
      this.viewSingleDayDate = day.date;
    },
    createEntity: function createEntity() {
      console.log('create entity');
      var self = this;
      axios.post('/entity/create', self.entity).then(function (response) {
        // handle success
        console.log(response.data);
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.entityDialog = false;
        self.getEntities();
      });
    },
    createAppointment: function createAppointment() {
      this.editAppointment = false;
      this.overlayLoading = true;
      var self = this; // self.appointment.end_time

      axios.post('/appointment', self.appointment).then(function (response) {
        // handle success
        if (response.data.error !== undefined) {
          self.snackbar = true;
          var alertMessage = response.data.error;

          for (var a = 0; a < alertMessage.length; a++) {
            self.$toastr('error', alertMessage[a], 'Βρέθηκε το παρακάτω πρόβλημα');
            self.overlayLoading = false;
          }
        } else {
          self.rightPanel = false;
          self.overlayLoading = false;
          self.$toastr('info', 'Δημιουργήθηκε Νεο Ραντεβού', response.data.title);
          this.$forceUpdate();
        }
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.getEvents(self.start, self.end);
      });
    },
    getAudits: function getAudits() {
      var self = this; //close right panel

      this.current_appointment = null;
      axios.post('/calendar/' + self.appointment.id + '/audits').then(function (response) {
        // handle success
        console.log('audtis');
        console.log(response.data);
        self.appointmentAudits = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    getAppointment: function getAppointment(data) {
      var self = this; //close right panel

      this.rightPanel = false;
      this.current_appointment = null;
      axios.get('/calendar/' + data.id).then(function (response) {
        self.appointment = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {//open right panel
      });
    },
    removeAppointment: function removeAppointment() {
      this.overlayLoading = true;
      var self = this;
      axios.post('/appointment/' + self.appointment.id + '/delete', self.appointment).then(function (response) {
        // handle success
        self.$toastr('info', 'Διαγραφή Ραντεβού');
        self.rightPanel = false;
        self.editAppointment = false;
        self.getEvents(self.start, self.end);
        self.$refs.reloadPendings.getPendingAppointments();
        self.overlayLoading = false;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    updateAppointmentSendEmail: function updateAppointmentSendEmail() {
      this.overlayLoading = true;
      var self = this;
      axios.post('/appointmentWithEmail/' + self.appointment.id, self.appointment).then(function (response) {
        // handle success
        self.rightPanel = false;
        self.editAppointment = false;
        self.getEvents(self.start, self.end);
        self.$refs.reloadPendings.getPendingAppointments();
        console.log(response.data);
        self.overlayLoading = false;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    updateAppointment: function updateAppointment() {
      this.overlayLoading = true;
      var self = this;
      axios.post('/appointment/' + self.appointment.id, self.appointment).then(function (response) {
        // handle success
        console.log(response.data);
        self.overlayLoading = false;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.rightPanel = false;
        self.editAppointment = false;
        self.getEvents(self.start, self.end);
        self.$refs.reloadPendings.getPendingAppointments();
      });
    },
    editAppointmentEntry: function editAppointmentEntry() {
      this.selectedOpen = false;
      this.rightPanel = true;
      this.editAppointment = true;
    },
    newAppointment: function newAppointment() {
      this.rightPanel = true;
      var today = new Date();
      console.log('this.selectedEntity.id');
      console.log(this.selectedEntity.id);
      this.appointment = {
        'start_date': this.viewSingleDayDate != null ? this.viewSingleDayDate : new Date().toISOString().substr(0, 10),
        'end_date': this.viewSingleDayDate != null ? this.viewSingleDayDate : new Date().toISOString().substr(0, 10),
        'start_time': '',
        'email': '',
        'phone': '',
        'title': '',
        'comment': '',
        'color': '#1976d2',
        'duration': 30,
        'entity_id': this.selectedEntity.id != undefined && this.selectedEntity.id != null ? this.selectedEntity.id : null,
        'approved': false
      };
      this.getAvailableTimesForEntityByDay();
      this.rightPanel = true;
    },
    getServicesByEntity: function getServicesByEntity() {
      var self = this;
      axios.get("/entity/" + self.appointment.entity_id + "/all_services").then(function (response) {
        self.services = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    getServices: function getServices() {
      var self = this;
      axios.get('/get_all_services').then(function (response) {
        self.services = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    getEntities: function getEntities() {
      var companyId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      if (companyId != null) {
        var company = companyId;
      } else if (this.company_id != null) {
        var company = this.company_id;
      }

      console.log('company');
      console.log(company);
      var self = this;
      axios.post('/entities/json_format', {
        company_id: company
      }).then(function (response) {
        // handle success
        self.entities = response.data;
        self.getEvents(self.start, self.end);
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    getEvents: function getEvents(start, end) {
      var entity = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var self = this;

      if (this.selectedCompanie != null) {
        this.company_id = this.selectedCompanie;
      } else if (this.company.id !== undefined) {
        this.company_id = this.company.id;
      }

      if (entity != null) {
        entity = entity.id;
      } else if (this.selectedEntity.id != undefined) {
        entity = this.selectedEntity.id;
      }

      axios.post('/calendar/appointments_json', {
        company_id: self.company_id != null ? self.company_id : self.selectedCompanie ? self.selectedCompanie : null,
        start: start,
        end: end,
        entity: entity
      }).then(function (response) {
        // handle success
        console.log(response.data);
        self.events = response.data.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      }); // this.events = events
    },
    getEventColor: function getEventColor(event) {
      return event.color;
    },
    rnd: function rnd(a, b) {
      return Math.floor((b - a + 1) * Math.random()) + a;
    },
    formatDate: function formatDate(a, withTime) {
      return withTime ? "".concat(a.getFullYear(), "-").concat(a.getMonth() + 1, "-").concat(a.getDate(), " ").concat(a.getHours(), ":").concat(a.getMinutes()) : "".concat(a.getFullYear(), "-").concat(a.getMonth() + 1, "-").concat(a.getDate());
    },
    nth: function nth(d) {
      return d > 3 && d < 21 ? 'th' : ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'][d % 10];
    },
    viewDay: function viewDay(_ref) {
      var date = _ref.date;
      this.focus = date;
      this.type = 'day';
    },
    prev: function prev() {
      this.$refs.calendar.prev();
    },
    next: function next() {
      this.$refs.calendar.next();
    },
    editPending: function editPending(event) {
      console.log('edit pending');
      console.log(event);
      this.appointment = event;
      this.editAppointmentEntry();
    },
    showEvent: function showEvent(_ref2) {
      var nativeEvent = _ref2.nativeEvent,
          event = _ref2.event;
      console.log('event');
      console.log(event);
      var self = this;
      var responseData = {}; // this.getAvailableTimesForEntityByDay();
      //close right panel

      this.rightPanel = false;
      this.current_appointment = null;
      console.log('event id');
      console.log(event.id);
      axios.get('/calendar/get/' + event.id).then(function (response) {
        // handle success
        console.log('response');
        console.log(response);
        responseData = response.data;
        self.selectedEvent = response.data;
        self.appointment = response.data;
        self.getAvailableTimesForEntityByDay();
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        //open right panel
        var open = function open() {
          console.log('before pop up');
          console.log(responseData);
          self.selectedElement = nativeEvent.target;
          setTimeout(function () {
            return self.selectedOpen = true;
          }, 50);
        };

        if (self.selectedOpen) {
          self.selectedOpen = false;
          setTimeout(open, 50);
        } else {
          open();
        }
      });
    },
    updateRange: function updateRange(_ref3) {
      var start = _ref3.start,
          end = _ref3.end;
      console.log('update range');
      var min = new Date("".concat(start.date, "T00:00:00"));
      var max = new Date("".concat(end.date, "T23:59:59"));
      var days = (max.getTime() - min.getTime()) / 86400000;
      var eventCount = this.rnd(days, days + 20);
      var events = [];
      this.start = start;
      this.end = end;
      this.getEvents(start, end);
    },
    locationHashChanged: function locationHashChanged() {
      if (location.hash === '#ekremotites') {
        console.log("You're visiting a cool feature!");
      }
    }
  },
  watch: {
    'appointment.duration': function appointmentDuration(newValue, oldValue) {
      var time = moment(this.appointment.start_time, 'HH:mm');
      time.add(newValue, 'minutes');
      this.appointment.end_time = time.format("HH:mm");
    },
    'appointment.start_time': function appointmentStart_time(newValue, oldValue) {
      console.log(newValue);
      var time = moment(newValue, 'HH:mm');
      time.add(this.appointment.duration, 'minutes');
      var startTime = this.appointment.start_time;
      var durationInMinutes = this.appointment.duration;
      var endTime = moment(startTime, 'HH:mm:ss').add(durationInMinutes, 'minutes').format('HH:mm');
      this.appointment.end_time = endTime;
    },
    'appointment.start_date': function appointmentStart_date(newValue, oldValue) {
      // var startDate = new Date(newValue); //dd-mm-YYYY
      // var endDate = new Date(this.appointment.end_date);
      // if(startDate >= endDate){
      this.appointment.end_date = newValue; // }

      if (this.rightPanel == true) {
        this.getAvailableTimesForEntityByDay();
      }
    },
    'rightPanel': function rightPanel(newValue) {// if(newValue == true){
      //   this.getAvailableTimesForEntityByDay();
      // }
    },
    'appointment.end_date': function appointmentEnd_date(newValue, oldValue) {
      var endDate = new Date(newValue); //dd-mm-YYYY

      var startDate = new Date(this.appointment.start_date);

      if (endDate > startDate || endDate < startDate) {
        this.appointment.end_date = this.appointment.start_date;
      }
    }
  },
  computed: {
    approvedMessage: function approvedMessage() {
      if (this.appointment.approved) {
        return 'Έχει εγκριθεί';
      }

      return 'Μη Εγκεκριμένο';
    },
    swatchStyle: function swatchStyle() {
      var color = this.color,
          menu = this.menu;
      return {
        backgroundColor: this.appointment.color,
        cursor: 'pointer',
        height: '30px',
        width: '30px',
        borderRadius: menu ? '50%' : '4px',
        transition: 'border-radius 200ms ease-in-out'
      };
    },
    title: function title() {
      var start = this.start,
          end = this.end;

      if (!start || !end) {
        return 'Nothing';
      }

      var startMonth = this.monthFormatter(start);
      var endMonth = this.monthFormatter(end);
      var suffixMonth = startMonth === endMonth ? '' : endMonth;
      var startYear = start.year;
      var endYear = end.year;
      var suffixYear = startYear === endYear ? '' : endYear;
      var startDay = start.day + this.nth(start.day);
      var endDay = end.day + this.nth(end.day);

      switch (this.type) {
        case 'month':
          return "".concat(startMonth, " ").concat(startYear);

        case 'week':
        case '4day':
          return "".concat(startMonth, " ").concat(startDay, " ").concat(startYear, " - ").concat(suffixMonth, " ").concat(endDay, " ").concat(suffixYear);

        case 'day':
          return "".concat(startMonth, " ").concat(startDay, " ").concat(startYear);
      }

      return '';
    },
    monthFormatter: function monthFormatter() {
      return this.$refs.calendar.getFormatter({
        timeZone: 'UTC',
        month: 'long'
      });
    }
  },
  mounted: function mounted() {
    this.getUser();
    this.getEntities();
    this.getServices();
    window.onhashchange = this.locationHashChanged();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/companies.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      companies: [],
      newCompanyName: null,
      newCompanyTitle: null,
      newCompanyDialog: false,
      newCompanyError: null
    };
  },
  methods: {
    getCompanies: function getCompanies() {
      var self = this;
      axios.get('/companies_json').then(function (response) {
        // handle success
        console.log(response.data);
        self.companies = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    createCompany: function createCompany() {
      var self = this;

      if (self.newCompanyName == null || self.newCompanyName == '' || self.newCompanyTitle == null || self.newCompanyTitle == '') {
        self.newCompanyError = 'Παρακαλώ συμπληρώστε όλα τα στοιχεία της φόρμας.';
        return;
      }

      axios.post('/company', {
        name: self.newCompanyName,
        title: self.newCompanyTitle
      }).then(function (response) {
        // handle success
        console.log(response.data);
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.entityDialog = false;
        self.getCompanies();
        self.newCompanyDialog = false;
        self.newCompanyError = null;
      });
    },
    showCalendar: function showCalendar(item) {
      window.location = '/show/' + item.name;
    },
    editcompany: function editcompany(company) {
      window.location = '/company/' + company.name;
    },
    deleteCompany: function deleteCompany(company) {
      axios.post('/company/' + company.id + "/delete").then(function (response) {
        // handle success
        this.getCompanies();
        console.log(response.data);
      }.bind(this))["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// window.location = '/companies'
      });
    }
  },
  computed: {},
  mounted: function mounted() {
    this.getCompanies();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['company'],
  data: function data() {
    return {
      companies: [],
      newCompanyName: null,
      newCompanyTitle: null,
      entities: []
    };
  },
  methods: {
    getEntities: function getEntities() {
      var self = this;
      axios.post('/companies_entities_json', {
        'company_id': self.company.id
      }).then(function (response) {
        // handle success
        console.log('entitieues');
        console.log(response.data);
        self.entities = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    }
  },
  computed: {},
  mounted: function mounted() {
    this.getEntities();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company/edit.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/dist/quill.core.css */ "./node_modules/quill/dist/quill.core.css");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! quill/dist/quill.bubble.css */ "./node_modules/quill/dist/quill.bubble.css");
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__["quillEditor"]
  },
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  props: ['company'],
  data: function data() {
    return {
      file: {
        name: '',
        file: '',
        success: ''
      }
    };
  },
  methods: {
    update: function update() {
      axios.put('/company/' + this.company.id, this.company).then(function (response) {
        console.log('success');
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/entities.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers


/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  props: ['company'],
  data: function data() {
    return {
      headers: [{
        text: 'Επιλογή',
        align: 'start',
        sortable: false,
        value: 'id'
      }, {
        text: 'Τίτλος',
        align: 'start',
        sortable: false,
        filterable: true,
        value: 'title'
      }],
      showTimeTable: false,
      search: '',
      updateResponseAllertText: '',
      createEntityForm: false,
      updateEntityForm: false,
      updateResponseAllert: false,
      selectedServices: [],
      selectedRows: [],
      services: {},
      company_id: null,
      setEntityForUpdate: [],
      menuUpdate: false,
      colormaskUpdate: '!#XXXXXX',
      entityDialog: false,
      selectedCompany: null,
      companies: [],
      menu: false,
      colormask: '!#XXXXXX',
      entity: {
        title: null,
        description: null,
        color: '#303F9F',
        expertise: null,
        duration: 60
      },
      snackbar: true,
      entities: [],
      alertMessage: [],
      vertical: true,
      alert: true,
      valid: true,
      drawer: true,
      date: new Date().toISOString().substr(0, 10),
      manager: {
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
      },
      managers: [],
      showPassA: false,
      showPassB: false,
      rules: {
        required: function required(value) {
          return !!value || 'Required.';
        },
        min: function min(v) {
          return v.length >= 8 || 'Min 8 characters';
        },
        emailMatch: function emailMatch() {
          return 'The email and password you entered don\'t match';
        }
      },
      timetable: []
    };
  },
  methods: {
    deleteEntity: function deleteEntity(id) {
      if (confirm('Διαγραφή Υπηρεσίας;')) {
        axios.post('/entity/' + id + "/delete").then(function (response) {
          // handle success
          this.getEntities();
        }.bind(this))["catch"](function (error) {
          // handle error
          console.log(error);
        }).then(function () {// window.location = '/companies'
        });
      }
    },
    updatedTimetableValues: function updatedTimetableValues(value) {
      this.timetable = value;
    },
    copyIcalKey: function copyIcalKey(key) {
      if (key == null) {
        alert('Παρακαλώ επικοινωνήστε με τον Διαχειριστή');
        return;
      }
      /* Get the text field */


      this.$copyText('https://medical.booktool.gr/ical/' + key).then(function (e) {
        alert('To Url για την προσθήκη νέου ημερολογίου έχει αντιγραφεί στο πρόχειρο');
        console.log(e);
      }, function (e) {
        alert('Can not copy');
        console.log(e);
      });
    },
    loadTimeTable: function loadTimeTable(id) {
      var self = this;
      console.log('id');
      console.log(id);
      axios.get('/entities/timetable/' + id).then(function (response) {
        // handle success
        console.log(response.data);
        self.timetable = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    getCompanies: function getCompanies() {
      var self = this;
      axios.get('/companies_json').then(function (response) {
        // handle success
        console.log(response.data);
        self.companies = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    updateEntity: function updateEntity() {
      console.log('create entity');
      var self = this;
      axios.post('/entity/' + self.setEntityForUpdate.id + '/update', {
        'entity': self.setEntityForUpdate,
        'services': self.selectedServices,
        'timetable': self.timetable
      }).then(function (response) {
        // handle success
        self.updateResponseAllert = true;
        self.updateResponseAllertText = 'Επιτυχής Ενημέρωση';
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
        console.log(response.data);
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.entityDialog = false;
        self.getEntities();
      });
    },
    createEntity: function createEntity() {
      // var entity =  {
      //      title: this.entity.title,
      //      description: this.entity.description,
      //      color:this.entity.color,
      //      expertise: this.entity.expertise,
      //      duration: this.entity.duration
      //  },
      var self = this;
      axios.post('/entity/create', {
        company_id: self.selectedCompany,
        entity: self.entity
      }).then(function (response) {
        // handle success
        console.log(response.data);
        self.createEntityForm = false;
        self.updateResponseAllert = true;
        self.updateResponseAllertText = 'Επιτυχής Δημιουργία';
        self.getEntities();
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.entityDialog = false;
        self.getEntities();
      });
    },
    getServices: function getServices() {
      var self = this; // axios.get('/entities/json_format')

      axios.get('/get_all_services').then(function (response) {
        // handle success
        self.services = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    },
    getEntities: function getEntities() {
      var self = this; // axios.get('/entities/json_format')

      axios.post('/entities/json_format', {
        company_id: self.company_id
      }).then(function (response) {
        // handle success
        console.log('entitieues');
        console.log(response.data);
        self.entities = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    },
    loadEntityServices: function loadEntityServices(setEntityForUpdate) {
      axios.get('/entity/' + this.setEntityForUpdate.id + '/services').then(function (response) {
        // handle success
        console.log('entitieues');
        this.selectedRows = response.data;
        this.selectedServices = response.data;
        self.entities = response.data;
      }.bind(this))["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    }
  },
  computed: {
    swatchStyleUpdate: function swatchStyleUpdate() {
      var color = this.color,
          menu = this.menu;
      return {
        backgroundColor: this.setEntityForUpdate.color,
        cursor: 'pointer',
        height: '30px',
        width: '30px',
        borderRadius: menu ? '50%' : '4px',
        transition: 'border-radius 200ms ease-in-out'
      };
    },
    swatchStyle: function swatchStyle() {
      var color = this.color,
          menu = this.menu;
      return {
        backgroundColor: this.entity.color,
        cursor: 'pointer',
        height: '30px',
        width: '30px',
        borderRadius: menu ? '50%' : '4px',
        transition: 'border-radius 200ms ease-in-out'
      };
    }
  },
  mounted: function mounted() {
    if (this.company.id !== undefined) {
      this.company_id = this.company.id;
      this.selectedCompany = this.company.id;
    }

    this.getServices();
    this.getEntities();
    this.getCompanies();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/history.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-bootstrap-datetimepicker */ "./node_modules/vue-bootstrap-datetimepicker/dist/vue-bootstrap-datetimepicker.js");
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker/dist/locale */ "./node_modules/vuejs-datepicker/dist/locale/index.js");
/* harmony import */ var vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vuejs_datepicker_dist_locale__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css */ "./node_modules/pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css");
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue2_timepicker_src__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue2-timepicker/src */ "./node_modules/vue2-timepicker/src/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Import date picker css



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['auth', 'appointments'],
  components: {},
  data: function data() {
    return {
      user: null
    };
  },
  methods: {
    deleteAppointment: function deleteAppointment(id) {
      // διαγραφή appointment
      axios.post('/booking/history/deleteAppointment', {
        'appointment': id
      }).then(function (response) {
        location.reload();
      }.bind(this))["catch"](function (error) {
        console.log(error);
      });
    },
    getUser: function getUser() {
      axios.get('/userisonline').then(function (response) {
        this.user = response.data;

        if (this.user != '') {
          this.email = this.user.email;
        }
      }.bind(this))["catch"](function (error) {
        console.log(error);
      });
    }
  },
  created: function created() {},
  mounted: function mounted() {
    this.getUser();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/index.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fullcalendar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @fullcalendar/vue */ "./node_modules/@fullcalendar/vue/main.esm.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fullcalendar_timegrid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fullcalendar/timegrid */ "./node_modules/@fullcalendar/timegrid/main.js");
/* harmony import */ var _fullcalendar_timegrid__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_timegrid__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _fullcalendar_core_main_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fullcalendar/core/main.css */ "./node_modules/@fullcalendar/core/main.css");
/* harmony import */ var _fullcalendar_core_main_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_core_main_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _fullcalendar_daygrid_main_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fullcalendar/daygrid/main.css */ "./node_modules/@fullcalendar/daygrid/main.css");
/* harmony import */ var _fullcalendar_daygrid_main_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_daygrid_main_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _fullcalendar_timegrid_main_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fullcalendar/timegrid/main.css */ "./node_modules/@fullcalendar/timegrid/main.css");
/* harmony import */ var _fullcalendar_timegrid_main_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_fullcalendar_timegrid_main_css__WEBPACK_IMPORTED_MODULE_6__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FullCalendar: _fullcalendar_vue__WEBPACK_IMPORTED_MODULE_0__["default"] // make the <FullCalendar> tag available

  },
  props: {
    source: String
  },
  data: function data() {
    return {
      calendarWeekends: null,
      calendarEvents: [{
        title: 'event 1',
        date: '2020-01-16 08:00'
      }, {
        title: 'event 2',
        editable: true,
        allDay: false,
        start: '2020-01-15 09:00',
        end: '2020-01-15 11:00',
        backgroundColor: 'red',
        borderColor: 'red',
        textColor: 'white'
      }],
      calendarPlugins: [_fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1___default.a, _fullcalendar_timegrid__WEBPACK_IMPORTED_MODULE_2___default.a, _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3___default.a],
      drawer: null,
      items: [{
        href: '#',
        icon: 'mdi-chart-bubble',
        text: 'Διαχείρηση'
      }, {
        href: '#',
        icon: 'mdi-bicycle-basket',
        text: 'Παραγγελίες'
      }, {
        href: '#',
        icon: 'mdi-account',
        text: 'Πελάτες'
      }, {
        href: '#',
        icon: 'mdi-wallet-giftcard',
        text: 'Προϊόντα'
      }, {
        href: '#',
        icon: 'mdi-shape',
        text: 'Κατηγορίες'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Εκπτώσεις'
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Κουπόνια'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Μπλόκ'
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Σελίδες'
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Slider'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Σύστημα'
      }, {
        href: '#',
        icon: 'touch_app',
        text: 'Reminders'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        heading: 'Labels'
      }, {
        href: '#',
        icon: 'add',
        text: 'Create new label'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'archive',
        text: 'Archive'
      }, {
        href: '#',
        icon: 'delete',
        text: 'Trash'
      }, {
        href: '#',
        icon: 'settings',
        text: 'Settings'
      }, {
        href: '#',
        icon: 'chat_bubble',
        text: 'Trash'
      }, {
        href: '#',
        icon: 'help',
        text: 'Help'
      }, {
        href: '#',
        icon: 'phonelink',
        text: 'App downloads'
      }, {
        href: '#',
        icon: 'keyboard',
        text: 'Keyboard shortcuts'
      }],
      desserts: [{
        name: 'Frozen Yogurt',
        calories: 159
      }, {
        name: 'Ice cream sandwich',
        calories: 237
      }, {
        name: 'Eclair',
        calories: 262
      }, {
        name: 'Cupcake',
        calories: 305
      }, {
        name: 'Gingerbread',
        calories: 356
      }, {
        name: 'Jelly bean',
        calories: 375
      }, {
        name: 'Lollipop',
        calories: 392
      }, {
        name: 'Honeycomb',
        calories: 408
      }, {
        name: 'Donut',
        calories: 452
      }, {
        name: 'KitKat',
        calories: 518
      }]
    };
  },
  methods: {
    toggleWeekends: function toggleWeekends() {
      this.calendarWeekends = !this.calendarWeekends; // update a property
    },
    gotoPast: function gotoPast() {
      var calendarApi = this.$refs.fullCalendar.getApi(); // from the ref="..."

      calendarApi.gotoDate("2000-01-01"); // call a method on the Calendar object
    },
    eventClick: function eventClick(arg) {
      console.log(arg);
    },
    handleDateClick: function handleDateClick(arg) {
      if (confirm("Would you like to add an event to " + arg.dateStr + " ?")) {
        this.calendarEvents.push({
          // add new event data
          title: "New Event",
          start: arg.date,
          allDay: arg.allDay
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/managers.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers


/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      permissions: [],
      snackbar: true,
      alertMessage: [],
      vertical: true,
      selectedEntities: [],
      selectedCompanies: [],
      selectedPermisions: [],
      newManager: false,
      editManagerDialog: false,
      editManager: {},
      entities: [],
      companies: [],
      alert: true,
      valid: true,
      drawer: true,
      date: new Date().toISOString().substr(0, 10),
      items: [{
        href: '#',
        icon: 'mdi-chart-bubble',
        text: 'Διαχείρηση'
      }, {
        href: '#',
        icon: 'mdi-bicycle-basket',
        text: 'Ραντεβού'
      }, {
        href: '#',
        icon: 'mdi-account',
        text: 'Επισκέπτες'
      }, {
        href: '#',
        icon: 'mdi-wallet-giftcard',
        text: 'Στατιστικά'
      }, {
        href: '#',
        icon: 'mdi-shape',
        text: 'Σημεία Ραντεβού'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'lightbulb_outline',
        text: 'Προφίλ'
      }, {
        href: '#',
        icon: 'mdi-account',
        text: 'Χρήστες'
      }, {
        href: '#',
        divider: true
      }, {
        href: '#',
        icon: 'settings',
        text: 'Εταιρία'
      }, {
        href: '#',
        icon: 'chat_bubble',
        text: 'Διαγραμμένα Ραντέβου'
      }],
      //menu
      manager: {
        role: 'manager',
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
      },
      managers: [],
      showPassA: false,
      showPassB: false,
      rules: {
        required: function required(value) {
          return !!value || 'Required.';
        },
        min: function min(v) {
          return v.length >= 8 || 'Min 8 characters';
        },
        emailMatch: function emailMatch() {
          return 'The email and password you entered don\'t match';
        }
      }
    };
  },
  methods: {
    updateManager: function updateManager() {
      var self = this;
      var data = {
        entities: this.selectedEntities,
        companies: this.selectedCompanies,
        permisions: this.selectedPermisions
      };
      axios.post('/company/users/' + self.editManager.id, data).then(function (response) {
        // handle success
        console.log(response.data);
        self.selectedCompanies = [];
        self.selectedEntities = [];
        self.editManager = {};
        self.editManagerDialog = false;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.getManagers();
      });
    },
    createManager: function createManager() {
      console.log('create entity');
      var self = this;
      axios.post('/company/managers/create', {
        manager: self.manager,
        permissions: self.permissions
      }).then(function (response) {
        // handle success
        console.log(response.data);

        if (response.data.error !== undefined) {
          self.snackbar = true;
          var alertMessage = response.data.error;

          for (var a = 0; a < alertMessage.length; a++) {
            self.$toastr('error', alertMessage[a], 'Βρέθηκε το παρακάτω πρόβλημα');
          }
        } else {
          self.$toastr('info', 'Ο Χρήστης  Δημιουργήθηκε', response.data.name);
        }
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        self.getManagers();
        self.newManager = false;
      });
    },
    getEntities: function getEntities() {
      var self = this; // axios.get('/entities/json_format')

      axios.post('/entities/json_format', {
        company_id: self.company_id
      }).then(function (response) {
        self.entities = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    },
    getUserEntities: function getUserEntities() {
      axios.get('/company/users/' + self.editManager.id + '/entitiesId').then(function (response) {
        // handle success
        this.selectedEntities = response.data;
      }.bind(this))["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    getUserCompanies: function getUserCompanies() {
      axios.get('/company/users/' + self.editManager.id + '/companiesId').then(function (response) {
        // handle success
        console.log('response.data');
        console.log(response.data);
        this.selectedCompanies = response.data;
      }.bind(this))["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    getManagers: function getManagers() {
      self = this;
      axios.get('/company/users').then(function (response) {
        // handle success
        console.log(response.data);
        self.managers = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {});
    },
    getCompanies: function getCompanies() {
      var self = this;
      axios.get('/companies_json').then(function (response) {
        // handle success
        console.log('company_json');
        console.log(response.data);
        self.companies = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    }
  },
  mounted: function mounted() {
    this.getCompanies();
    this.getManagers();
    this.getEntities();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/meeting.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/meeting.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    navigation: _navigation__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['roomname'],
  data: function data() {
    return {};
  },
  methods: {},
  mounted: function mounted() {// nikos
    // https://tele.isol.gr
    //     user nikos
    // pass nikos7
    // const domain = 'tele.isol.gr';
    // const options = {
    //     roomName: this.roomname,
    //     width: 700,
    //     height: 700,
    //     parentNode: document.querySelector('#meet')
    // };
    // const api = new JitsiMeetExternalAPI(domain, options);
    // console.log('api.getCurrentDevices()')
    // console.log(api.getCurrentDevices())
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navigation.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navigation.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_0__["default"]],
  data: function data() {
    return {
      auth: {
        name: null
      },
      pendings: [],
      items: [{
        href: '/calendar',
        icon: 'mdi-calendar',
        text: 'ΗΜΕΡΟΛΟΓΙΟ',
        auth: ['manager']
      }, // { href:'#', icon: 'mdi-account', text: 'Επισκέπτες' },
      // { href:'#', icon: 'mdi-wallet-giftcard', text: 'Στατιστικά' },
      // { href:'#', icon: 'mdi-shape', text: 'Σημεία Ραντεβού' },
      // { href:'#', icon: 'lightbulb_outline', text: 'Προφίλ' },
      {
        href: '/settings',
        icon: 'mdi-bank',
        text: 'Ρυθμίσεις ',
        permission: 'CreateManagers',
        auth: ['administrator']
      }, {
        href: '/companies',
        icon: 'mdi-bank',
        text: 'Κτίρια',
        permission: 'CreateManagers',
        auth: ['manager']
      }, {
        href: '/entity',
        icon: 'mdi-home-map-marker',
        text: 'Υπηρεσίες',
        permission: 'CreateManagers',
        auth: ['manager']
      }, {
        href: '/services',
        icon: 'mdi-file-document',
        text: 'Δικαιολογιτικά',
        permission: 'CreateManagers',
        auth: ['manager']
      }, {
        href: '/company/managers',
        icon: 'mdi-account',
        text: 'Διαχειριστές',
        permission: 'CreateManagers',
        auth: ['manager']
      }, {
        href: '#',
        divider: true
      }, // { href:'/companies', icon: 'settings', text: 'Ιατρεία' },
      // { href:'/company', icon: 'settings', text: 'Σχολεία' },
      {
        href: '/entity',
        icon: 'house',
        text: 'Ιατρεία',
        permission: 'ViewEntities',
        auth: ['manager']
      } // {href: '#ekremotites', icon: 'chat_bubble', text: 'Ραντέβου Σε Εκρεμμότητα', auth: ['manager']},
      // {href: '/ical', icon: 'mail', text: 'Ical'},
      ],
      //menu
      entityDialog: false,
      entity: {
        title: null,
        description: null,
        color: '#303F9FFF',
        duration: 60
      },
      drawer: null
    };
  },
  computed: {
    pendings_number: function pendings_number() {
      return this.pendings.length;
    }
  },
  methods: {
    logout: function logout() {
      axios.post('/logout').then(function (response) {
        // handle success
        window.location = '/login';
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    },
    showPendings: function showPendings() {
      this.$emit('showPendings', true);
    },
    sendPendings: function sendPendings() {
      console.log(this.pendings);
      this.$emit('pendings', this.pendings);
    },
    getAuth: function getAuth() {
      var self = this;
      axios.get('/userisonline').then(function (response) {
        // handle succes
        self.auth = response.data;
      })["catch"](function (error) {
        console.log(error);
      }).then(function () {// always executed
      });
    },
    getPendingAppointments: function getPendingAppointments() {
      console.log('run getPendingAppointments');
      var self = this;
      axios.get('/calendar/pendings').then(function (response) {
        // handle success
        console.log(response.data);
        self.pendings = response.data;
        self.sendPendings();
      })["catch"](function (error) {
        // handle error
        console.log(error);
      }).then(function () {// always executed
      });
    }
  },
  created: function created() {
    this.getAuth();
  },
  mounted: function mounted() {
    this.getPendingAppointments();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/register.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/register.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    source: String
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers


/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  props: ['company'],
  data: function data() {
    return {
      files: [],
      services: [],
      headers: [{
        text: 'Title',
        align: 'start',
        sortable: true,
        value: 'title'
      }, {
        text: 'Delete',
        align: 'end',
        sortable: true,
        value: 'delete'
      }]
    };
  },
  methods: {
    editService: function editService(id) {
      window.location = '/services/' + id;
    },
    getServices: function getServices() {
      var self = this;
      axios.get('/get_all_services').then(function (response) {
        // handle success
        console.log(response.data);
        self.services = response.data;
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    newService: function newService() {
      window.location = '/services/new';
    },
    deleteService: function deleteService(service) {
      var result = confirm("Θέλετε να διαγραφεί το δικαιολογιτικό με τίτλο: " + service.title);

      if (result) {
        axios["delete"]('/service/' + service.id).then(function (res) {
          this.getServices();
        }.bind(this))["catch"](function (err) {
          console.log(err);
        });
      }
    }
  },
  mounted: function mounted() {
    this.getServices();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/edit.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/dist/quill.core.css */ "./node_modules/quill/dist/quill.core.css");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! quill/dist/quill.bubble.css */ "./node_modules/quill/dist/quill.bubble.css");
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__["quillEditor"]
  },
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  props: ['service'],
  data: function data() {
    return {
      file: {
        name: '',
        file: '',
        success: ''
      },
      media: {},
      formMessage: null,
      formMessageFile: null,
      editorOption: {// Some Quill options...
      }
    };
  },
  methods: {
    onChange: function onChange(e) {
      this.file.file = e.target.files[0];
      console.log('e.target.files[0]');
      console.log(e.target.files[0]);
    },
    formSubmit: function formSubmit(e) {
      e.preventDefault();
      var existingObj = this;
      var config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      };

      if (this.file.file == '') {
        this.formMessageFile = 'Παρακαλώ Επιλέξτε ένα αρχείο';
        return;
      }

      this.formMessageFile = null;

      if (this.file.name == '') {
        this.formMessage = 'Παρακαλώ συμπληρώστε τον Τίτλο του αρχείου';
        return;
      }

      this.formMessage = null;
      var data = new FormData();
      data.append('file', this.file.file);
      data.append('name', this.file.name);
      var self = this;
      axios.post('/upload/' + this.service.id, data, config).then(function (res) {
        console.log('upload success');
        console.log(res);
        self.getMedia();
        self.form.success = res.data.data.success;
      }.bind(this))["catch"](function (err) {
        console.log(err);
        existingObj.output = err;
      });
    },
    deleteMedia: function deleteMedia(id) {
      axios["delete"]('/media/' + id + '/delete').then(function (res) {
        this.getMedia();
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    },
    gotoServices: function gotoServices() {
      window.location = '/services';
    },
    getMedia: function getMedia() {
      var self = this;
      axios.get('/get_media/' + this.service.id).then(function (res) {
        console.log('res.data');
        console.log(res.data);
        self.media = res.data.data;
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    },
    updateService: function updateService() {
      axios.put('/services/' + this.service.id, this.service).then(function (response) {
        console.log('success');
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    },
    deleteService: function deleteService() {
      var result = confirm("Θέλετε να διαγραφεί το δικαιολογιτικό με τίτλο: ");

      if (result) {
        axios["delete"]('/service/' + this.service.id).then(function (res) {
          window.location.replace("/services");
        }.bind(this))["catch"](function (err) {
          console.log(err);
        });
      }
    }
  },
  mounted: function mounted() {
    this.getMedia();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/new.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../navigation */ "./resources/js/components/navigation.vue");
/* harmony import */ var _mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/permissionsCheck */ "./resources/js/mixins/permissionsCheck.vue");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/dist/quill.core.css */ "./node_modules/quill/dist/quill.core.css");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! quill/dist/quill.bubble.css */ "./node_modules/quill/dist/quill.bubble.css");
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_5__["quillEditor"]
  },
  mixins: [_mixins_permissionsCheck__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      service: {
        title: '',
        description: ''
      }
    };
  },
  methods: {
    createService: function createService() {
      axios.post('/services', this.service).then(function (response) {
        console.log('success');
        window.location = '/services/' + response.data.id;
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    }
  },
  mounted: function mounted() {
    this.getMedia();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/settings.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//@todo update and delete managers

/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [],
  props: ['settings'],
  data: function data() {
    return {
      alert: false,
      tab: null,
      form: {
        'title': null,
        'homeDescription': null,
        'clientId': null,
        'clientSecret': null,
        'urlAuthorize': null,
        'urlAccessToken': null,
        'urlResourceOwnerDetails': null,
        'redirectUri': null
      }
    };
  },
  methods: {
    update: function update() {
      axios.put('/settings', this.form).then(function () {
        this.alert = true;
      }.bind(this))["catch"](function (err) {
        console.log(err);
      });
    }
  },
  mounted: function mounted() {
    this.form = this.settings;
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/timetables.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/timetables.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['form'],
  data: function data() {
    return {
      newDay: {
        day: 1,
        slots: 1,
        open_at: 1,
        close_to: 1
      },
      hours: [],
      days: [{
        'number': 1,
        'day': 'Δευτέρα'
      }, {
        'number': 2,
        'day': 'Τρίτη'
      }, {
        'number': 3,
        'day': 'Τετάρτη'
      }, {
        'number': 4,
        'day': 'Πέμπτη'
      }, {
        'number': 5,
        'day': 'Παρασκευή'
      }, {
        'number': 6,
        'day': 'Σάββατο'
      }, {
        'number': 7,
        'day': 'Κυριακή'
      }]
    };
  },
  methods: {
    addHoursToDays: function addHoursToDays(key) {
      this.form[key]['hours'].push({
        open_at: null,
        close_to: null,
        slots: 1
      });
    },
    removeHoursFromDays: function removeHoursFromDays(dKey, hKey) {
      if (this.form[dKey]['hours'].length < 1) {
        return;
      }

      this.form[dKey]['hours'].splice(hKey, 1);
      this.$forceUpdate();
    },
    addNewDay: function addNewDay() {
      for (var a = 0; a < this.days.length; a++) {
        if (this.days[a].number == this.newDay.day) {
          var selectedDayName = this.days[a].day;
        }
      }

      this.form.push({
        "day_number": this.newDay.day,
        "day": selectedDayName,
        "hours": [{
          "slots": this.newDay.slots,
          "open_at": this.newDay.open_at,
          "close_to": this.newDay.close_to
        }]
      });
    }
  },
  watch: {
    form: {
      // This will let Vue know to look inside the array
      deep: true,
      // We have to move our method to a handler field
      handler: function handler() {
        this.$emit('updatedTimetableValues', this.form);
      }
    }
  },
  mounted: function mounted() {
    var x = 15; //minutes interval

    var times = []; // time array

    var tt = 0; // start time

    var ap = ['ΠΜ', 'ΜΜ']; // AM-PM
    //loop to increment the time and push results in array

    for (var i = 0; tt < 24 * 60; i++) {
      var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format

      var mm = tt % 60; // getting minutes of the hour in 0-55 format

      times[i] = ("0" + hh).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]

      tt = tt + x;
    }

    this.hours = times;
    this.$forceUpdate();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    can: function can(value) {
      return Laravel.permissions.indexOf(value) !== -1;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../node_modules/css-loader!@fullcalendar/core/main.css */ "./node_modules/css-loader/index.js!./node_modules/@fullcalendar/core/main.css"), "");
exports.i(__webpack_require__(/*! -!../../../node_modules/css-loader!@fullcalendar/daygrid/main.css */ "./node_modules/css-loader/index.js!./node_modules/@fullcalendar/daygrid/main.css"), "");

// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nbody {\n    /*min-height: 1500px;*/\n}\n.vdp-datepicker__calendar {\n    width: 100% !important;\n}\nspan.cell.day {\n    border: solid 0.1px #f9f9f9;\n}\n.vdp-datepicker__calendar {\n    border: solid 1px #eee\n}\nspan.cell.day.selected {\n    color: #fff;\n}\n.vdp-datepicker__calendar {\n    border: none !important;\n}\n\n/*steper*/\nspan.badge {\n    border-radius: 50%;\n}\nul.nav.justify-content-center.stepper li+li::before {\n    width: 35px;\n    height: 1px;\n    position: absolute;\n    background: #047bfe;\n    top: 21px;\n    content: '';\n    left: -16px;\n    z-index: -8;\n}\nul.nav.justify-content-center.stepper li {\n    position: relative;\n}\na.nav-link span {\n    background: #ccc;\n    width: 18px;\n}\na.nav-link.active span {\n    background: #007bff;\n}\nbutton.btn.timebutton {\n    background: #eee;\n    border-radius: 0;\n    margin: 5px;\n    width: 68px;\n    border: solid 1px #ddd;\n}\n.btn.ipiresiaBtn {\n    background: #eee;\n    border-radius: 0;\n    margin: 5px;\n    border: solid 1px #ddd;\n}\nbutton.btn.timebutton:hover, .btn.ipiresiaBtn:hover {\n    background: #44bbdd;\n    color: #fff;\n}\n.cursor {\n    cursor: pointer;\n}\nbutton.btn.timebutton:focus, button.btn.timebutton:active, .btn.ipiresiaBtn {\n    background: #44bcdc;\n}\n.panel {\n    border: solid 1px #eee;\n    box-shadow: 0 0 4px #bdbdbd;\n}\nspan.btn.timebutton.danger {\n    background: red;\n    border-radius: 0;\n}\nbutton.btn.timebutton.danger {\n    background: red;\n    position: relative;\n}\n.btn.timebutton.danger {\n    position: relative;\n}\n.btn.timebutton.danger:before {\n    top: 5px;\n    content: \"\";\n    width: 2px;\n    height: 25px;\n    position: absolute;\n    background: rgba(0, 0, 0, 0.5);\n    transform: rotate(48deg);\n    left: 50%;\n    margin-right: auto;\n}\n.btn.timebutton.danger:after {\n    top: 5px;\n    content: \"\";\n    width: 2px;\n    height: 25px;\n    position: absolute;\n    background: rgba(0, 0, 0, 0.5);\n    transform: rotate(-48deg);\n    left: 50%;\n    margin-right: auto;\n}\nspan.btn.timebutton.danger {\n    background: red;\n    border-radius: 0;\n    margin: 5px;\n    width: 68px;\n}\n.vdp-datepicker__calendar .disabled {\n    color: #848383;\n    cursor: default;\n}\nspan.cell.day {\n    border: solid 0.1px #dedede;\n}\n.btn-blue {\n    background: #145082;\n    color: #fff;\n}\n.btn-green {\n    background: #9AB021;\n    color: #fff;\n}\n.btn-green:hover, .btn-blue:hover {\n    color: #fff;\n    box-shadow: 0 0 4px #222;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nspan.vue__time-picker.time-picker input {\n    border: none;\n    border-bottom: solid 1px #999;\n    width: 100%;\n    margin-top: -1px;\n}\nspan.vue__time-picker.time-picker {\n    width: 100%;\n}\n.labelSmall {\n    font-size: 12px;\n}\n\n/*@import '~@fullcalendar/core/main.css';*/\n/*@import '~@fullcalendar/daygrid/main.css';*/\n.mb-2 {\n    margin-bottom: 2px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/history.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nbody {\n  /*min-height: 1500px;*/\n}\n.vdp-datepicker__calendar {\n  width: 100% !important;\n}\nspan.cell.day {\n  border: solid 0.1px #f9f9f9;\n}\n.vdp-datepicker__calendar {\n  border: solid 1px #eee\n}\nspan.cell.day.selected {\n  color: #fff;\n}\n.vdp-datepicker__calendar {\n  border: none !important;\n}\n\n/*steper*/\nspan.badge {\n  border-radius: 50%;\n}\nul.nav.justify-content-center.stepper li + li::before {\n  width: 35px;\n  height: 1px;\n  position: absolute;\n  background: #047bfe;\n  top: 21px;\n  content: '';\n  left: -16px;\n  z-index: -8;\n}\nul.nav.justify-content-center.stepper li {\n  position: relative;\n}\na.nav-link span {\n  background: #ccc;\n  width: 18px;\n}\na.nav-link.active span {\n  background: #007bff;\n}\nbutton.btn.timebutton {\n  background: #eee;\n  border-radius: 0;\n  margin: 5px;\n  width: 68px;\n  border: solid 1px #ddd;\n}\n.btn.ipiresiaBtn{\n  background: #eee;\n  border-radius: 0;\n  margin: 5px;\n  border: solid 1px #ddd;\n}\nbutton.btn.timebutton:hover , .btn.ipiresiaBtn:hover{\n  background: #44bbdd;\n  color: #fff;\n}\n.cursor {\n  cursor: pointer;\n}\nbutton.btn.timebutton:focus, button.btn.timebutton:active , .btn.ipiresiaBtn {\n  background: #44bcdc;\n}\n.panel {\n  border: solid 1px #eee;\n  box-shadow: 0 0 4px #bdbdbd;\n}\nspan.btn.timebutton.danger {\n  background: red;\n  border-radius: 0;\n}\nbutton.btn.timebutton.danger {\n  background: red;\n  position: relative;\n}\n.btn.timebutton.danger{\n  position: relative;}\n.btn.timebutton.danger:before {top: 5px; content: \"\";width: 2px;height: 25px;position: absolute;background: rgba(0, 0, 0, 0.5);transform: rotate(48deg);left: 50%;margin-right: auto;}\n.btn.timebutton.danger:after {top: 5px; content: \"\";width: 2px;height: 25px;position: absolute;background: rgba(0, 0, 0, 0.5);transform: rotate(-48deg);left: 50%;margin-right: auto;}\nspan.btn.timebutton.danger {\n  background: red;\n  border-radius: 0;\n  margin: 5px;\n  width: 68px;\n}\n.vdp-datepicker__calendar .disabled {\n  color: #848383;\n  cursor: default;\n}\nspan.cell.day {\n  border: solid 0.1px #dedede;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./companies.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./company.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./entities.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./managers.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./services.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./new.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./bookingform.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./calendar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/history.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./history.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/auth.vue?vue&type=template&id=15b8471a&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/auth.vue?vue&type=template&id=15b8471a& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "inspire" } },
    [
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { staticClass: "fill-height", attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                { attrs: { align: "center", justify: "center" } },
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "8", md: "4" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "elevation-12" },
                        [
                          _c(
                            "v-toolbar",
                            { attrs: { color: "primary", dark: "", flat: "" } },
                            [
                              _c("v-toolbar-title", [_vm._v("Login form")]),
                              _vm._v(" "),
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { bottom: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g(
                                              {
                                                attrs: {
                                                  href: _vm.source,
                                                  icon: "",
                                                  large: "",
                                                  target: "_blank"
                                                }
                                              },
                                              on
                                            ),
                                            [
                                              _c("v-icon", [
                                                _vm._v("mdi-code-tags")
                                              ])
                                            ],
                                            1
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [_vm._v(" "), _c("span", [_vm._v("Source")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { right: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g(
                                              {
                                                attrs: {
                                                  icon: "",
                                                  large: "",
                                                  href:
                                                    "https://codepen.io/johnjleider/pen/pMvGQO",
                                                  target: "_blank"
                                                }
                                              },
                                              on
                                            ),
                                            [
                                              _c("v-icon", [
                                                _vm._v("mdi-codepen")
                                              ])
                                            ],
                                            1
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [_vm._v(" "), _c("span", [_vm._v("Codepen")])]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c(
                                "v-form",
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Login",
                                      name: "login",
                                      "prepend-icon": "person",
                                      type: "text"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      id: "password",
                                      label: "Password",
                                      name: "password",
                                      "prepend-icon": "lock",
                                      type: "password"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c("v-btn", { attrs: { color: "primary" } }, [
                                _vm._v("Login")
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container", attrs: { id: "start" } }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12 text-center" }, [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.emailfError != null,
                  expression: "emailfError != null"
                }
              ],
              staticClass: "alert alert-danger",
              attrs: { role: "alert" }
            },
            [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.emailfError) +
                  "\n                "
              )
            ]
          )
        ]),
        _vm._v(" "),
        _vm.step != 5
          ? _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "text-center" }, [
                _vm._m(0),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "ul",
                  { staticClass: "nav justify-content-center stepper" },
                  [
                    _c("li", { staticClass: "nav-item" }, [
                      _c(
                        "a",
                        {
                          staticClass: "nav-link",
                          class: _vm.step == 1 ? "active" : null
                        },
                        [
                          _c("span", { staticClass: "badge badge-primary" }, [
                            _vm._v("1")
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-item" }, [
                      _c(
                        "a",
                        {
                          staticClass: "nav-link",
                          class: _vm.step == 2 ? "active" : null,
                          attrs: { href: "#" }
                        },
                        [
                          _c("span", { staticClass: "badge badge-primary" }, [
                            _vm._v("2")
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-item" }, [
                      _c(
                        "a",
                        {
                          staticClass: "nav-link",
                          class: _vm.step == 3 ? "active" : null,
                          attrs: { href: "#" }
                        },
                        [
                          _c("span", { staticClass: "badge badge-primary" }, [
                            _vm._v("3")
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-item" }, [
                      _c(
                        "a",
                        {
                          staticClass: "nav-link disabled",
                          class: _vm.step == 4 ? "active" : null,
                          attrs: {
                            href: "#",
                            tabindex: "-1",
                            "aria-disabled": "true"
                          }
                        },
                        [
                          _c("span", { staticClass: "badge badge-primary" }, [
                            _vm._v("4")
                          ])
                        ]
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c("i", [_vm._v("Βήμα " + _vm._s(_vm.step) + " απο 4")])
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.step == 1
          ? _c("div", { staticClass: "col-md-4 " }, [
              _c("h2", [_vm._v("Επιλογή Υπηρεσίας")])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.step == 1
          ? _c("div", { staticClass: "col-md-4 offset-md-4" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.searchQuery,
                    expression: "searchQuery"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  placeholder: "Αναζήτηση: π.χ Δήλωση Γέννησης "
                },
                domProps: { value: _vm.searchQuery },
                on: {
                  change: _vm.searchService,
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.searchQuery = $event.target.value
                  }
                }
              })
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.step == 1
          ? _c("div", { staticClass: "col-md-12 panel" }, [
              _c(
                "div",
                { staticClass: "companies" },
                _vm._l(_vm.resultCompanies, function(s, c) {
                  return _c(
                    "div",
                    {
                      staticClass: "company mb-2",
                      attrs: { id: s.company_id }
                    },
                    [
                      _c("div", { staticClass: "list-group" }, [
                        _c(
                          "a",
                          {
                            staticClass:
                              "list-group-item list-group-item-action ",
                            staticStyle: {
                              "font-size": "18px",
                              "font-weight": "bold"
                            },
                            attrs: {
                              "data-toggle": "collapse",
                              href: "#s" + s.company_id,
                              role: "button",
                              "aria-expanded": "false",
                              "aria-controls": "#s" + s.company_id
                            }
                          },
                          [_vm._v(" " + _vm._s(s.company))]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "collapse multi-collapse",
                          class: _vm.searchQuery != "" ? "show" : "",
                          attrs: { id: "s" + s.company_id }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "list-group" },
                            _vm._l(s.services, function(d) {
                              return _c(
                                "span",
                                {
                                  staticClass:
                                    "list-group-item list-group-item-action cursor"
                                },
                                [
                                  _c(
                                    "a",
                                    {
                                      on: {
                                        click: function($event) {
                                          _vm.selectedService = d
                                          _vm.step = 2
                                          _vm.company_id = s.company_id
                                          _vm.title = d.title
                                          _vm.description = d.title
                                          _vm.get_service_entities()
                                          _vm.get_service_media()
                                        }
                                      }
                                    },
                                    [_vm._v(" " + _vm._s(d.title) + " ")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn ml-2 btn-green float-right",
                                      on: {
                                        click: function($event) {
                                          _vm.selectedService = d
                                          _vm.step = 2
                                          _vm.company_id = s.company_id
                                          _vm.title = d.title
                                          _vm.description = d.title
                                          _vm.get_service_entities()
                                          _vm.get_service_media()
                                        }
                                      }
                                    },
                                    [_vm._v(" Κλείστε Ραντεβού ")]
                                  ),
                                  _vm._v(" "),
                                  d.external_link != "" &&
                                  d.external_link != null
                                    ? _c(
                                        "a",
                                        {
                                          staticClass:
                                            " btn btn-blue ml-2 float-right",
                                          attrs: {
                                            href: d.external_link,
                                            target: "_blank"
                                          }
                                        },
                                        [_vm._v(" Εκδώστε το ψηφιακά εδώ")]
                                      )
                                    : _vm._e()
                                ]
                              )
                            }),
                            0
                          )
                        ]
                      )
                    ]
                  )
                }),
                0
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.selectedService != null && _vm.step == 2
          ? _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "panel p-4" }, [
                _c("h3", [_vm._v(_vm._s(_vm.selectedService.title))]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Πρέπει να έχετε μαζί σας τα παρακάτω έντυπα και δικαιολογητικά συμπληρωμένα μαζί με την αστυνομική σας ταυτότητα. "
                  )
                ]),
                _vm._v(" "),
                _c("div", {
                  domProps: {
                    innerHTML: _vm._s(_vm.selectedService.description)
                  }
                }),
                _vm._v(" "),
                _c("div", {
                  domProps: { innerHTML: _vm._s(_vm.selectedService.documents) }
                }),
                _vm._v(" "),
                _c(
                  "ul",
                  { staticClass: "list-unstyled" },
                  _vm._l(_vm.selectedServiceMedia, function(m) {
                    return _c("li", [
                      _vm._v(
                        "\n                            Κατεβάστε το έντυπο: "
                      ),
                      _c(
                        "a",
                        { attrs: { href: m.original_url, target: "_blank" } },
                        [_vm._v(_vm._s(m.name))]
                      )
                    ])
                  }),
                  0
                ),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary float-left",
                        on: {
                          click: function($event) {
                            _vm.step = 1
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                                << Προηγούμενο"
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary float-right",
                        on: {
                          click: function($event) {
                            _vm.step = 3
                            _vm.getEntityTimeTable()
                          }
                        }
                      },
                      [_vm._v("Επόμενο >>\n                            ")]
                    )
                  ])
                ])
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.selectedService != null && _vm.step == 3
          ? _c(
              "div",
              { staticClass: "col-md-12 panel p-3" },
              [
                _c("h2", [_vm._v(_vm._s(_vm.selectedService.title))]),
                _vm._v(" "),
                _c("h3", { staticStyle: { "font-size": "18px" } }, [
                  _vm._v("Επιλογή Υπηρεσίας ")
                ]),
                _vm._v(" "),
                _vm._l(_vm.entities, function(e) {
                  return _c(
                    "button",
                    {
                      staticClass: "btn ipiresiaBtn mr-2",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          _vm.entity = e
                          _vm.entity_id = e.id
                          _vm.company_id = e.company_id
                          _vm.setTimeTable()
                          _vm.selectTime = null
                          _vm.selectedDate = null
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                    " +
                          _vm._s(e.title) +
                          "\n                "
                      )
                    ]
                  )
                }),
                _vm._v(" "),
                _c("p", { staticClass: "small" }, [
                  _vm._v(
                    "Επιλέξτε την υπηρεσία που επιθυμείτε να εκδόσετε το πιστοποιητικό σας για να συνεχίσετε. "
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    { staticClass: "col-md-6" },
                    [
                      _vm.entity_id
                        ? _c("p", { staticClass: "text-center" }, [
                            _c("b", [_vm._v("Επιλογή Ημερομηνίας")])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.entity_id
                        ? _c("datepicker", {
                            attrs: {
                              "open-date": _vm.openDate,
                              language: _vm.el,
                              inline: true,
                              highlighted: _vm.highlighted,
                              disabledDates: _vm.disabledDates
                            },
                            on: { selected: _vm.dateIsSelected },
                            model: {
                              value: _vm.selectedDate,
                              callback: function($$v) {
                                _vm.selectedDate = $$v
                              },
                              expression: "selectedDate"
                            }
                          })
                        : _vm._e()
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _vm.selectTimes.length > 0
                      ? _c(
                          "div",
                          { staticClass: "form-group mt-2 mb-2" },
                          [
                            _vm._m(2),
                            _vm._v(" "),
                            _vm._l(_vm.selectTimes, function(t) {
                              return _c("span", [
                                !t.startsWith("x", 0)
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "btn timebutton",
                                        class:
                                          _vm.selectTime == t ? "active" : null,
                                        attrs: { type: "button" },
                                        on: {
                                          click: function($event) {
                                            _vm.selectTime = t
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                    " +
                                            _vm._s(t) +
                                            "\n                                "
                                        )
                                      ]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                t.startsWith("x", 0)
                                  ? _c(
                                      "span",
                                      {
                                        staticClass: "btn timebutton danger",
                                        attrs: { type: "button", disabled: "" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                    " +
                                            _vm._s(t.substring(1)) +
                                            "\n                                "
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              ])
                            })
                          ],
                          2
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.selectedDate && _vm.entity_id && _vm.selectTime
                      ? _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-md-12 " }, [
                            _c("h4", [_vm._v("Τρόπος Συνάντησης")]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "custom-control custom-radio" },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.setTypeOfAppointment,
                                      expression: "setTypeOfAppointment"
                                    }
                                  ],
                                  staticClass: "custom-control-input",
                                  attrs: {
                                    type: "radio",
                                    id: "setTypeOfAppointment",
                                    value: "1"
                                  },
                                  domProps: {
                                    checked: _vm._q(
                                      _vm.setTypeOfAppointment,
                                      "1"
                                    )
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.setTypeOfAppointment = "1"
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  {
                                    staticClass: "custom-control-label",
                                    attrs: { for: "setTypeOfAppointment" }
                                  },
                                  [_vm._v("Διά ζώσης")]
                                )
                              ]
                            )
                          ])
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-12" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary",
                        on: {
                          click: function($event) {
                            _vm.step = 2
                            _vm.selectedDate = null
                            _vm.selectTime = null
                            _vm.selectTimes = []
                            _vm.selectedDate = null
                            _vm.entity_id = null
                          }
                        }
                      },
                      [_vm._v("\n                            << Προηγούμενο")]
                    ),
                    _vm._v(" "),
                    _vm.selectedDate && _vm.entity_id && _vm.selectTime
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-primary float-right",
                            on: {
                              click: function($event) {
                                _vm.step = 4
                              }
                            }
                          },
                          [_vm._v(" Επόμενο >>\n                        ")]
                        )
                      : _vm._e()
                  ])
                ])
              ],
              2
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.step == 4
          ? _c("div", { staticClass: "col-md-12 panel p-3" }, [
              _vm._m(3),
              _vm._v(" "),
              _c("table", { staticClass: "table table-sm" }, [
                _c("tr", [
                  _c("td", [_vm._v("Όνομα")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.auth.given_name,
                          expression: "auth.given_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "" },
                      domProps: { value: _vm.auth.given_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.auth, "given_name", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Επίθετο")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.auth.family_name,
                          expression: "auth.family_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "" },
                      domProps: { value: _vm.auth.family_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.auth, "family_name", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Όνομα Πατέρα ")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.auth.father_name,
                          expression: "auth.father_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "" },
                      domProps: { value: _vm.auth.father_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.auth, "father_name", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("ΑΦΜ ")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.auth.vat,
                          expression: "auth.vat"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "" },
                      domProps: { value: _vm.auth.vat },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.auth, "vat", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Υπηρεσία")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.entity.title) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Πιστοποιητικό")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.title) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Ημ/νία:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.selectedDateOfAppointment))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Ώρα:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.selectTime))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Τρόπος Συνάντησης:")]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(
                      _vm._s(
                        _vm.setTypeOfAppointment == 1
                          ? "Διά ζώσης"
                          : "Μέ τηλεδιάσκεψη"
                      )
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td"),
                  _vm._v(" "),
                  _c("td", [
                    _vm.setTypeOfAppointment == 1
                      ? _c("span", [
                          _vm._v(
                            "\n                                Πρέπει να συμπληρώσετε το Τηλέφωνο Επικοινωνίας σας για να μπορέσετε να συνεχίσετε, και αν θέλετε και το e-mail σας.\n                            "
                          )
                        ])
                      : _c("span", [
                          _vm._v(
                            "\n                                Πρέπει να συμπληρώσετε το E-mail σας για να μπορέσετε να συνεχίσετε, και αν θέλετε και το Τηλέφωνο Επικοινωνίας σας.\n                            "
                          )
                        ])
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Τηλ. Επικοινωνίας:")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.phone,
                          expression: "phone"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        placeholder: "Καταχωρήστε τηλέφωνο επικοινωνίας."
                      },
                      domProps: { value: _vm.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.phone = $event.target.value
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Αποστολή με Email:")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.email,
                          expression: "email"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "" },
                      domProps: { value: _vm.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.email = $event.target.value
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("small", { staticClass: "form-text text-muted" }, [
                      _vm._v(
                        "Συμπληρώστε το e-mail σας αν θέλετε να λάβετε έκεί το αποδεικτικο του ραντεβού σας."
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("label", { attrs: { for: "comment" } }, [_vm._v("Σχόλια")]),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.comment,
                    expression: "comment"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "comment", rows: "3" },
                domProps: { value: _vm.comment },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.comment = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary float-left",
                      on: {
                        click: function($event) {
                          _vm.step = 3
                          _vm.selectedDate = null
                          _vm.selectTime = null
                          _vm.selectTimes = []
                          _vm.entity_id = null
                        }
                      }
                    },
                    [_vm._v("\n                            << Προηγούμενο")]
                  ),
                  _vm._v(" "),
                  _vm.setTypeOfAppointment == 1
                    ? _c(
                        "button",
                        {
                          staticClass: "btn btn-success float-right",
                          attrs: {
                            disabled:
                              _vm.selectTime != null && _vm.phone.length > 9
                                ? false
                                : true
                          },
                          on: { click: _vm.postAppointment }
                        },
                        [
                          _vm._v(
                            "Καταχώρηση Διά ζώσης\n                                "
                          ),
                          _vm.loadingAppointment
                            ? _c("i", {
                                staticClass: "fas fa-spinner fa-pulse"
                              })
                            : _vm._e()
                        ]
                      )
                    : _c(
                        "button",
                        {
                          staticClass: "btn btn-success float-right",
                          attrs: {
                            disabled:
                              _vm.selectTime != null && _vm.email.length > 9
                                ? false
                                : true
                          },
                          on: { click: _vm.postAppointment }
                        },
                        [
                          _vm._v("Καταχώρηση\n                            "),
                          _vm.loadingAppointment
                            ? _c("i", {
                                staticClass: "fas fa-spinner fa-pulse"
                              })
                            : _vm._e()
                        ]
                      )
                ])
              ])
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _vm.step == 5
        ? _c("div", [
            _vm._m(4),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "text-right" }, [
                _c("i", {
                  staticClass: "fas fa-print",
                  on: { click: _vm.printDialog }
                })
              ]),
              _vm._v(" "),
              _c("table", { staticClass: "table table-sm" }, [
                _c("tr", [
                  _c("td", [_vm._v("Όνομα")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.auth.given_name))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Επίθετο")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.auth.family_name) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Όνομα Πατέρα ")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.auth.father_name) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("ΑΦΜ ")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.auth.vat) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Υπηρεσία")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.entity.title) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Πιστοποιητικό")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(" " + _vm._s(_vm.title) + " ")])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Ημ/νία:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.selectedDateOfAppointment))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Ώρα:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.selectTime))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Τρόπος Συνάντησης:")]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(
                      _vm._s(
                        _vm.setTypeOfAppointment == 1
                          ? "Διά ζώσης"
                          : "Μέ τηλεδιάσκεψη. Αποθηκεύστε αυτό το λινκ για να συνδεθείτε την ώρα και ημέρα που έχετε επιλέξει: " +
                              _vm.chat_url
                      )
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Email:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.email))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Τηλ:")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.phone))])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Σχόλια")]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.comment))])
                ])
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-primary",
                  attrs: { href: "#" },
                  on: { click: _vm.printDialog }
                },
                [_vm._v("ΕΚΤΥΠΩΣΗ")]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-success float-right",
                  attrs: { href: "/" }
                },
                [_vm._v("ΝΕΑ ΚΑΤΑΧΩΡΗΣΗ")]
              )
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("h2", [_vm._v("Αίτημα Ραντεβού Υπηρεσίας")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-8 offset-md-2" }, [
      _c("p", { staticClass: "mt-3" }, [
        _vm._v(
          "\n                            Η Εφαρμογή Ηλεκτρονικών Ραντεβού είναι μια υπηρεσία που κάνει την ζωή σου με το Δήμο πιο απλή.\n                        "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n                            Αναζητήστε παρακάτω την υπηρεσία για την οποία επιθυμείτε να κλείσετε ραντεβού, ενημερωθείτε για τα\n                            απαραίτητα δικαιολογητικά που πρέπει να έχετε μαζί και επιλέξτε την ημέρα και ώρα που σας εξυπηρετεί.\n                            Απλά με ένα κλικ.\n                        "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "text-center" }, [
      _c("b", [_vm._v("Επιλογή Ώρας")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-left" }, [
      _c("h2", { staticStyle: { "font-size": "28px" } }, [_vm._v("Σύνοψη ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12 text-center" }, [
      _c("h2", [_vm._v("Σας Ευχαριστούμε")]),
      _vm._v(" "),
      _c("p", [_vm._v("Το ραντεβού έχει καταχωρηθεί επιτυχώς. "), _c("br")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=template&id=094224ee&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/calendar.vue?vue&type=template&id=094224ee& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: { pendings: _vm.getPendings, showPendings: _vm.openPendings }
      }),
      _vm._v(" "),
      _c(
        "v-navigation-drawer",
        {
          attrs: { right: "", app: "", clipped: "", width: "500" },
          model: {
            value: _vm.rightPanel,
            callback: function($$v) {
              _vm.rightPanel = $$v
            },
            expression: "rightPanel"
          }
        },
        [
          _c("v-app-bar-nav-icon", {
            on: {
              click: function($event) {
                _vm.rightPanel = !_vm.rightPanel
              }
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "title ml-3 mr-5" }, [
            _vm._v("Νέα "),
            _c("span", { staticClass: "font-weight-light" }, [
              _vm._v("Καταχώρηση")
            ])
          ]),
          _vm._v(" "),
          _c(
            "v-container",
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-form",
                        { ref: "form" },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { right: "" } },
                                [
                                  _c("v-switch", {
                                    attrs: {
                                      label:
                                        "" +
                                        (_vm.appointment.approved
                                          ? "Επιβεβαιωμένο"
                                          : "Επιβεβαίωση Ραντεβού")
                                    },
                                    model: {
                                      value: _vm.appointment.approved,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "approved",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.approved"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "8" } },
                                [
                                  _c("v-select", {
                                    attrs: {
                                      items: _vm.entities,
                                      "item-text": "title",
                                      "item-value": "id",
                                      label: "Υπηρεσία",
                                      "prepend-icon": "house",
                                      required: ""
                                    },
                                    on: {
                                      change: function($event) {
                                        return _vm.getAvailableTimesForEntityByDay()
                                      }
                                    },
                                    model: {
                                      value: _vm.appointment.entity_id,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "entity_id",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.entity_id"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "4" } },
                                [
                                  _c("v-select", {
                                    attrs: {
                                      value: _vm.appointment.duration,
                                      items: [
                                        10,
                                        15,
                                        20,
                                        25,
                                        30,
                                        35,
                                        40,
                                        45,
                                        50,
                                        55,
                                        60
                                      ],
                                      label: "Διάρκεια",
                                      "prepend-icon": "alarm",
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.appointment.duration,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "duration",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.duration"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "12" } },
                                [
                                  _c("v-select", {
                                    attrs: {
                                      items: _vm.services,
                                      "item-text": "title",
                                      "item-value": "title",
                                      label: "Πιστοποιητικό",
                                      "prepend-icon": "house",
                                      required: ""
                                    },
                                    on: {
                                      change: function($event) {
                                        return _vm.getAvailableTimesForEntityByDay()
                                      }
                                    },
                                    model: {
                                      value: _vm.appointment.title,
                                      callback: function($$v) {
                                        _vm.$set(_vm.appointment, "title", $$v)
                                      },
                                      expression: "appointment.title"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "6" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Όνομα",
                                      "prepend-icon": "mdi-account",
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.appointment.given_name,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "given_name",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.given_name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "6" } },
                                [
                                  _c("v-text-field", {
                                    attrs: { label: "Επίθετο", required: "" },
                                    model: {
                                      value: _vm.appointment.family_name,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "family_name",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.family_name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "4" } },
                                [
                                  _c(
                                    "v-menu",
                                    {
                                      attrs: {
                                        "close-on-content-click": false,
                                        "nudge-right": 40,
                                        transition: "scale-transition",
                                        "offset-y": "",
                                        "min-width": "290px"
                                      },
                                      scopedSlots: _vm._u([
                                        {
                                          key: "activator",
                                          fn: function(ref) {
                                            var on = ref.on
                                            return [
                                              _c(
                                                "v-text-field",
                                                _vm._g(
                                                  {
                                                    attrs: {
                                                      label:
                                                        "Επιλογή Ημ/νιας Έναρξης",
                                                      "prepend-icon": "event",
                                                      readonly: ""
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment
                                                          .start_date,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "start_date",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.start_date"
                                                    }
                                                  },
                                                  on
                                                )
                                              )
                                            ]
                                          }
                                        }
                                      ]),
                                      model: {
                                        value: _vm.menu2,
                                        callback: function($$v) {
                                          _vm.menu2 = $$v
                                        },
                                        expression: "menu2"
                                      }
                                    },
                                    [
                                      _vm._v(" "),
                                      _c("v-date-picker", {
                                        on: {
                                          input: function($event) {
                                            _vm.menu2 = false
                                          }
                                        },
                                        model: {
                                          value: _vm.appointment.start_date,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.appointment,
                                              "start_date",
                                              $$v
                                            )
                                          },
                                          expression: "appointment.start_date"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "4" } },
                                [
                                  _c("v-select", {
                                    attrs: {
                                      items: _vm.EntityAvailiableTimeByDay,
                                      label: "Ώρα Έναρξης",
                                      "prepend-icon": "alarm",
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.appointment.start_time,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "start_time",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.start_time"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "4" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      disabled: "",
                                      label: "Ώρα Λήξης",
                                      "prepend-icon": "alarm",
                                      required: ""
                                    },
                                    model: {
                                      value: _vm.appointment.end_time,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "end_time",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.end_time"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "8" } },
                                [
                                  _c("v-textarea", {
                                    attrs: {
                                      "prepend-icon": "insert_comment",
                                      rows: "1",
                                      "auto-grow": "",
                                      name: "input-2-2",
                                      label: "Περιγραφή",
                                      value: _vm.appointment.comment
                                    },
                                    model: {
                                      value: _vm.appointment.comment,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.appointment,
                                          "comment",
                                          $$v
                                        )
                                      },
                                      expression: "appointment.comment"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "4" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "ma-0 pa-0",
                                    attrs: {
                                      elevation: "0",
                                      mask: _vm.colormask,
                                      "hide-details": "",
                                      solo: ""
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "append",
                                        fn: function() {
                                          return [
                                            _c(
                                              "v-menu",
                                              {
                                                attrs: {
                                                  top: "",
                                                  "nudge-bottom": "105",
                                                  "nudge-left": "16",
                                                  "close-on-content-click": false
                                                },
                                                scopedSlots: _vm._u([
                                                  {
                                                    key: "activator",
                                                    fn: function(ref) {
                                                      var on = ref.on
                                                      return [
                                                        _c(
                                                          "div",
                                                          _vm._g(
                                                            {
                                                              style:
                                                                _vm.swatchStyle
                                                            },
                                                            on
                                                          )
                                                        )
                                                      ]
                                                    }
                                                  }
                                                ]),
                                                model: {
                                                  value: _vm.menu,
                                                  callback: function($$v) {
                                                    _vm.menu = $$v
                                                  },
                                                  expression: "menu"
                                                }
                                              },
                                              [
                                                _vm._v(" "),
                                                _c(
                                                  "v-card",
                                                  [
                                                    _c(
                                                      "v-card-text",
                                                      { staticClass: "pa-0" },
                                                      [
                                                        _c("v-color-picker", {
                                                          attrs: {
                                                            flat: "",
                                                            "show-swatches": ""
                                                          },
                                                          model: {
                                                            value:
                                                              _vm.appointment
                                                                .color,
                                                            callback: function(
                                                              $$v
                                                            ) {
                                                              _vm.$set(
                                                                _vm.appointment,
                                                                "color",
                                                                $$v
                                                              )
                                                            },
                                                            expression:
                                                              "appointment.color"
                                                          }
                                                        })
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        },
                                        proxy: true
                                      }
                                    ]),
                                    model: {
                                      value: _vm.appointment.color,
                                      callback: function($$v) {
                                        _vm.$set(_vm.appointment, "color", $$v)
                                      },
                                      expression: "appointment.color"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { attrs: { cols: "12" } },
                                [
                                  _c(
                                    "v-expansion-panels",
                                    { attrs: { hover: "", flat: _vm.flat } },
                                    [
                                      _c(
                                        "v-expansion-panel",
                                        { attrs: { elevation: 0 } },
                                        [
                                          _c("v-expansion-panel-header", [
                                            _vm._v("Περισσότερα")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "v-expansion-panel-content",
                                            [
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "12" } },
                                                [
                                                  _c("v-text-field", {
                                                    attrs: {
                                                      label: "Email",
                                                      "prepend-icon": "email"
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment.email,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "email",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.email"
                                                    }
                                                  })
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "12" } },
                                                [
                                                  _c("v-text-field", {
                                                    attrs: {
                                                      label: "Τηλέφωνο 1",
                                                      "prepend-icon": "phone"
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment
                                                          .phone_number,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "phone_number",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.phone_number"
                                                    }
                                                  })
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "12" } },
                                                [
                                                  _c("v-text-field", {
                                                    attrs: {
                                                      label: "Τηλέφωνο 2",
                                                      "prepend-icon": "phone"
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment.phone,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "phone",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.phone"
                                                    }
                                                  })
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "12" } },
                                                [
                                                  _c("v-text-field", {
                                                    attrs: {
                                                      label: "Ημ/νια Γέννησης"
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment
                                                          .birthday,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "birthday",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.birthday"
                                                    }
                                                  })
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "12" } },
                                                [
                                                  _c("v-text-field", {
                                                    attrs: {
                                                      label: "Αρ. Ταυτότητας"
                                                    },
                                                    model: {
                                                      value:
                                                        _vm.appointment
                                                          .id_card_number,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.appointment,
                                                          "id_card_number",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "appointment.id_card_number"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("v-spacer"),
                              _vm._v(" "),
                              _vm.startRemoveProcess == true ||
                              _vm.appointment.admin_comment != null
                                ? _c(
                                    "v-col",
                                    { attrs: { cols: "12" } },
                                    [
                                      _c("v-textarea", {
                                        attrs: {
                                          "prepend-icon": "insert_comment",
                                          rows: "1",
                                          "auto-grow": "",
                                          name: "input-2-2",
                                          label: "Σχόλιο Απόρριψης",
                                          value: _vm.appointment.admin_comment
                                        },
                                        model: {
                                          value: _vm.appointment.admin_comment,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.appointment,
                                              "admin_comment",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "appointment.admin_comment"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-overlay",
                            { attrs: { value: _vm.overlayLoading } },
                            [
                              _c("v-progress-circular", {
                                attrs: { indeterminate: "", size: "64" }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.editAppointment == false,
                                  expression: "editAppointment == false"
                                }
                              ]
                            },
                            [
                              _c(
                                "v-btn",
                                {
                                  staticClass: "mr-4 mt-5",
                                  attrs: {
                                    loading: _vm.overlayLoading,
                                    disabled: _vm.overlayLoading,
                                    color: "success"
                                  },
                                  on: { click: _vm.createAppointment }
                                },
                                [
                                  _vm._v(
                                    "\n                                ΔΗΜΙΟΥΡΓΙΑ\n                            "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          !_vm.startRemoveProcess
                            ? _c(
                                "div",
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.editAppointment == true,
                                          expression: "editAppointment == true "
                                        }
                                      ],
                                      staticClass: "mr-4 mt-5",
                                      attrs: {
                                        disabled:
                                          _vm.appointment.entity_id == null ||
                                          _vm.appointment.entity_id == "",
                                        color: "primary"
                                      },
                                      on: { click: _vm.updateAppointment }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                ΕΝΗΜΕΡΩΣΗ\n                            "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      directives: [
                                        {
                                          name: "show",
                                          rawName: "v-show",
                                          value: _vm.editAppointment == true,
                                          expression: "editAppointment == true"
                                        }
                                      ],
                                      staticClass: "mr-4 mt-5",
                                      attrs: { color: "primary" },
                                      on: {
                                        click: _vm.updateAppointmentSendEmail
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                ΕΝΗΜΕΡΩΣΗ ΚΑΙ ΑΠΟΣΤΟΛΗ EMAIL\n                            "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.startRemoveProcess == true,
                                  expression: "startRemoveProcess == true"
                                }
                              ],
                              staticClass: "mr-4 mt-5",
                              attrs: {
                                disabled:
                                  _vm.appointment.admin_comment != null
                                    ? false
                                    : true,
                                color: "red",
                                dark: ""
                              },
                              on: { click: _vm.removeAppointment }
                            },
                            [
                              _vm._v(
                                "\n                            ΟΛΟΚΛΗΡΩΣΗ ΑΠΟΡΡΙΨΗΣ\n                        "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value:
                                    _vm.editAppointment == true &&
                                    _vm.startRemoveProcess == false,
                                  expression:
                                    "editAppointment == true && startRemoveProcess == false"
                                }
                              ],
                              staticClass: "mr-4 mt-5",
                              attrs: { color: "red", dark: "" },
                              on: {
                                click: function($event) {
                                  _vm.startRemoveProcess = true
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n                            ΑΠΟΡΡΙΨΗ\n                        "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "text-right float-right",
                              attrs: { right: "" }
                            },
                            [
                              _c(
                                "v-btn",
                                {
                                  staticClass: "mr-4 mt-5",
                                  attrs: { right: "", color: "danger" },
                                  on: {
                                    click: function($event) {
                                      _vm.rightPanel = false
                                      _vm.startRemoveProcess = false
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                ΑΚΥΡΩΣΗ\n                            "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _vm.can("ViewAudits")
                        ? _c(
                            "a",
                            {
                              attrs: { href: "#" },
                              on: {
                                click: function($event) {
                                  _vm.showAudits = true
                                  _vm.getAudits()
                                }
                              }
                            },
                            [_vm._v("Ιστορικότητα Αλλαγών")]
                          )
                        : _vm._e()
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("v-divider")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: {
            fullscreen: "",
            "hide-overlay": "",
            transition: "dialog-bottom-transition"
          },
          scopedSlots: _vm._u([
            {
              key: "activator",
              fn: function(ref) {
                var on = ref.on
                var attrs = ref.attrs
                return [
                  _c(
                    "v-btn",
                    _vm._g(
                      _vm._b(
                        { attrs: { color: "primary", dark: "" } },
                        "v-btn",
                        attrs,
                        false
                      ),
                      on
                    ),
                    [_vm._v("\n                Open Dialog\n            ")]
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.showAudits,
            callback: function($$v) {
              _vm.showAudits = $$v
            },
            expression: "showAudits"
          }
        },
        [
          _vm._v(" "),
          _c(
            "v-card",
            [
              _c(
                "v-toolbar",
                { attrs: { dark: "", color: "primary" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { icon: "", dark: "" },
                      on: {
                        click: function($event) {
                          _vm.showAudits = false
                        }
                      }
                    },
                    [_c("v-icon", [_vm._v("mdi-close")])],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-toolbar-title", [_vm._v("Ιστορικότητα Αλλαγών")]),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-list",
                { attrs: { "three-line": "", subheader: "" } },
                [
                  _c("v-subheader", [_vm._v(_vm._s(_vm.appointment.title))]),
                  _vm._v(" "),
                  _c("v-simple-table", {
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function() {
                          return [
                            _c("thead", [
                              _c("tr", [
                                _c("th", { staticClass: "text-left" }, [
                                  _vm._v(" Χρήστης")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticClass: "text-left" }, [
                                  _vm._v(" Ενέργεια")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticClass: "text-left" }, [
                                  _vm._v("Ημ/νια")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticClass: "text-left" }, [
                                  _vm._v("Παλιές Τιμές")
                                ]),
                                _vm._v(" "),
                                _c("th", { staticClass: "text-left" }, [
                                  _vm._v("Νέες Τιμές")
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(_vm.appointmentAudits, function(item) {
                                return _c("tr", { key: item.id }, [
                                  _c("td", [_vm._v(_vm._s(item.user.email))]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.event))]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.created_at))]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _c(
                                      "ul",
                                      _vm._l(item.old_values, function(v, i) {
                                        return _c("li", [
                                          _vm._v(
                                            " " + _vm._s(i) + " - " + _vm._s(v)
                                          )
                                        ])
                                      }),
                                      0
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _c(
                                      "ul",
                                      _vm._l(item.new_values, function(v, i) {
                                        return _c("li", [
                                          _vm._v(
                                            " " + _vm._s(i) + " - " + _vm._s(v)
                                          )
                                        ])
                                      }),
                                      0
                                    )
                                  ])
                                ])
                              }),
                              0
                            )
                          ]
                        },
                        proxy: true
                      }
                    ])
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c("v-divider")
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.showPendings
        ? _c(
            "v-content",
            { staticClass: "white" },
            [
              _c(
                "v-container",
                { staticClass: "white", attrs: { fluid: "" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-2",
                          attrs: { dark: "", color: "blue darken-2" },
                          on: {
                            click: function($event) {
                              _vm.showPendings = false
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                    ΕΠΙΣΤΡΟΦΗ ΣΤΟ ΗΜΕΡΟΛΟΓΙΟ\n                "
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { attrs: { color: "purple" } },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "6" } },
                        [
                          _c("h2", [_vm._v("Ραντεβού προς Έλεγχο")]),
                          _vm._v(" "),
                          _c(
                            "v-list",
                            { attrs: { "two-line": "" } },
                            [
                              _vm._l(_vm.pendings, function(item, index) {
                                return [
                                  _c(
                                    "v-list-item",
                                    {
                                      on: {
                                        click: function($event) {
                                          return _vm.editPending(
                                            _vm.pendings[index]
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c("v-list-item-title", [
                                            _vm._v(_vm._s(item.start_date))
                                          ]),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", {
                                            domProps: {
                                              innerHTML: _vm._s(item.title)
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("v-divider", { key: index })
                                ]
                              })
                            ],
                            2
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      !_vm.showPendings
        ? _c(
            "v-content",
            { staticClass: "mt-2 pt-5" },
            [
              _c(
                "v-container",
                { staticClass: "grey lighten-4 ", attrs: { fluid: "" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "3" } },
                        [
                          _vm.userCompanies.length > 1
                            ? _c("v-select", {
                                attrs: {
                                  "menu-props": { maxHeight: "400" },
                                  items: _vm.userCompanies,
                                  "item-text": "title",
                                  "item-value": "id",
                                  label: "ΗΜΕΡΟΛΟΓΙΑ",
                                  solo: ""
                                },
                                on: { change: _vm.filterEntities },
                                model: {
                                  value: _vm.selectedCompanie,
                                  callback: function($$v) {
                                    _vm.selectedCompanie = $$v
                                  },
                                  expression: "selectedCompanie"
                                }
                              })
                            : _vm._e()
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "9" } },
                        _vm._l(_vm.entities, function(e) {
                          return _c(
                            "v-btn",
                            {
                              key: e.id,
                              staticClass: "mr-2",
                              class: "mb-2",
                              attrs: {
                                small: "",
                                dark: "",
                                left: "",
                                "active-class": "red",
                                color:
                                  _vm.selectedEntity.id != null &&
                                  _vm.selectedEntity.id == e.id
                                    ? e.color
                                    : _vm.selectedEntity.id != null
                                    ? "grey"
                                    : e.color,
                                elevation:
                                  _vm.selectedEntity.id != null &&
                                  _vm.selectedEntity.id == e.id
                                    ? 10
                                    : _vm.selectedEntity.id != null
                                    ? 0
                                    : 4
                              },
                              on: {
                                click: function($event) {
                                  return _vm.setEntity(e)
                                }
                              }
                            },
                            [_c("span", [_vm._v(_vm._s(e.title))])]
                          )
                        }),
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { staticClass: "fill-height" },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-sheet",
                            { attrs: { height: "64" } },
                            [
                              _c(
                                "v-toolbar",
                                { attrs: { flat: "", color: "white" } },
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "mr-4",
                                      attrs: {
                                        outlined: "",
                                        color: "grey darken-2"
                                      },
                                      on: { click: _vm.setToday }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                ΣΗΜΕΡΑ\n                            "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        fab: "",
                                        text: "",
                                        small: "",
                                        color: "grey darken-2"
                                      },
                                      on: { click: _vm.prev }
                                    },
                                    [
                                      _c("v-icon", { attrs: { small: "" } }, [
                                        _vm._v("mdi-chevron-left")
                                      ])
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        fab: "",
                                        text: "",
                                        small: "",
                                        color: "grey darken-2"
                                      },
                                      on: { click: _vm.next }
                                    },
                                    [
                                      _c("v-icon", { attrs: { small: "" } }, [
                                        _vm._v("mdi-chevron-right")
                                      ])
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("v-toolbar-title", [
                                    _vm._v(_vm._s(_vm.title))
                                  ]),
                                  _vm._v(" "),
                                  _c("v-spacer"),
                                  _vm._v(" "),
                                  _c(
                                    "v-menu",
                                    {
                                      attrs: { bottom: "", right: "" },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "activator",
                                            fn: function(ref) {
                                              var on = ref.on
                                              return [
                                                _c(
                                                  "v-btn",
                                                  _vm._g(
                                                    {
                                                      staticClass: "mr-5",
                                                      attrs: {
                                                        outlined: "",
                                                        color: "grey darken-2"
                                                      }
                                                    },
                                                    on
                                                  ),
                                                  [
                                                    _c("span", [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm.typeToLabel[
                                                            _vm.type
                                                          ]
                                                        )
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "v-icon",
                                                      { attrs: { right: "" } },
                                                      [_vm._v("mdi-menu-down")]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        false,
                                        2600186216
                                      )
                                    },
                                    [
                                      _vm._v(" "),
                                      _c(
                                        "v-list",
                                        [
                                          _c(
                                            "v-list-item",
                                            {
                                              on: {
                                                click: function($event) {
                                                  _vm.type = "month"
                                                }
                                              }
                                            },
                                            [
                                              _c("v-list-item-title", [
                                                _vm._v("Μήνας")
                                              ])
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-list-item",
                                            {
                                              on: {
                                                click: function($event) {
                                                  _vm.type = "day"
                                                }
                                              }
                                            },
                                            [
                                              _c("v-list-item-title", [
                                                _vm._v("Ημέρα")
                                              ])
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-list-item",
                                            {
                                              on: {
                                                click: function($event) {
                                                  _vm.type = "week"
                                                }
                                              }
                                            },
                                            [
                                              _c("v-list-item-title", [
                                                _vm._v("Εβδομάδα")
                                              ])
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("v-menu", {
                                    attrs: { right: "" },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "activator",
                                          fn: function(ref) {
                                            var on = ref.on
                                            return [
                                              _c(
                                                "div",
                                                { staticClass: "my-2" },
                                                [
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      staticClass:
                                                        "blue darken-2 ",
                                                      attrs: {
                                                        dark: "",
                                                        large: "",
                                                        icon: "",
                                                        depressed: "",
                                                        small: ""
                                                      },
                                                      on: {
                                                        click:
                                                          _vm.newAppointment
                                                      }
                                                    },
                                                    [
                                                      _c("v-icon", [
                                                        _vm._v("mdi-plus")
                                                      ])
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      false,
                                      503347724
                                    )
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-sheet",
                            { attrs: { height: "700" } },
                            [
                              _c("v-calendar", {
                                ref: "calendar",
                                attrs: {
                                  locale: "el",
                                  color: "primary",
                                  events: _vm.events,
                                  "first-interval": _vm.firstTime,
                                  "event-color": _vm.getEventColor,
                                  now: _vm.today,
                                  type: _vm.type,
                                  "event-overlap-mode": "stack",
                                  "event-overlap-threshold": 30
                                },
                                on: {
                                  "click:event": _vm.showEvent,
                                  "click:date": _vm.myDate,
                                  change: _vm.updateRange
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "event",
                                      fn: function(ref) {
                                        var event = ref.event
                                        return [
                                          _vm._v(
                                            "\n                                " +
                                              _vm._s(event.start_time) +
                                              " - " +
                                              _vm._s(event.end_time) +
                                              " " +
                                              _vm._s(event.user_name) +
                                              " " +
                                              _vm._s(event.name) +
                                              "\n                            "
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  436004155
                                ),
                                model: {
                                  value: _vm.focus,
                                  callback: function($$v) {
                                    _vm.focus = $$v
                                  },
                                  expression: "focus"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "v-menu",
                                {
                                  attrs: {
                                    "close-on-content-click": false,
                                    activator: _vm.selectedElement,
                                    "offset-x": ""
                                  },
                                  model: {
                                    value: _vm.selectedOpen,
                                    callback: function($$v) {
                                      _vm.selectedOpen = $$v
                                    },
                                    expression: "selectedOpen"
                                  }
                                },
                                [
                                  _c(
                                    "v-card",
                                    {
                                      attrs: {
                                        color: "grey lighten-4",
                                        "min-width": "350px",
                                        "max-width": "450px",
                                        flat: ""
                                      }
                                    },
                                    [
                                      _c(
                                        "v-toolbar",
                                        {
                                          attrs: {
                                            color: _vm.selectedEvent.color,
                                            dark: ""
                                          }
                                        },
                                        [
                                          _c(
                                            "v-btn",
                                            {
                                              attrs: { icon: "" },
                                              on: {
                                                click: _vm.editAppointmentEntry
                                              }
                                            },
                                            [
                                              _c("v-icon", [
                                                _vm._v("mdi-pencil")
                                              ])
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c("v-toolbar-title", {
                                            domProps: {
                                              innerHTML: _vm._s(
                                                _vm.selectedEvent.title
                                              )
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("v-spacer")
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("v-card-text", [
                                        _c("span", [
                                          _c("b", [
                                            _vm._v(
                                              _vm._s(
                                                _vm.selectedEvent.start_date
                                              ) +
                                                " " +
                                                _vm._s(
                                                  _vm.selectedEvent.start_time
                                                ) +
                                                " - " +
                                                _vm._s(
                                                  _vm.selectedEvent.end_date
                                                ) +
                                                " " +
                                                _vm._s(
                                                  _vm.selectedEvent.end_time
                                                )
                                            )
                                          ])
                                        ]),
                                        _c("br"),
                                        _vm._v(" "),
                                        _c("span", {
                                          domProps: {
                                            innerHTML: _vm._s(
                                              _vm.selectedEvent.given_name +
                                                " " +
                                                _vm.selectedEvent.family_name
                                            )
                                          }
                                        }),
                                        _c("br"),
                                        _vm._v(" "),
                                        _c("span", {
                                          domProps: {
                                            innerHTML: _vm._s(
                                              "ΑΦΜ: " + _vm.selectedEvent.vat
                                            )
                                          }
                                        }),
                                        _c("br"),
                                        _vm._v(" "),
                                        _vm.selectedEvent.email != null
                                          ? _c("span", {
                                              domProps: {
                                                innerHTML: _vm._s(
                                                  "Email: " +
                                                    _vm.selectedEvent.email
                                                )
                                              }
                                            })
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _c("br"),
                                        _vm._v(" "),
                                        _vm.selectedEvent.phone != null
                                          ? _c("span", {
                                              domProps: {
                                                innerHTML: _vm._s(
                                                  "Τηλ 1: " +
                                                    _vm.selectedEvent.phone
                                                )
                                              }
                                            })
                                          : _vm._e(),
                                        _c("br"),
                                        _vm._v(" "),
                                        _vm.selectedEvent.phone_number != null
                                          ? _c("span", {
                                              domProps: {
                                                innerHTML: _vm._s(
                                                  "Τηλ 2: " +
                                                    _vm.selectedEvent
                                                      .phone_number
                                                )
                                              }
                                            })
                                          : _vm._e(),
                                        _c("br"),
                                        _vm._v(" "),
                                        _vm.selectedEvent.comment != null
                                          ? _c("span", {
                                              domProps: {
                                                innerHTML: _vm._s(
                                                  "Σχόλια: " +
                                                    _vm.selectedEvent.comment
                                                )
                                              }
                                            })
                                          : _vm._e()
                                      ]),
                                      _vm._v(" "),
                                      _c("v-card-actions")
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-card",
                        {
                          staticClass: "mx-auto",
                          attrs: { "min-height": "380px" }
                        },
                        [
                          _c(
                            "v-card-title",
                            { staticClass: " text-light", attrs: { dark: "" } },
                            [
                              _vm._v(
                                "\n                            Κτίρια\n                            "
                              ),
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    small: "",
                                    absolute: "",
                                    top: "",
                                    right: "",
                                    color: "primary"
                                  },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.newCompanyDialog = true
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                ΝΕΟ ΚΤΙΡΙΟ\n                            "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c("v-simple-table", {
                                scopedSlots: _vm._u([
                                  {
                                    key: "default",
                                    fn: function() {
                                      return [
                                        _c("thead", [
                                          _c("tr", [
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Όνομα")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Uri")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Επεξεργασία")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Προβολή")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Διαγραφή ")]
                                            )
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "tbody",
                                          _vm._l(_vm.companies, function(item) {
                                            return _c("tr", { key: item.id }, [
                                              _c("td", [
                                                _vm._v(_vm._s(item.title))
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(_vm._s(item.name))
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "v-dialog",
                                                    {
                                                      staticClass: "mr-3",
                                                      attrs: {
                                                        elevation: "0",
                                                        persistent: "",
                                                        "max-width": "600px"
                                                      },
                                                      scopedSlots: _vm._u(
                                                        [
                                                          {
                                                            key: "activator",
                                                            fn: function(ref) {
                                                              var on = ref.on
                                                              return [
                                                                _c(
                                                                  "v-btn",
                                                                  _vm._g(
                                                                    {
                                                                      attrs: {
                                                                        elevation:
                                                                          "0"
                                                                      },
                                                                      on: {
                                                                        click: function(
                                                                          $event
                                                                        ) {
                                                                          return _vm.editcompany(
                                                                            item
                                                                          )
                                                                        }
                                                                      }
                                                                    },
                                                                    on
                                                                  ),
                                                                  [
                                                                    _c(
                                                                      "v-icon",
                                                                      [
                                                                        _vm._v(
                                                                          "\n                                                                mdi-pencil\n                                                            "
                                                                        )
                                                                      ]
                                                                    )
                                                                  ],
                                                                  1
                                                                )
                                                              ]
                                                            }
                                                          }
                                                        ],
                                                        null,
                                                        true
                                                      ),
                                                      model: {
                                                        value: _vm.entityDialog,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.entityDialog = $$v
                                                        },
                                                        expression:
                                                          "entityDialog"
                                                      }
                                                    },
                                                    [
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-card",
                                                        [
                                                          _c("v-card-title", [
                                                            _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "headline"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Ενημέρωση Χώρος"
                                                                )
                                                              ]
                                                            )
                                                          ]),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-card-actions",
                                                            [_c("v-spacer")],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      attrs: {
                                                        icon: "",
                                                        color: "light-blue"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.showCalendar(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-icon",
                                                        { attrs: { dark: "" } },
                                                        [_vm._v("event")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      attrs: {
                                                        icon: "",
                                                        color: "light-blue"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.deleteCompany(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-icon",
                                                        { attrs: { dark: "" } },
                                                        [_vm._v("mdi-delete")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ])
                                          }),
                                          0
                                        )
                                      ]
                                    },
                                    proxy: true
                                  }
                                ])
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "400" },
                      model: {
                        value: _vm.newCompanyDialog,
                        callback: function($$v) {
                          _vm.newCompanyDialog = $$v
                        },
                        expression: "newCompanyDialog"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", { staticClass: "headline" }, [
                            _vm._v(
                              "\n                            Νέο Κτίριο\n                        "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c(
                                "v-form",
                                [
                                  _vm.newCompanyError != null
                                    ? _c("span", { staticClass: "red" }, [
                                        _vm._v(_vm._s(_vm.newCompanyError))
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Uri",
                                      required: "",
                                      type: "text"
                                    },
                                    model: {
                                      value: _vm.newCompanyName,
                                      callback: function($$v) {
                                        _vm.newCompanyName = $$v
                                      },
                                      expression: "newCompanyName"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Τίτλος Υπηρεσίας",
                                      type: "text"
                                    },
                                    model: {
                                      value: _vm.newCompanyTitle,
                                      callback: function($$v) {
                                        _vm.newCompanyTitle = $$v
                                      },
                                      expression: "newCompanyTitle"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "green darken-1", text: "" },
                                  on: {
                                    click: function($event) {
                                      _vm.newCompanyDialog = false
                                    }
                                  }
                                },
                                [_vm._v("Άκυρο")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "green darken-1", text: "" },
                                  on: {
                                    click: function($event) {
                                      return _vm.createCompany()
                                    }
                                  }
                                },
                                [_vm._v("ΔΗΜΙΟΥΡΓΙΑ")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=template&id=55c63782&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company.vue?vue&type=template&id=55c63782& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "12" } }, [
                _c("h2", [_vm._v(_vm._s(_vm.company.title))])
              ]),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6" } },
                [
                  _c(
                    "v-card",
                    {
                      staticClass: "mx-auto",
                      attrs: { "min-height": "380px" }
                    },
                    [
                      _c(
                        "v-card-title",
                        { staticClass: "blue ", attrs: { dark: "" } },
                        [
                          _vm._v(
                            "\n                        Υπηρεσίες\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-card-text",
                        [
                          _c("v-simple-table", {
                            scopedSlots: _vm._u([
                              {
                                key: "default",
                                fn: function() {
                                  return [
                                    _c("thead", [
                                      _c("tr", [
                                        _c("th", { staticClass: "text-left" }, [
                                          _vm._v("Όνομα")
                                        ]),
                                        _vm._v(" "),
                                        _c("th", { staticClass: "text-left" }, [
                                          _vm._v("Ιδιότητα")
                                        ])
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "tbody",
                                      _vm._l(_vm.entities, function(item) {
                                        return _c("tr", { key: item.id }, [
                                          _c("td", [
                                            _vm._v(_vm._s(item.title))
                                          ]),
                                          _vm._v(" "),
                                          _c("td", [
                                            _vm._v(_vm._s(item.expertise))
                                          ])
                                        ])
                                      }),
                                      0
                                    )
                                  ]
                                },
                                proxy: true
                              }
                            ])
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-text-field", {
                        attrs: {
                          label: "Τίτλος",
                          rules: _vm.rules,
                          "hide-details": "auto"
                        },
                        model: {
                          value: _vm.company.title,
                          callback: function($$v) {
                            _vm.$set(_vm.company, "title", $$v)
                          },
                          expression: "company.title"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      staticClass: "mb-5",
                      attrs: { cols: "12", height: "400" }
                    },
                    [
                      _c(
                        "v-card",
                        { attrs: { elevation: "0" } },
                        [
                          _c("quill-editor", {
                            attrs: {
                              content: _vm.company.description,
                              options: _vm.editorOption
                            },
                            model: {
                              value: _vm.company.description,
                              callback: function($$v) {
                                _vm.$set(_vm.company, "description", $$v)
                              },
                              expression: "company.description"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", height: "400" } },
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { depressed: "", color: "primary" },
                          on: { click: _vm.update }
                        },
                        [_vm._v("\n              ΕΝΗΜΕΡΩΣΗ\n            ")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=template&id=0c64408c&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/entities.vue?vue&type=template&id=0c64408c& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  !_vm.updateEntityForm && !_vm.createEntityForm
                    ? _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-card",
                            {
                              staticClass: "mx-auto",
                              attrs: { "min-height": "380px" }
                            },
                            [
                              _c(
                                "v-card-title",
                                { staticClass: " ", attrs: { dark: "" } },
                                [
                                  _vm._v(
                                    "\n                            Υπηρεσίες\n                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-card-text",
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        small: "",
                                        absolute: "",
                                        top: "",
                                        right: "",
                                        color: "primary"
                                      },
                                      on: {
                                        click: function($event) {
                                          $event.stopPropagation()
                                          _vm.createEntityForm = true
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                ΝΕΑ ΥΠΗΡΕΣΙΑ\n                            "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("v-simple-table", {
                                    attrs: { dense: "" },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function() {
                                            return [
                                              _c("thead", [
                                                _c("tr", [
                                                  _c(
                                                    "th",
                                                    {
                                                      staticClass: "text-left"
                                                    },
                                                    [_vm._v("Όνομα")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "th",
                                                    {
                                                      staticClass: "text-left"
                                                    },
                                                    [_vm._v("Ιδιότητα")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "th",
                                                    {
                                                      staticClass: "text-left"
                                                    },
                                                    [_vm._v("Κτίριο")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "th",
                                                    {
                                                      staticClass: "text-left"
                                                    },
                                                    [_vm._v("Επεξεργασία")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "th",
                                                    {
                                                      staticClass: "text-left"
                                                    },
                                                    [_vm._v("Διαγραφή")]
                                                  )
                                                ])
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "tbody",
                                                _vm._l(_vm.entities, function(
                                                  item
                                                ) {
                                                  return _c(
                                                    "tr",
                                                    { key: item.id },
                                                    [
                                                      _c("td", [
                                                        _vm._v(
                                                          _vm._s(item.title)
                                                        )
                                                      ]),
                                                      _vm._v(" "),
                                                      _c("td", [
                                                        _vm._v(
                                                          _vm._s(item.expertise)
                                                        )
                                                      ]),
                                                      _vm._v(" "),
                                                      _c("td", [
                                                        _vm._v(
                                                          _vm._s(
                                                            item.company &&
                                                              item.company
                                                                .title !=
                                                                undefined
                                                              ? item.company
                                                                  .title
                                                              : "-"
                                                          )
                                                        )
                                                      ]),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        [
                                                          _c(
                                                            "v-btn",
                                                            _vm._g(
                                                              {
                                                                attrs: {
                                                                  elevation:
                                                                    "0",
                                                                  color:
                                                                    "light-blue",
                                                                  dark: ""
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    _vm.setEntityForUpdate = item
                                                                    _vm.updateEntityForm = true
                                                                    _vm.loadEntityServices()
                                                                    _vm.loadTimeTable(
                                                                      item.id
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              _vm.on
                                                            ),
                                                            [
                                                              _vm._v(
                                                                "ΕΠΕΞΕΡΓΑΣΙΑ "
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        [
                                                          _c(
                                                            "v-icon",
                                                            {
                                                              attrs: {
                                                                right: ""
                                                              },
                                                              on: {
                                                                click: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.deleteEntity(
                                                                    item.id
                                                                  )
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                "mdi-delete"
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ]
                                                  )
                                                }),
                                                0
                                              )
                                            ]
                                          },
                                          proxy: true
                                        }
                                      ],
                                      null,
                                      false,
                                      1831845137
                                    )
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.updateEntityForm
                    ? _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-card",
                            { staticClass: "pa-5", attrs: { elevation: "2" } },
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    small: "",
                                    absolute: "",
                                    top: "",
                                    right: "",
                                    color: "error"
                                  },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.updateEntityForm = false
                                    }
                                  }
                                },
                                [_vm._v("x")]
                              ),
                              _vm._v(" "),
                              _c("v-card-title", [
                                _c("span", { staticClass: "headline" }, [
                                  _vm._v("Ενημέρωση Υπηρεσίας")
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "v-row",
                                [
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12", sm: "4", md: "6" } },
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          items: _vm.companies,
                                          "item-text": "title",
                                          "item-value": "id",
                                          label: "Κτίριο",
                                          required: ""
                                        },
                                        model: {
                                          value:
                                            _vm.setEntityForUpdate.company_id,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "company_id",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.company_id"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12", sm: "4", md: "6" } },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Τίτλος",
                                          required: ""
                                        },
                                        model: {
                                          value: _vm.setEntityForUpdate.title,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "title",
                                              $$v
                                            )
                                          },
                                          expression: "setEntityForUpdate.title"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12", sm: "4", md: "6" } },
                                    [
                                      _c("v-text-field", {
                                        attrs: { label: "Δ/νση", required: "" },
                                        model: {
                                          value: _vm.setEntityForUpdate.address,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "address",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.address"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12", sm: "4", md: "3" } },
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          items: [
                                            "10",
                                            "15",
                                            "20",
                                            "25",
                                            "30",
                                            "35",
                                            "40",
                                            "45",
                                            "50",
                                            "55",
                                            "60",
                                            "65",
                                            "70",
                                            "75",
                                            "80",
                                            "85",
                                            "90",
                                            "95",
                                            "100",
                                            "105",
                                            "110",
                                            "115",
                                            "120"
                                          ],
                                          label: "Διάρκεια",
                                          hint:
                                            "Επιλέξτε τη διάρκεια που θέλετε να έχουν οι καταχωρήσεις για αυτόν τον χώρο, μπορεί να αλλαχθεί κατά τη δημιουργία του Ραντεβού.",
                                          "persistent-hint": "",
                                          required: ""
                                        },
                                        model: {
                                          value:
                                            _vm.setEntityForUpdate.duration,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "duration",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.duration"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12", sm: "4", md: "3" } },
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          items: [
                                            "",
                                            "5",
                                            "10",
                                            "15",
                                            "20",
                                            "25",
                                            "30",
                                            "35",
                                            "40",
                                            "45",
                                            "50",
                                            "55",
                                            "60",
                                            "65",
                                            "70",
                                            "75",
                                            "80",
                                            "85",
                                            "90",
                                            "95",
                                            "100",
                                            "105",
                                            "110",
                                            "115",
                                            "120"
                                          ],
                                          label: "Διάλειμα",
                                          hint:
                                            "Επιλέξτε τη διάρκεια του κενούπου θα υπάρχει μεταξύ των ραντεβού.",
                                          "persistent-hint": "",
                                          required: ""
                                        },
                                        model: {
                                          value:
                                            _vm.setEntityForUpdate.gap_duration,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "gap_duration",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.gap_duration"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "8" } },
                                    [
                                      _c("v-textarea", {
                                        attrs: {
                                          rows: "1",
                                          "auto-grow": "",
                                          name: "input-2-2",
                                          label: "Περιγραφή",
                                          item: _vm.entity.description
                                        },
                                        model: {
                                          value:
                                            _vm.setEntityForUpdate.description,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "description",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.description"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "4" } },
                                    [
                                      _c("v-text-field", {
                                        directives: [
                                          {
                                            name: "mask",
                                            rawName: "v-mask",
                                            value: _vm.colormask,
                                            expression: "colormask"
                                          }
                                        ],
                                        staticClass: "ma-0 pa-0",
                                        attrs: {
                                          elevation: "0",
                                          "hide-details": "",
                                          solo: ""
                                        },
                                        scopedSlots: _vm._u(
                                          [
                                            {
                                              key: "append",
                                              fn: function() {
                                                return [
                                                  _c(
                                                    "v-menu",
                                                    {
                                                      attrs: {
                                                        top: "",
                                                        "nudge-bottom": "105",
                                                        "nudge-left": "16",
                                                        "close-on-content-click": false
                                                      },
                                                      scopedSlots: _vm._u(
                                                        [
                                                          {
                                                            key: "activator",
                                                            fn: function(ref) {
                                                              var on = ref.on
                                                              return [
                                                                _c(
                                                                  "div",
                                                                  _vm._g(
                                                                    {
                                                                      style:
                                                                        _vm.swatchStyleUpdate
                                                                    },
                                                                    on
                                                                  )
                                                                )
                                                              ]
                                                            }
                                                          }
                                                        ],
                                                        null,
                                                        false,
                                                        2434969495
                                                      ),
                                                      model: {
                                                        value: _vm.menuUpdate,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.menuUpdate = $$v
                                                        },
                                                        expression: "menuUpdate"
                                                      }
                                                    },
                                                    [
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-card",
                                                        [
                                                          _c(
                                                            "v-card-text",
                                                            {
                                                              staticClass:
                                                                "pa-0"
                                                            },
                                                            [
                                                              _c(
                                                                "v-color-picker",
                                                                {
                                                                  attrs: {
                                                                    flat: "",
                                                                    "show-swatches":
                                                                      ""
                                                                  },
                                                                  model: {
                                                                    value:
                                                                      _vm
                                                                        .setEntityForUpdate
                                                                        .color,
                                                                    callback: function(
                                                                      $$v
                                                                    ) {
                                                                      _vm.$set(
                                                                        _vm.setEntityForUpdate,
                                                                        "color",
                                                                        $$v
                                                                      )
                                                                    },
                                                                    expression:
                                                                      "setEntityForUpdate.color"
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              },
                                              proxy: true
                                            }
                                          ],
                                          null,
                                          false,
                                          1623584389
                                        ),
                                        model: {
                                          value: _vm.setEntityForUpdate.color,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "color",
                                              $$v
                                            )
                                          },
                                          expression: "setEntityForUpdate.color"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    {
                                      attrs: { cols: "12", sm: "6", md: "12" }
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Ιδιότητα",
                                          required: ""
                                        },
                                        model: {
                                          value:
                                            _vm.setEntityForUpdate.expertise,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.setEntityForUpdate,
                                              "expertise",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "setEntityForUpdate.expertise"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "10" } },
                                    [
                                      _c(
                                        "v-btn",
                                        {
                                          on: {
                                            click: function($event) {
                                              _vm.showTimeTable = !_vm.showTimeTable
                                            }
                                          }
                                        },
                                        [_vm._v("ΔΙΑΧΕΙΡΙΣΗ ΩΡΩΝ ")]
                                      ),
                                      _vm._v(" "),
                                      _vm.showTimeTable
                                        ? _c(
                                            "div",
                                            [
                                              _c("timetable", {
                                                attrs: { form: _vm.timetable },
                                                on: {
                                                  updatedTimetableValues:
                                                    _vm.updatedTimetableValues
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-col",
                                    { attrs: { cols: "12" } },
                                    [
                                      _c(
                                        "v-card",
                                        [
                                          _c("v-data-table", {
                                            staticClass: "elevation-1",
                                            attrs: {
                                              height: "400",
                                              headers: _vm.headers,
                                              value: _vm.selectedServices,
                                              items: _vm.services,
                                              search: _vm.search,
                                              "item-key": "id"
                                            },
                                            scopedSlots: _vm._u(
                                              [
                                                {
                                                  key: "body",
                                                  fn: function(ref) {
                                                    var items = ref.items
                                                    return [
                                                      _c(
                                                        "tbody",
                                                        _vm._l(
                                                          _vm.services,
                                                          function(item) {
                                                            return _c(
                                                              "tr",
                                                              { key: item.id },
                                                              [
                                                                _c(
                                                                  "td",
                                                                  [
                                                                    _c(
                                                                      "v-checkbox",
                                                                      {
                                                                        staticStyle: {
                                                                          margin:
                                                                            "0px",
                                                                          padding:
                                                                            "0px"
                                                                        },
                                                                        attrs: {
                                                                          value:
                                                                            item.id,
                                                                          "hide-details":
                                                                            ""
                                                                        },
                                                                        model: {
                                                                          value:
                                                                            _vm.selectedServices,
                                                                          callback: function(
                                                                            $$v
                                                                          ) {
                                                                            _vm.selectedServices = $$v
                                                                          },
                                                                          expression:
                                                                            "selectedServices"
                                                                        }
                                                                      }
                                                                    )
                                                                  ],
                                                                  1
                                                                ),
                                                                _vm._v(" "),
                                                                _c("td", [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      item.title
                                                                    )
                                                                  )
                                                                ])
                                                              ]
                                                            )
                                                          }
                                                        ),
                                                        0
                                                      )
                                                    ]
                                                  }
                                                }
                                              ],
                                              null,
                                              false,
                                              4115352170
                                            ),
                                            model: {
                                              value: _vm.selectedRows,
                                              callback: function($$v) {
                                                _vm.selectedRows = $$v
                                              },
                                              expression: "selectedRows"
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          staticClass: "mt-2",
                                          attrs: { color: "primary" },
                                          on: { click: _vm.updateEntity }
                                        },
                                        [_vm._v("ΕΝΗΜΕΡΩΣΗ")]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.createEntityForm && _vm.can("CreateEntity")
                    ? _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-card",
                            [
                              _c("v-card-title", [
                                _c(
                                  "span",
                                  {
                                    staticClass: "headline",
                                    attrs: { color: "blue", dark: "" }
                                  },
                                  [_vm._v("Δημιουργία Υπηρεσίας")]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "v-card-text",
                                [
                                  _c(
                                    "v-container",
                                    [
                                      _c(
                                        "v-row",
                                        [
                                          _c(
                                            "v-col",
                                            {
                                              attrs: {
                                                cols: "12",
                                                sm: "12",
                                                md: "12"
                                              }
                                            },
                                            [
                                              _c("v-select", {
                                                attrs: {
                                                  items: _vm.companies,
                                                  "item-text": "title",
                                                  "item-value": "id",
                                                  label: "Κτίριο",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.selectedCompany,
                                                  callback: function($$v) {
                                                    _vm.selectedCompany = $$v
                                                  },
                                                  expression: "selectedCompany"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-col",
                                            {
                                              attrs: {
                                                cols: "12",
                                                sm: "6",
                                                md: "6"
                                              }
                                            },
                                            [
                                              _c("v-text-field", {
                                                attrs: {
                                                  label: "Ονομα Υπηρεσίας",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.entity.title,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.entity,
                                                      "title",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "entity.title"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-col",
                                            {
                                              attrs: {
                                                cols: "12",
                                                sm: "6",
                                                md: "6"
                                              }
                                            },
                                            [
                                              _c("v-text-field", {
                                                attrs: {
                                                  label: "Δ/νση",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.entity.address,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.entity,
                                                      "address",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "entity.address"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-col",
                                            {
                                              attrs: {
                                                cols: "12",
                                                sm: "6",
                                                md: "6"
                                              }
                                            },
                                            [
                                              _c("v-select", {
                                                attrs: {
                                                  items: [
                                                    10,
                                                    15,
                                                    20,
                                                    25,
                                                    30,
                                                    35,
                                                    40,
                                                    45,
                                                    50,
                                                    55,
                                                    60,
                                                    65,
                                                    70,
                                                    75,
                                                    80,
                                                    85,
                                                    90,
                                                    95,
                                                    100,
                                                    105,
                                                    110,
                                                    115,
                                                    120
                                                  ],
                                                  label: "Διάρκεια",
                                                  hint:
                                                    "Επιλέξτε τη διάρκεια που θέλετε να έχουν οι καταχωρήσεις για αυτόν τον χώρο, μπορείτε να αλλαχθεί κατά τη δημιουργία του Ραντεβού.",
                                                  "persistent-hint": "",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.entity.duration,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.entity,
                                                      "duration",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "entity.duration"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-col",
                                            { attrs: { cols: "8" } },
                                            [
                                              _c("v-textarea", {
                                                attrs: {
                                                  rows: "1",
                                                  "auto-grow": "",
                                                  name: "input-2-2",
                                                  label: "Περιγραφή",
                                                  value: _vm.entity.description
                                                },
                                                model: {
                                                  value: _vm.entity.description,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.entity,
                                                      "description",
                                                      $$v
                                                    )
                                                  },
                                                  expression:
                                                    "entity.description"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-col",
                                            { attrs: { cols: "4" } },
                                            [
                                              _c("v-text-field", {
                                                directives: [
                                                  {
                                                    name: "mask",
                                                    rawName: "v-mask",
                                                    value: _vm.colormask,
                                                    expression: "colormask"
                                                  }
                                                ],
                                                staticClass: "ma-0 pa-0",
                                                attrs: {
                                                  elevation: "0",
                                                  "hide-details": "",
                                                  solo: ""
                                                },
                                                scopedSlots: _vm._u(
                                                  [
                                                    {
                                                      key: "append",
                                                      fn: function() {
                                                        return [
                                                          _c(
                                                            "v-menu",
                                                            {
                                                              attrs: {
                                                                top: "",
                                                                "nudge-bottom":
                                                                  "105",
                                                                "nudge-left":
                                                                  "16",
                                                                "close-on-content-click": false
                                                              },
                                                              scopedSlots: _vm._u(
                                                                [
                                                                  {
                                                                    key:
                                                                      "activator",
                                                                    fn: function(
                                                                      ref
                                                                    ) {
                                                                      var on =
                                                                        ref.on
                                                                      return [
                                                                        _c(
                                                                          "div",
                                                                          _vm._g(
                                                                            {
                                                                              style:
                                                                                _vm.swatchStyle
                                                                            },
                                                                            on
                                                                          )
                                                                        )
                                                                      ]
                                                                    }
                                                                  }
                                                                ],
                                                                null,
                                                                false,
                                                                1889209126
                                                              ),
                                                              model: {
                                                                value: _vm.menu,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.menu = $$v
                                                                },
                                                                expression:
                                                                  "menu"
                                                              }
                                                            },
                                                            [
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-card",
                                                                [
                                                                  _c(
                                                                    "v-card-text",
                                                                    {
                                                                      staticClass:
                                                                        "pa-0"
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "v-color-picker",
                                                                        {
                                                                          attrs: {
                                                                            flat:
                                                                              "",
                                                                            "show-swatches":
                                                                              ""
                                                                          },
                                                                          model: {
                                                                            value:
                                                                              _vm
                                                                                .entity
                                                                                .color,
                                                                            callback: function(
                                                                              $$v
                                                                            ) {
                                                                              _vm.$set(
                                                                                _vm.entity,
                                                                                "color",
                                                                                $$v
                                                                              )
                                                                            },
                                                                            expression:
                                                                              "entity.color"
                                                                          }
                                                                        }
                                                                      )
                                                                    ],
                                                                    1
                                                                  )
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ]
                                                      },
                                                      proxy: true
                                                    }
                                                  ],
                                                  null,
                                                  false,
                                                  1113079556
                                                ),
                                                model: {
                                                  value: _vm.entity.color,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.entity,
                                                      "color",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "entity.color"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("small", [
                                    _vm._v("*indicates required field")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-card-actions",
                                [
                                  _c("v-spacer"),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        error: "",
                                        color: "blue error",
                                        text: ""
                                      },
                                      on: {
                                        click: function($event) {
                                          _vm.createEntityForm = false
                                        }
                                      }
                                    },
                                    [_vm._v("ΑΚΥΡΩΣΗ")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        color: "blue darken-1",
                                        text: ""
                                      },
                                      on: { click: _vm.createEntity }
                                    },
                                    [_vm._v("ΔΗΜΙΟΥΡΓΙΑ")]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-snackbar",
                {
                  attrs: { timeout: "3000" },
                  scopedSlots: _vm._u([
                    {
                      key: "action",
                      fn: function(ref) {
                        var attrs = ref.attrs
                        return [
                          _c(
                            "v-btn",
                            _vm._b(
                              {
                                attrs: { color: "blue", text: "" },
                                on: {
                                  click: function($event) {
                                    _vm.snackbar = false
                                  }
                                }
                              },
                              "v-btn",
                              attrs,
                              false
                            ),
                            [_vm._v("Κλείσιμο")]
                          )
                        ]
                      }
                    }
                  ]),
                  model: {
                    value: _vm.updateResponseAllert,
                    callback: function($$v) {
                      _vm.updateResponseAllert = $$v
                    },
                    expression: "updateResponseAllert"
                  }
                },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.updateResponseAllertText) +
                      "\n                "
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=template&id=ede207ce&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/history.vue?vue&type=template&id=ede207ce& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container", attrs: { id: "start" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("table", { staticClass: "table" }, [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.appointments, function(a) {
                  return _c("tr", { key: a.id }, [
                    _c("th", { attrs: { scope: "row" } }, [
                      _vm._v(_vm._s(a.id))
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(a.entities.title))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(a.title))]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v(
                        _vm._s(a.start_date) + " - " + _vm._s(a.start_time)
                      )
                    ])
                  ])
                }),
                0
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Υπηρεσία")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Δικαιολογιτικό")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Ώρα")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=template&id=2ac2c897&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/index.vue?vue&type=template&id=2ac2c897& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c(
        "v-app-bar",
        { attrs: { app: "", "clipped-left": "", color: "yellow lighten-2" } },
        [
          _c("v-app-bar-nav-icon", {
            on: {
              click: function($event) {
                _vm.drawer = !_vm.drawer
              }
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "title ml-3 mr-5" }, [
            _vm._v("My "),
            _c("span", { staticClass: "font-weight-light" }, [
              _vm._v("Calendar")
            ])
          ]),
          _vm._v(" "),
          _c("v-spacer")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-navigation-drawer",
        {
          attrs: { app: "", clipped: "", color: "grey lighten-4" },
          model: {
            value: _vm.drawer,
            callback: function($$v) {
              _vm.drawer = $$v
            },
            expression: "drawer"
          }
        },
        [
          _c(
            "v-list",
            { staticClass: "grey lighten-4", attrs: { dense: "" } },
            [
              _vm._l(_vm.items, function(item, i) {
                return [
                  item.heading
                    ? _c(
                        "v-row",
                        { key: i, attrs: { align: "center" } },
                        [
                          _c(
                            "v-col",
                            { attrs: { cols: "6" } },
                            [
                              item.heading
                                ? _c("v-subheader", [
                                    _vm._v(
                                      "\n                                " +
                                        _vm._s(item.heading) +
                                        "\n                            "
                                    )
                                  ])
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : item.divider
                    ? _c("v-divider", {
                        key: i,
                        staticClass: "my-4",
                        attrs: { dark: "" }
                      })
                    : _c(
                        "v-list-item",
                        { key: i, attrs: { link: "", href: item.href } },
                        [
                          _c(
                            "v-list-item-action",
                            [_c("v-icon", [_vm._v(_vm._s(item.icon))])],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-list-item-content",
                            [
                              _c(
                                "v-list-item-title",
                                { staticClass: "grey--text" },
                                [
                                  _vm._v(
                                    "\n                             " +
                                      _vm._s(item.text) +
                                      "\n                            "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                ]
              })
            ],
            2
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { staticClass: "grey lighten-4 ", attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c("v-col", { attrs: { cols: "12" } }, [
                    _c("h1", [_vm._v("Home Page")])
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                { attrs: { justify: "left", align: "left" } },
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-simple-table", {
                        attrs: { dense: "" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function() {
                              return [
                                _c("thead", [
                                  _c("tr", [
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Name")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Calories")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Calories")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Calories")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Calories")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Calories")
                                    ])
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "tbody",
                                  _vm._l(_vm.desserts, function(item) {
                                    return _c("tr", { key: item.name }, [
                                      _c("td", [_vm._v(_vm._s(item.name))]),
                                      _vm._v(" "),
                                      _c("td", [_vm._v(_vm._s(item.calories))]),
                                      _vm._v(" "),
                                      _c("td", [_vm._v(_vm._s(item.calories))]),
                                      _vm._v(" "),
                                      _c("td", [_vm._v(_vm._s(item.calories))]),
                                      _vm._v(" "),
                                      _c("td", [_vm._v(_vm._s(item.calories))]),
                                      _vm._v(" "),
                                      _c("td", [_vm._v(_vm._s(item.calories))])
                                    ])
                                  }),
                                  0
                                )
                              ]
                            },
                            proxy: true
                          }
                        ])
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "8" } },
                    [
                      _c("FullCalendar", {
                        ref: "fullCalendar",
                        staticClass: "demo-app-calendar",
                        attrs: {
                          defaultView: "dayGridMonth",
                          header: {
                            left: "prev,next today",
                            center: "title",
                            right:
                              "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
                          },
                          plugins: _vm.calendarPlugins,
                          weekends: _vm.calendarWeekends,
                          events: _vm.calendarEvents
                        },
                        on: {
                          dateClick: _vm.handleDateClick,
                          eventClick: _vm.eventClick
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-col", { attrs: { cols: "4" } })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", { ref: "reloadPendings" }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _vm.can("CreateManagers") && !_vm.newManager
                    ? _c(
                        "v-col",
                        { staticClass: "text-right", attrs: { cols: "12" } },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "mx-2",
                              attrs: {
                                right: "",
                                fab: "",
                                dark: "",
                                small: "",
                                color: "primary"
                              },
                              on: {
                                click: function($event) {
                                  _vm.newManager = true
                                }
                              }
                            },
                            [
                              _c("v-icon", { attrs: { dark: "" } }, [
                                _vm._v("mdi-plus")
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-row",
                        [
                          _vm.can("CreateManagers") && _vm.newManager
                            ? _c(
                                "v-col",
                                { attrs: { cols: "12" } },
                                [
                                  _c(
                                    "v-card",
                                    {
                                      staticClass: "mx-auto",
                                      attrs: { "min-height": "380px" }
                                    },
                                    [
                                      _c(
                                        "v-card-title",
                                        {
                                          staticClass: "blue ",
                                          attrs: { dark: "" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                              Δημιουργία νέου Διαχειριστή\n                            "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-card-text",
                                        [
                                          _c(
                                            "v-row",
                                            [
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "4" } },
                                                [
                                                  _c(
                                                    "v-form",
                                                    {
                                                      model: {
                                                        value: _vm.valid,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.valid = $$v
                                                        },
                                                        expression: "valid"
                                                      }
                                                    },
                                                    [
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          label: "Όνομα",
                                                          required: ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.manager.name,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.manager,
                                                              "name",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "manager.name"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          label: "E-mail",
                                                          required: ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.manager.email,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.manager,
                                                              "email",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "manager.email"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("v-select", {
                                                        attrs: {
                                                          items: ["manager"],
                                                          label: "Ρόλος"
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.manager.role,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.manager,
                                                              "role",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "manager.role"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          "append-icon": _vm.showPassA
                                                            ? "mdi-eye"
                                                            : "mdi-eye-off",
                                                          rules: [
                                                            _vm.rules.required,
                                                            _vm.rules.min
                                                          ],
                                                          type: _vm.showPassA
                                                            ? "text"
                                                            : "password",
                                                          name: "input-10-1",
                                                          label: "Κωδικός",
                                                          hint:
                                                            "Τουλάχιστον 8 Χαρακτήρες",
                                                          counter: ""
                                                        },
                                                        on: {
                                                          "click:append": function(
                                                            $event
                                                          ) {
                                                            _vm.showPassA = !_vm.showPassA
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.manager
                                                              .password,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.manager,
                                                              "password",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "manager.password"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("v-text-field", {
                                                        attrs: {
                                                          "append-icon": _vm.showPassB
                                                            ? "mdi-eye"
                                                            : "mdi-eye-off",
                                                          rules: [
                                                            _vm.rules.required,
                                                            _vm.rules.min
                                                          ],
                                                          type: _vm.showPassB
                                                            ? "text"
                                                            : "password",
                                                          name: "input-10-1",
                                                          label:
                                                            "Επιβεβαίωση Κωδικού",
                                                          hint:
                                                            "Τουλάχιστον 8 Χαρακτήρες",
                                                          counter: ""
                                                        },
                                                        on: {
                                                          "click:append": function(
                                                            $event
                                                          ) {
                                                            _vm.showPassB = !_vm.showPassB
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.manager
                                                              .password_confirmation,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.manager,
                                                              "password_confirmation",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "manager.password_confirmation"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-col",
                                                { attrs: { cols: "4" } },
                                                [
                                                  _vm._v(
                                                    "\n                                  Permissions\n                                  "
                                                  ),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label:
                                                        "Δημιουργία Χρηστών",
                                                      value: "CreateManagers",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label:
                                                        "Επεξεργασία Χρηστών",
                                                      value: "EditManagers",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label: "Δημιουργία ",
                                                      value: "CreateEntity",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label: "Επεξεργασία",
                                                      value: "EditEntity",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label: "Προβολή ",
                                                      value: "ViewEntites",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("v-checkbox", {
                                                    attrs: {
                                                      label:
                                                        "Ιστορικότητα Αλλαγών",
                                                      value: "ViewAudits",
                                                      "hide-details": ""
                                                    },
                                                    model: {
                                                      value: _vm.permissions,
                                                      callback: function($$v) {
                                                        _vm.permissions = $$v
                                                      },
                                                      expression: "permissions"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "v-btn",
                                            {
                                              attrs: {
                                                color: "blue darken-2",
                                                dark: ""
                                              },
                                              on: { click: _vm.createManager }
                                            },
                                            [_vm._v("ΔΗΜΙΟΥΡΓΙΑ")]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      attrs: { cols: _vm.editManager.id != undefined ? 6 : 12 }
                    },
                    [
                      _c(
                        "v-card",
                        {
                          staticClass: "mx-auto",
                          attrs: { "min-height": "380px" }
                        },
                        [
                          _c(
                            "v-card-title",
                            { staticClass: " ", attrs: { dark: "" } },
                            [
                              _vm._v(
                                "\n                Διαχειριστές\n              "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c("v-simple-table", {
                                scopedSlots: _vm._u([
                                  {
                                    key: "default",
                                    fn: function() {
                                      return [
                                        _c("thead", [
                                          _c("tr", [
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Όνομα")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Εταιρία")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("E-mail")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "th",
                                              { staticClass: "text-left" },
                                              [_vm._v("Edit")]
                                            )
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "tbody",
                                          _vm._l(_vm.managers, function(item) {
                                            return _c("tr", { key: item.id }, [
                                              _c("td", [
                                                _vm._v(_vm._s(item.name))
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    item.company &&
                                                      item.company.name !=
                                                        undefined
                                                      ? item.company.name
                                                      : "......."
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(_vm._s(item.email))
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      attrs: {
                                                        color: "primary",
                                                        fab: "",
                                                        small: "",
                                                        dark: "",
                                                        elevation: "0"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          _vm.editManager = item
                                                          _vm.getUserEntities()
                                                          _vm.getUserCompanies()
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("v-icon", [
                                                        _vm._v("mdi-pencil")
                                                      ])
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ])
                                          }),
                                          0
                                        )
                                      ]
                                    },
                                    proxy: true
                                  }
                                ])
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm.editManager.id != undefined
                    ? _c(
                        "v-col",
                        { attrs: { cols: "6" } },
                        [
                          _c(
                            "v-card",
                            { staticClass: "mx-auto" },
                            [
                              _c(
                                "v-card-title",
                                {
                                  staticClass: "editManager ",
                                  attrs: { dark: "" }
                                },
                                [
                                  _vm._v(
                                    "\n                Δικαιώματα\n              "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    small: "",
                                    absolute: "",
                                    top: "",
                                    right: "",
                                    fab: ""
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.editManagerDialog = false
                                      _vm.editManager = {}
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", [
                                    _vm._v(
                                      "\n                  mdi-close\n                "
                                    )
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-expansion-panels",
                                _vm._l(_vm.companies, function(c, i) {
                                  return _c(
                                    "v-expansion-panel",
                                    { key: i },
                                    [
                                      _c("v-expansion-panel-header", [
                                        _vm._v(
                                          "\n                    " +
                                            _vm._s(c.title) +
                                            "\n                  "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "v-expansion-panel-content",
                                        _vm._l(c.entities, function(e) {
                                          return _c("v-checkbox", {
                                            key: e.id,
                                            attrs: {
                                              label: e.title,
                                              value: e.id
                                            },
                                            model: {
                                              value: _vm.selectedEntities,
                                              callback: function($$v) {
                                                _vm.selectedEntities = $$v
                                              },
                                              expression: "selectedEntities"
                                            }
                                          })
                                        }),
                                        1
                                      )
                                    ],
                                    1
                                  )
                                }),
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    color: "blue darken-2 ml-5 mb-5",
                                    dark: ""
                                  },
                                  on: { click: _vm.updateManager }
                                },
                                [_vm._v("ΕΝΗΜΕΡΩΣΗ")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("div", { attrs: { id: "meet" } })])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-app-bar",
        {
          attrs: {
            app: "",
            "clipped-left": "",
            color: "blue darken-2",
            dark: ""
          }
        },
        [
          _c("v-app-bar-nav-icon", {
            on: {
              click: function($event) {
                _vm.drawer = !_vm.drawer
              }
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "title ml-3 mr-5" }, [
            _vm._v(" "),
            _c("span", { staticClass: "font-weight-light" }, [
              _vm._v("BookTool ")
            ])
          ]),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c("v-dialog", {
            staticClass: "mr-3",
            attrs: { elevation: "0", persistent: "", "max-width": "600px" },
            model: {
              value: _vm.entityDialog,
              callback: function($$v) {
                _vm.entityDialog = $$v
              },
              expression: "entityDialog"
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "small" }, [_vm._v(_vm._s(_vm.auth.name))]),
          _vm._v(" "),
          _c(
            "v-btn",
            {
              staticClass: "ml-3 mr-3",
              attrs: { elevation: "0", color: "light-blue", dark: "" },
              on: { click: _vm.logout }
            },
            [_vm._v("ΕΞΟΔΟΣ")]
          ),
          _vm._v(" "),
          _c(
            "v-badge",
            {
              staticClass: "mr-5",
              attrs: {
                color: _vm.pendings_number > 0 ? "red" : "green",
                dark: ""
              },
              scopedSlots: _vm._u([
                {
                  key: "badge",
                  fn: function() {
                    return [_c("span", [_vm._v(_vm._s(_vm.pendings_number))])]
                  },
                  proxy: true
                }
              ])
            },
            [
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: {
                    elevation: "0",
                    color: "light-green",
                    fab: "",
                    "x-small": "",
                    dark: ""
                  },
                  on: {
                    click: function($event) {
                      _vm.sendPendings()
                      _vm.showPendings()
                    }
                  }
                },
                [_c("v-icon", [_vm._v("lightbulb_outline")])],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-navigation-drawer",
        {
          attrs: { app: "", clipped: "", color: "grey lighten-4" },
          model: {
            value: _vm.drawer,
            callback: function($$v) {
              _vm.drawer = $$v
            },
            expression: "drawer"
          }
        },
        [
          _c(
            "v-list",
            { staticClass: "grey lighten-4", attrs: { dense: "" } },
            [
              _vm._l(_vm.items, function(item, i) {
                return item.permission == undefined || _vm.can(item.permission)
                  ? [
                      item.heading
                        ? _c(
                            "v-row",
                            { key: i, attrs: { align: "center" } },
                            [
                              _c(
                                "v-col",
                                { attrs: { cols: "6" } },
                                [
                                  item.heading
                                    ? _c("v-subheader", [
                                        _vm._v(
                                          "\n                " +
                                            _vm._s(item.heading) +
                                            " sdf\n              "
                                        )
                                      ])
                                    : _vm._e()
                                ],
                                1
                              )
                            ],
                            1
                          )
                        : item.divider
                        ? _c("v-divider", {
                            key: i,
                            staticClass: "my-4",
                            attrs: { dark: "" }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      item.permission == undefined || _vm.can(item.permission)
                        ? _c(
                            "v-list-item",
                            {
                              directives: [{ name: "else", rawName: "v-else" }],
                              key: i,
                              attrs: { link: "", href: item.href }
                            },
                            [
                              _c(
                                "v-list-item-action",
                                [_c("v-icon", [_vm._v(_vm._s(item.icon))])],
                                1
                              ),
                              _vm._v(" "),
                              item.permission == undefined ||
                              _vm.can(item.permission)
                                ? _c(
                                    "v-list-item-content",
                                    [
                                      _c(
                                        "v-list-item-title",
                                        { staticClass: "grey--text" },
                                        [
                                          _vm._v(
                                            "\n                " +
                                              _vm._s(item.text) +
                                              "\n              "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ],
                            1
                          )
                        : _vm._e()
                    ]
                  : _vm._e()
              })
            ],
            2
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/register.vue?vue&type=template&id=aacc3324&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/register.vue?vue&type=template&id=aacc3324& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "inspire" } },
    [
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { staticClass: "fill-height", attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                { attrs: { align: "center", justify: "center" } },
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "8", md: "4" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "elevation-12" },
                        [
                          _c(
                            "v-toolbar",
                            { attrs: { color: "primary", dark: "", flat: "" } },
                            [
                              _c("v-toolbar-title", [_vm._v("Register form")]),
                              _vm._v(" "),
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { bottom: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g(
                                              {
                                                attrs: {
                                                  href: _vm.source,
                                                  icon: "",
                                                  large: "",
                                                  target: "_blank"
                                                }
                                              },
                                              on
                                            ),
                                            [
                                              _c("v-icon", [
                                                _vm._v("mdi-code-tags")
                                              ])
                                            ],
                                            1
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [_vm._v(" "), _c("span", [_vm._v("Source")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { right: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g(
                                              {
                                                attrs: {
                                                  icon: "",
                                                  large: "",
                                                  href:
                                                    "https://codepen.io/johnjleider/pen/pMvGQO",
                                                  target: "_blank"
                                                }
                                              },
                                              on
                                            ),
                                            [
                                              _c("v-icon", [
                                                _vm._v("mdi-codepen")
                                              ])
                                            ],
                                            1
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [_vm._v(" "), _c("span", [_vm._v("Codepen")])]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c(
                                "v-form",
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Name",
                                      name: "name",
                                      "prepend-icon": "person",
                                      type: "text"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "E-mail",
                                      name: "email",
                                      "prepend-icon": "person",
                                      type: "text"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      id: "password",
                                      label: "Password",
                                      name: "password",
                                      "prepend-icon": "lock",
                                      type: "password"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      id: "password",
                                      label: "Confirm Password",
                                      name: "confirm_password",
                                      "prepend-icon": "lock",
                                      type: "password"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c("v-btn", { attrs: { color: "primary" } }, [
                                _vm._v("Register")
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=template&id=26e572a9&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services.vue?vue&type=template&id=26e572a9& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-btn",
                        _vm._g(
                          _vm._b(
                            {
                              staticClass: "mb-2 flat-right",
                              attrs: { right: "", color: "primary", dark: "" },
                              on: { click: _vm.newService }
                            },
                            "v-btn",
                            _vm.attrs,
                            false
                          ),
                          _vm.on
                        ),
                        [_vm._v("\n            ΝΕΑ ΚΑΤΑΧΩΡΗΣΗ\n          ")]
                      ),
                      _vm._v(" "),
                      _c("v-simple-table", {
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function() {
                              return [
                                _c("thead", [
                                  _c("tr", [
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Τίτλος")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Διαγραφή")
                                    ])
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "tbody",
                                  _vm._l(_vm.services, function(item) {
                                    return _c("tr", { key: item.name }, [
                                      _c(
                                        "td",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.editService(item.id)
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(item.title))]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "td",
                                        [
                                          _c(
                                            "v-icon",
                                            {
                                              attrs: { small: "" },
                                              on: {
                                                click: function($event) {
                                                  return _vm.deleteService(item)
                                                }
                                              }
                                            },
                                            [_vm._v("mdi-delete")]
                                          )
                                        ],
                                        1
                                      )
                                    ])
                                  }),
                                  0
                                )
                              ]
                            },
                            proxy: true
                          }
                        ])
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=template&id=41680f60&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/edit.vue?vue&type=template&id=41680f60& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-text-field", {
                        attrs: {
                          label: "Τίτλος",
                          rules: _vm.rules,
                          "hide-details": "auto"
                        },
                        model: {
                          value: _vm.service.title,
                          callback: function($$v) {
                            _vm.$set(_vm.service, "title", $$v)
                          },
                          expression: "service.title"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      staticClass: "mb-5",
                      attrs: { cols: "12", height: "400" }
                    },
                    [
                      _c(
                        "v-card",
                        { attrs: { elevation: "0" } },
                        [
                          _c("quill-editor", {
                            attrs: {
                              content: _vm.service.description,
                              options: _vm.editorOption
                            },
                            model: {
                              value: _vm.service.description,
                              callback: function($$v) {
                                _vm.$set(_vm.service, "description", $$v)
                              },
                              expression: "service.description"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      staticClass: "mb-5",
                      attrs: { cols: "12", height: "400" }
                    },
                    [
                      _c(
                        "v-card",
                        { attrs: { elevation: "0" } },
                        [
                          _c("v-text-field", {
                            attrs: {
                              label: "Εξωτερικός Σύνδεσμος για ψηφιακή έκδωση",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.service.external_link,
                              callback: function($$v) {
                                _vm.$set(_vm.service, "external_link", $$v)
                              },
                              expression: "service.external_link"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "mt-5", attrs: { elevation: "0" } },
                        [
                          _vm._v("\n            Νέο Αρχείο\n            "),
                          _vm.success != ""
                            ? _c(
                                "div",
                                { staticClass: "alert alert-success" },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(_vm.success) +
                                      "\n            "
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "form",
                            {
                              attrs: { enctype: "multipart/form-data" },
                              on: { submit: _vm.formSubmit }
                            },
                            [
                              _c("input", {
                                staticClass: "form-control",
                                attrs: { type: "file" },
                                on: { change: _vm.onChange }
                              }),
                              _vm._v(" "),
                              _c("br"),
                              _vm._v(" "),
                              _c("v-text-field", {
                                staticClass: "form-control",
                                attrs: {
                                  label: "Όνομα Αρχείου",
                                  cols: "3",
                                  type: "text"
                                },
                                model: {
                                  value: _vm.file.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.file, "name", $$v)
                                  },
                                  expression: "file.name"
                                }
                              }),
                              _vm._v(" "),
                              _vm.formMessage != null
                                ? _c(
                                    "span",
                                    { staticClass: "red text-white pa-1 mb-2" },
                                    [
                                      _vm._v(_vm._s(_vm.formMessage) + " "),
                                      _c("br")
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formMessageFile != null
                                ? _c(
                                    "span",
                                    { staticClass: "red text-white pa-1 mb-2" },
                                    [
                                      _vm._v(_vm._s(_vm.formMessageFile) + " "),
                                      _c("br")
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { depressed: "", color: "green" },
                                  on: { click: _vm.formSubmit }
                                },
                                [
                                  _vm._v(
                                    "\n                Ανέβασμα Αρχείου\n              "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", height: "400" } },
                    [
                      _c("v-simple-table", {
                        attrs: { elevation: "2" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function() {
                              return [
                                _c("thead", [
                                  _c("tr", [
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Όνομα Αρχείου")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Διαδρομή")
                                    ]),
                                    _vm._v(" "),
                                    _c("th", { staticClass: "text-left" }, [
                                      _vm._v("Διαγραφή")
                                    ])
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "tbody",
                                  _vm._l(_vm.media, function(item) {
                                    return _c("tr", { key: item.name }, [
                                      _c("td", [_vm._v(_vm._s(item.name))]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              href: item.fullUrl,
                                              target: "_blank"
                                            }
                                          },
                                          [_vm._v("Προβολή")]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "td",
                                        [
                                          _c(
                                            "v-btn",
                                            {
                                              on: {
                                                click: function($event) {
                                                  return _vm.deleteMedia(
                                                    item.id
                                                  )
                                                }
                                              }
                                            },
                                            [_vm._v("ΔΙΑΓΡΑΦΗ")]
                                          )
                                        ],
                                        1
                                      )
                                    ])
                                  }),
                                  0
                                )
                              ]
                            },
                            proxy: true
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { right: "", depressed: "", color: "primary" },
                          on: { click: _vm.updateService }
                        },
                        [_vm._v("ΕΝΗΜΕΡΩΣΗ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { right: "", depressed: "", color: "#cccccc" },
                          on: { click: _vm.gotoServices }
                        },
                        [_vm._v("ΕΠΙΣΤΡΟΦΗ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "float-right",
                          attrs: { right: "", depressed: "", color: "error" },
                          on: { click: _vm.deleteService }
                        },
                        [_vm._v("ΔΙΑΓΡΑΦΗ")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=template&id=22a018ba&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/services/new.vue?vue&type=template&id=22a018ba& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-text-field", {
                        attrs: {
                          label: "Τίτλος",
                          rules: _vm.rules,
                          "hide-details": "auto"
                        },
                        model: {
                          value: _vm.service.title,
                          callback: function($$v) {
                            _vm.$set(_vm.service, "title", $$v)
                          },
                          expression: "service.title"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      staticClass: "mb-5",
                      attrs: { cols: "12", height: "400" }
                    },
                    [
                      _c(
                        "v-card",
                        { attrs: { elevation: "0" } },
                        [
                          _c("quill-editor", {
                            attrs: {
                              content: _vm.service.description,
                              options: _vm.editorOption
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", height: "400" } },
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { depressed: "", color: "primary" },
                          on: { click: _vm.createService }
                        },
                        [_vm._v("\n            Δημιουργια\n          ")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=template&id=303db3ee&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/settings.vue?vue&type=template&id=303db3ee& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-app",
    { attrs: { id: "keep" } },
    [
      _c("navigation", {
        ref: "reloadPendings",
        on: {
          pendings: function($event) {
            ;[]
          },
          showPendings: function($event) {
            ;[]
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-content",
        [
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-text-field", {
                        attrs: { label: "Τίτλος", "hide-details": "auto" },
                        model: {
                          value: _vm.form.title,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "title", $$v)
                          },
                          expression: "form.title"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-textarea", {
                        attrs: {
                          name: "input-7-1",
                          filled: "",
                          label: "Περιγραφή",
                          "auto-grow": "",
                          value: _vm.form.homeDescription
                        },
                        model: {
                          value: _vm.form.homeDescription,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "homeDescription", $$v)
                          },
                          expression: "form.homeDescription"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v("\n" + _vm._s(_vm.settings) + "\n        "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c("v-switch", {
                        attrs: { label: "Live" },
                        model: {
                          value: _vm.form.live,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "live", $$v)
                          },
                          expression: "form.live"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm.form.live
                    ? _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _vm._v(
                            "\n            Σύστημα Διασύνδεσης LIVE\n            "
                          ),
                          _c("v-text-field", {
                            attrs: {
                              label: "Client Id",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.clientId,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "clientId", $$v)
                              },
                              expression: "form.clientId"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Client Secret",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.clientSecret,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "clientSecret", $$v)
                              },
                              expression: "form.clientSecret"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Authorize",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.urlAuthorize,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "urlAuthorize", $$v)
                              },
                              expression: "form.urlAuthorize"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Access Token",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.urlAccessToken,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "urlAccessToken", $$v)
                              },
                              expression: "form.urlAccessToken"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Resource Owner Details",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.urlResourceOwnerDetails,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.form,
                                  "urlResourceOwnerDetails",
                                  $$v
                                )
                              },
                              expression: "form.urlResourceOwnerDetails"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Redirect Uri",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.redirectUri,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "redirectUri", $$v)
                              },
                              expression: "form.redirectUri"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  !_vm.form.live
                    ? _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _vm._v(
                            "\n            Σύστημα Διασύνδεσης DEMO ΠΕΡΙΒΑΛΛΟΝ\n            "
                          ),
                          _c("v-text-field", {
                            attrs: {
                              label: "Client Id",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devClientId,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "devClientId", $$v)
                              },
                              expression: "form.devClientId"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Client Secret",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devClientSecret,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "devClientSecret", $$v)
                              },
                              expression: "form.devClientSecret"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Authorize",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devCrlAuthorize,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "devCrlAuthorize", $$v)
                              },
                              expression: "form.devCrlAuthorize"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Access Token",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devCrlAccessToken,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "devCrlAccessToken", $$v)
                              },
                              expression: "form.devCrlAccessToken"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Url Resource Owner Details",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devCrlResourceOwnerDetails,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.form,
                                  "devCrlResourceOwnerDetails",
                                  $$v
                                )
                              },
                              expression: "form.devCrlResourceOwnerDetails"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              label: "Redirect Uri",
                              "hide-details": "auto"
                            },
                            model: {
                              value: _vm.form.devCedirectUri,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "devCedirectUri", $$v)
                              },
                              expression: "form.devCedirectUri"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "v-tabs",
                        {
                          attrs: {
                            "background-color": "grey accent-4",
                            centered: "",
                            dark: "",
                            "icons-and-text": ""
                          },
                          model: {
                            value: _vm.tab,
                            callback: function($$v) {
                              _vm.tab = $$v
                            },
                            expression: "tab"
                          }
                        },
                        [
                          _c("v-tabs-slider"),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-1" } }, [
                            _vm._v("SMTP")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-2" } }, [
                            _vm._v("MAIL")
                          ]),
                          _vm._v(" "),
                          _c("v-tab", { attrs: { href: "#tab-3" } }, [
                            _vm._v("ΚΕΙΜΕΝΑ ΑΡΧΙΚΗΣ")
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tabs-items",
                        {
                          model: {
                            value: _vm.tab,
                            callback: function($$v) {
                              _vm.tab = $$v
                            },
                            expression: "tab"
                          }
                        },
                        [
                          _c(
                            "v-tab-item",
                            { key: 1, attrs: { value: "tab-" + 1 } },
                            [
                              _c(
                                "v-card",
                                { attrs: { flat: "" } },
                                [
                                  _c(
                                    "v-card-text",
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Client Id",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value: _vm.form.devClientId,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devClientId",
                                              $$v
                                            )
                                          },
                                          expression: "form.devClientId"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Client Secret",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value: _vm.form.devClientSecret,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devClientSecret",
                                              $$v
                                            )
                                          },
                                          expression: "form.devClientSecret"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Url Authorize",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value: _vm.form.devCrlAuthorize,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devCrlAuthorize",
                                              $$v
                                            )
                                          },
                                          expression: "form.devCrlAuthorize"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Url Access Token",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value: _vm.form.devCrlAccessToken,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devCrlAccessToken",
                                              $$v
                                            )
                                          },
                                          expression: "form.devCrlAccessToken"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Url Resource Owner Details",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value:
                                            _vm.form.devCrlResourceOwnerDetails,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devCrlResourceOwnerDetails",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "form.devCrlResourceOwnerDetails"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Redirect Uri",
                                          "hide-details": "auto"
                                        },
                                        model: {
                                          value: _vm.form.devCedirectUri,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "devCedirectUri",
                                              $$v
                                            )
                                          },
                                          expression: "form.devCedirectUri"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-tab-item",
                            { key: 2, attrs: { value: "tab-" + 2 } },
                            [
                              _c(
                                "v-card",
                                { attrs: { flat: "" } },
                                [_c("v-card-text", [_vm._v("Some dtext")])],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    [
                      _c("v-btn", { on: { click: _vm.update } }, [
                        _vm._v("ΕΝΗΜΕΡΩΣΗ")
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-snackbar",
                {
                  attrs: {
                    timeout: -1,
                    value: true,
                    color: "blue-grey",
                    absolute: "",
                    right: "",
                    rounded: "pill",
                    top: ""
                  },
                  model: {
                    value: _vm.alert,
                    callback: function($$v) {
                      _vm.alert = $$v
                    },
                    expression: "alert"
                  }
                },
                [_vm._v("\n          Η Σελίδα ενημερώθηκε!\n        ")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/timetables.vue?vue&type=template&id=c0554a06&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/timetables.vue?vue&type=template&id=c0554a06& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-row",
        [
          _vm._l(_vm.form, function(d, formKey) {
            return _c(
              "v-col",
              { key: formKey, attrs: { cols: "12" } },
              [
                _c(
                  "v-row",
                  [
                    _c("v-col", { staticClass: "mt-4", attrs: { cols: "2" } }, [
                      _vm._v(_vm._s(d.day))
                    ]),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      { attrs: { cols: "10" } },
                      _vm._l(_vm.form[formKey].hours, function(h, hKey) {
                        return _c(
                          "v-row",
                          { key: hKey, attrs: { "no-gutters": "" } },
                          [
                            _c(
                              "v-col",
                              { staticClass: "pl-4", attrs: { cols: "4" } },
                              [
                                _c("v-select", {
                                  attrs: {
                                    items: _vm.hours,
                                    "menu-props": "auto",
                                    label: "Select",
                                    "hide-details": "",
                                    "prepend-icon": "mdi-clock",
                                    "single-line": ""
                                  },
                                  model: {
                                    value:
                                      _vm.form[formKey].hours[hKey].open_at,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form[formKey].hours[hKey],
                                        "open_at",
                                        $$v
                                      )
                                    },
                                    expression:
                                      "form[formKey].hours[hKey].open_at"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-col",
                              { staticClass: "pl-4", attrs: { cols: "4" } },
                              [
                                _c("v-select", {
                                  attrs: {
                                    items: _vm.hours,
                                    "menu-props": "auto",
                                    label: "Select",
                                    "hide-details": "",
                                    "prepend-icon": "mdi-clock",
                                    "single-line": ""
                                  },
                                  model: {
                                    value:
                                      _vm.form[formKey].hours[hKey].close_to,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form[formKey].hours[hKey],
                                        "close_to",
                                        $$v
                                      )
                                    },
                                    expression:
                                      "form[formKey].hours[hKey].close_to"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-col",
                              { staticClass: "pl-4", attrs: { cols: "2" } },
                              [
                                _c("v-text-field", {
                                  attrs: {
                                    label: "Θέσεις Εξυπηρέτησης",
                                    "hide-details": "auto"
                                  },
                                  model: {
                                    value: _vm.form[formKey].hours[hKey].slots,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form[formKey].hours[hKey],
                                        "slots",
                                        $$v
                                      )
                                    },
                                    expression:
                                      "form[formKey].hours[hKey].slots"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-col",
                              { attrs: { cols: "1" } },
                              [
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { icon: "", color: "green" },
                                    on: {
                                      click: function($event) {
                                        return _vm.addHoursToDays(formKey)
                                      }
                                    }
                                  },
                                  [_c("v-icon", [_vm._v("mdi-plus")])],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { icon: "", color: "red" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeHoursFromDays(
                                          formKey,
                                          hKey
                                        )
                                      }
                                    }
                                  },
                                  [_c("v-icon", [_vm._v("mdi-minus")])],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      }),
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          }),
          _vm._v(" "),
          _c(
            "v-col",
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { staticClass: "mt-0", attrs: { cols: "3" } },
                    [
                      _c("v-select", {
                        staticClass: "pt-0",
                        attrs: {
                          items: _vm.days,
                          "item-text": "day",
                          "item-value": "number",
                          "menu-props": "auto",
                          label: "Select",
                          "hide-details": "",
                          "prepend-icon": "mdi-calendar",
                          "single-line": ""
                        },
                        model: {
                          value: _vm.newDay.day,
                          callback: function($$v) {
                            _vm.$set(_vm.newDay, "day", $$v)
                          },
                          expression: "newDay.day"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "8" } },
                    _vm._l(_vm.form[0].hours, function(h, hKey) {
                      return _c(
                        "v-row",
                        { key: hKey, attrs: { "no-gutters": "" } },
                        [
                          _c(
                            "v-col",
                            { staticClass: "pl-4", attrs: { cols: "4" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  items: _vm.hours,
                                  "menu-props": "auto",
                                  label: "Select",
                                  "hide-details": "",
                                  "prepend-icon": "mdi-clock",
                                  "single-line": ""
                                },
                                model: {
                                  value: _vm.newDay.open_at,
                                  callback: function($$v) {
                                    _vm.$set(_vm.newDay, "open_at", $$v)
                                  },
                                  expression: "newDay.open_at"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "pl-4", attrs: { cols: "4" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  items: _vm.hours,
                                  "menu-props": "auto",
                                  label: "Select",
                                  "hide-details": "",
                                  "prepend-icon": "mdi-clock",
                                  "single-line": ""
                                },
                                model: {
                                  value: _vm.newDay.close_to,
                                  callback: function($$v) {
                                    _vm.$set(_vm.newDay, "close_to", $$v)
                                  },
                                  expression: "newDay.close_to"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "pl-4", attrs: { cols: "2" } },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Θέσεις Εξυπηρέτησης",
                                  "hide-details": "auto"
                                },
                                model: {
                                  value: _vm.newDay.slots,
                                  callback: function($$v) {
                                    _vm.$set(_vm.newDay, "slots", $$v)
                                  },
                                  expression: "newDay.slots"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { attrs: { cols: "2" } },
                            [
                              _c(
                                "v-btn",
                                {
                                  on: {
                                    click: function($event) {
                                      return _vm.addNewDay()
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                ΠΡΟΣΘΗΚΗ\n              "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    }),
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuetify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuetify */ "./node_modules/vuetify/dist/vuetify.js");
/* harmony import */ var vuetify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuetify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _deveodk_vue_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @deveodk/vue-toastr */ "./node_modules/@deveodk/vue-toastr/dist/@deveodk/vue-toastr.js");
/* harmony import */ var _deveodk_vue_toastr__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_deveodk_vue_toastr__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _deveodk_vue_toastr_dist_deveodk_vue_toastr_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @deveodk/vue-toastr/dist/@deveodk/vue-toastr.css */ "./node_modules/@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css");
/* harmony import */ var _deveodk_vue_toastr_dist_deveodk_vue_toastr_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_deveodk_vue_toastr_dist_deveodk_vue_toastr_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue2_timepicker_dist_VueTimepicker_common_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue2-timepicker/dist/VueTimepicker.common.js */ "./node_modules/vue2-timepicker/dist/VueTimepicker.common.js");
/* harmony import */ var vue2_timepicker_dist_VueTimepicker_common_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue2_timepicker_dist_VueTimepicker_common_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue2_timepicker_dist_VueTimepicker_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue2-timepicker/dist/VueTimepicker.css */ "./node_modules/vue2-timepicker/dist/VueTimepicker.css");
/* harmony import */ var vue2_timepicker_dist_VueTimepicker_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue2_timepicker_dist_VueTimepicker_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuetify/dist/vuetify.min.css */ "./node_modules/vuetify/dist/vuetify.min.css");
/* harmony import */ var vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vuetify_dist_vuetify_min_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mdi_font_css_materialdesignicons_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @mdi/font/css/materialdesignicons.css */ "./node_modules/@mdi/font/css/materialdesignicons.css");
/* harmony import */ var _mdi_font_css_materialdesignicons_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_mdi_font_css_materialdesignicons_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vue_clipboard2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-clipboard2 */ "./node_modules/vue-clipboard2/vue-clipboard.js");
/* harmony import */ var vue_clipboard2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_clipboard2__WEBPACK_IMPORTED_MODULE_7__);
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */






__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");

window.Vue = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
Vue.use(vue2_timepicker_dist_VueTimepicker_common_js__WEBPACK_IMPORTED_MODULE_3___default.a);
Vue.use(vuetify__WEBPACK_IMPORTED_MODULE_0___default.a);
window.moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
window.moment.locale('el');
Vue.use(_deveodk_vue_toastr__WEBPACK_IMPORTED_MODULE_1___default.a, {
  defaultPosition: 'toast-top-right',
  defaultType: 'info',
  defaultTimeout: 5000
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */



 // const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('index', __webpack_require__(/*! ./components/index.vue */ "./resources/js/components/index.vue")["default"]);
Vue.component('calendar', __webpack_require__(/*! ./components/calendar.vue */ "./resources/js/components/calendar.vue")["default"]);
Vue.component('navigation', __webpack_require__(/*! ./components/navigation */ "./resources/js/components/navigation.vue")["default"]);
Vue.component('managers', __webpack_require__(/*! ./components/managers */ "./resources/js/components/managers.vue")["default"]);
Vue.component('company', __webpack_require__(/*! ./components/company */ "./resources/js/components/company.vue")["default"]);
Vue.component('company-edit', __webpack_require__(/*! ./components/company/edit */ "./resources/js/components/company/edit.vue")["default"]);
Vue.component('companies', __webpack_require__(/*! ./components/companies */ "./resources/js/components/companies.vue")["default"]);
Vue.component('loginforms', __webpack_require__(/*! ./components/auth */ "./resources/js/components/auth.vue")["default"]);
Vue.component('registerforms', __webpack_require__(/*! ./components/register */ "./resources/js/components/register.vue")["default"]);
Vue.component('bookingform', __webpack_require__(/*! ./components/bookingform */ "./resources/js/components/bookingform.vue")["default"]);
Vue.component('entities', __webpack_require__(/*! ./components/entities */ "./resources/js/components/entities.vue")["default"]);
Vue.component('meetup', __webpack_require__(/*! ./components/meeting */ "./resources/js/components/meeting.vue")["default"]);
Vue.component('services', __webpack_require__(/*! ./components/services */ "./resources/js/components/services.vue")["default"]);
Vue.component('services-edit', __webpack_require__(/*! ./components/services/edit */ "./resources/js/components/services/edit.vue")["default"]);
Vue.component('services-new', __webpack_require__(/*! ./components/services/new */ "./resources/js/components/services/new.vue")["default"]);
Vue.component('timetable', __webpack_require__(/*! ./components/timetables */ "./resources/js/components/timetables.vue")["default"]);
Vue.component('settings', __webpack_require__(/*! ./components/settings */ "./resources/js/components/settings.vue")["default"]);
Vue.component('history', __webpack_require__(/*! ./components/history */ "./resources/js/components/history.vue")["default"]);
Vue.use(vue_clipboard2__WEBPACK_IMPORTED_MODULE_7___default.a);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <div>.';

Vue.config.warnHandler = function (msg, vm, trace) {
  // `trace` is the component hierarchy trace
  if (msg === ignoreWarnMessage) {
    msg = null;
    vm = null;
    trace = null;
  }
};

var app = new Vue({
  el: '#app',
  vuetify: new vuetify__WEBPACK_IMPORTED_MODULE_0___default.a({})
});

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
window.axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

var token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['csrf-token'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// import Echo from 'laravel-echo'
// window.Pusher = require('pusher-js');
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

/***/ }),

/***/ "./resources/js/components/auth.vue":
/*!******************************************!*\
  !*** ./resources/js/components/auth.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.vue?vue&type=template&id=15b8471a& */ "./resources/js/components/auth.vue?vue&type=template&id=15b8471a&");
/* harmony import */ var _auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.vue?vue&type=script&lang=js& */ "./resources/js/components/auth.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/auth.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/auth.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/components/auth.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./auth.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/auth.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_auth_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/auth.vue?vue&type=template&id=15b8471a&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/auth.vue?vue&type=template&id=15b8471a& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./auth.vue?vue&type=template&id=15b8471a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/auth.vue?vue&type=template&id=15b8471a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_auth_vue_vue_type_template_id_15b8471a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/bookingform.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/bookingform.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookingform.vue?vue&type=template&id=2b3c25bc& */ "./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc&");
/* harmony import */ var _bookingform_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookingform.vue?vue&type=script&lang=js& */ "./resources/js/components/bookingform.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bookingform.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _bookingform_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/bookingform.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/bookingform.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/bookingform.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./bookingform.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./bookingform.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./bookingform.vue?vue&type=template&id=2b3c25bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/bookingform.vue?vue&type=template&id=2b3c25bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bookingform_vue_vue_type_template_id_2b3c25bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/calendar.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/calendar.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./calendar.vue?vue&type=template&id=094224ee& */ "./resources/js/components/calendar.vue?vue&type=template&id=094224ee&");
/* harmony import */ var _calendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./calendar.vue?vue&type=script&lang=js& */ "./resources/js/components/calendar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./calendar.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _calendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__["render"],
  _calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/calendar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/calendar.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/calendar.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./calendar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./calendar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/calendar.vue?vue&type=template&id=094224ee&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/calendar.vue?vue&type=template&id=094224ee& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./calendar.vue?vue&type=template&id=094224ee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/calendar.vue?vue&type=template&id=094224ee&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_calendar_vue_vue_type_template_id_094224ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/companies.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/companies.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./companies.vue?vue&type=template&id=0c42f6a0& */ "./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0&");
/* harmony import */ var _companies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./companies.vue?vue&type=script&lang=js& */ "./resources/js/components/companies.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./companies.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _companies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/companies.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/companies.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/companies.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./companies.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./companies.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./companies.vue?vue&type=template&id=0c42f6a0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/companies.vue?vue&type=template&id=0c42f6a0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_companies_vue_vue_type_template_id_0c42f6a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/company.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/company.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./company.vue?vue&type=template&id=55c63782& */ "./resources/js/components/company.vue?vue&type=template&id=55c63782&");
/* harmony import */ var _company_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./company.vue?vue&type=script&lang=js& */ "./resources/js/components/company.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./company.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _company_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__["render"],
  _company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/company.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/company.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/components/company.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./company.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/company.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./company.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/company.vue?vue&type=template&id=55c63782&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/company.vue?vue&type=template&id=55c63782& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./company.vue?vue&type=template&id=55c63782& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company.vue?vue&type=template&id=55c63782&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_company_vue_vue_type_template_id_55c63782___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/company/edit.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/company/edit.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=46ad6a27& */ "./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/components/company/edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__["render"],
  _edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/company/edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/company/edit.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/company/edit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=template&id=46ad6a27& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/company/edit.vue?vue&type=template&id=46ad6a27&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_46ad6a27___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/entities.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/entities.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./entities.vue?vue&type=template&id=0c64408c& */ "./resources/js/components/entities.vue?vue&type=template&id=0c64408c&");
/* harmony import */ var _entities_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./entities.vue?vue&type=script&lang=js& */ "./resources/js/components/entities.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./entities.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _entities_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/entities.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/entities.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/entities.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./entities.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./entities.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/entities.vue?vue&type=template&id=0c64408c&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/entities.vue?vue&type=template&id=0c64408c& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./entities.vue?vue&type=template&id=0c64408c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/entities.vue?vue&type=template&id=0c64408c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_entities_vue_vue_type_template_id_0c64408c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/history.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/history.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./history.vue?vue&type=template&id=ede207ce& */ "./resources/js/components/history.vue?vue&type=template&id=ede207ce&");
/* harmony import */ var _history_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./history.vue?vue&type=script&lang=js& */ "./resources/js/components/history.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./history.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/history.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _history_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/history.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/history.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/components/history.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./history.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/history.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/history.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./history.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/history.vue?vue&type=template&id=ede207ce&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/history.vue?vue&type=template&id=ede207ce& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./history.vue?vue&type=template&id=ede207ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/history.vue?vue&type=template&id=ede207ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_history_vue_vue_type_template_id_ede207ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/index.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/index.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=2ac2c897& */ "./resources/js/components/index.vue?vue&type=template&id=2ac2c897&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/components/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/index.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/index.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/index.vue?vue&type=template&id=2ac2c897&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/index.vue?vue&type=template&id=2ac2c897& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=2ac2c897& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/index.vue?vue&type=template&id=2ac2c897&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2ac2c897___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/managers.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/managers.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./managers.vue?vue&type=template&id=5a9f04b1& */ "./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1&");
/* harmony import */ var _managers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./managers.vue?vue&type=script&lang=js& */ "./resources/js/components/managers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./managers.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _managers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/managers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/managers.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/managers.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./managers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./managers.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./managers.vue?vue&type=template&id=5a9f04b1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/managers.vue?vue&type=template&id=5a9f04b1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_managers_vue_vue_type_template_id_5a9f04b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/meeting.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/meeting.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./meeting.vue?vue&type=template&id=3f6c3640& */ "./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640&");
/* harmony import */ var _meeting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./meeting.vue?vue&type=script&lang=js& */ "./resources/js/components/meeting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _meeting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__["render"],
  _meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/meeting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/meeting.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/components/meeting.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_meeting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./meeting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/meeting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_meeting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./meeting.vue?vue&type=template&id=3f6c3640& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/meeting.vue?vue&type=template&id=3f6c3640&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_meeting_vue_vue_type_template_id_3f6c3640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/navigation.vue":
/*!************************************************!*\
  !*** ./resources/js/components/navigation.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation.vue?vue&type=template&id=5ce47ec2& */ "./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2&");
/* harmony import */ var _navigation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navigation.vue?vue&type=script&lang=js& */ "./resources/js/components/navigation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _navigation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/navigation.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/navigation.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/navigation.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navigation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./navigation.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navigation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navigation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./navigation.vue?vue&type=template&id=5ce47ec2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navigation.vue?vue&type=template&id=5ce47ec2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navigation_vue_vue_type_template_id_5ce47ec2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/register.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/register.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.vue?vue&type=template&id=aacc3324& */ "./resources/js/components/register.vue?vue&type=template&id=aacc3324&");
/* harmony import */ var _register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.vue?vue&type=script&lang=js& */ "./resources/js/components/register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__["render"],
  _register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/register.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/register.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/register.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./register.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/register.vue?vue&type=template&id=aacc3324&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/register.vue?vue&type=template&id=aacc3324& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./register.vue?vue&type=template&id=aacc3324& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/register.vue?vue&type=template&id=aacc3324&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_register_vue_vue_type_template_id_aacc3324___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/services.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/services.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services.vue?vue&type=template&id=26e572a9& */ "./resources/js/components/services.vue?vue&type=template&id=26e572a9&");
/* harmony import */ var _services_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services.vue?vue&type=script&lang=js& */ "./resources/js/components/services.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _services_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/services.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/services.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/services.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./services.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/services.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./services.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/services.vue?vue&type=template&id=26e572a9&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/services.vue?vue&type=template&id=26e572a9& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./services.vue?vue&type=template&id=26e572a9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services.vue?vue&type=template&id=26e572a9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_services_vue_vue_type_template_id_26e572a9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/services/edit.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/services/edit.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=41680f60& */ "./resources/js/components/services/edit.vue?vue&type=template&id=41680f60&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/components/services/edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__["render"],
  _edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/services/edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/services/edit.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/services/edit.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/services/edit.vue?vue&type=template&id=41680f60&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/services/edit.vue?vue&type=template&id=41680f60& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./edit.vue?vue&type=template&id=41680f60& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/edit.vue?vue&type=template&id=41680f60&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_41680f60___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/services/new.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/services/new.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./new.vue?vue&type=template&id=22a018ba& */ "./resources/js/components/services/new.vue?vue&type=template&id=22a018ba&");
/* harmony import */ var _new_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./new.vue?vue&type=script&lang=js& */ "./resources/js/components/services/new.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _new_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/services/new.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/services/new.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/services/new.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./new.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./new.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/services/new.vue?vue&type=template&id=22a018ba&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/services/new.vue?vue&type=template&id=22a018ba& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./new.vue?vue&type=template&id=22a018ba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/services/new.vue?vue&type=template&id=22a018ba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_new_vue_vue_type_template_id_22a018ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/settings.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/settings.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settings.vue?vue&type=template&id=303db3ee& */ "./resources/js/components/settings.vue?vue&type=template&id=303db3ee&");
/* harmony import */ var _settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.vue?vue&type=script&lang=js& */ "./resources/js/components/settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settings.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__["render"],
  _settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/settings.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/settings.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/settings.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/settings.vue?vue&type=template&id=303db3ee&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/settings.vue?vue&type=template&id=303db3ee& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=template&id=303db3ee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/settings.vue?vue&type=template&id=303db3ee&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_303db3ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/timetables.vue":
/*!************************************************!*\
  !*** ./resources/js/components/timetables.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./timetables.vue?vue&type=template&id=c0554a06& */ "./resources/js/components/timetables.vue?vue&type=template&id=c0554a06&");
/* harmony import */ var _timetables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./timetables.vue?vue&type=script&lang=js& */ "./resources/js/components/timetables.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _timetables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__["render"],
  _timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/timetables.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/timetables.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/timetables.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_timetables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./timetables.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/timetables.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_timetables_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/timetables.vue?vue&type=template&id=c0554a06&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/timetables.vue?vue&type=template&id=c0554a06& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./timetables.vue?vue&type=template&id=c0554a06& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/timetables.vue?vue&type=template&id=c0554a06&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_timetables_vue_vue_type_template_id_c0554a06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/mixins/permissionsCheck.vue":
/*!**************************************************!*\
  !*** ./resources/js/mixins/permissionsCheck.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _permissionsCheck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./permissionsCheck.vue?vue&type=script&lang=js& */ "./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns




/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  _permissionsCheck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"],
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/mixins/permissionsCheck.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_permissionsCheck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./permissionsCheck.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/mixins/permissionsCheck.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_permissionsCheck_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /shared/httpd/municipality/municipality/resources/js/app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! /shared/httpd/municipality/municipality/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);